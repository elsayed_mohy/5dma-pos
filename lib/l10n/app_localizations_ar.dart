import 'app_localizations.dart';

/// The translations for Arabic (`ar`).
class AppLocalizationsAr extends AppLocalizations {
  AppLocalizationsAr([String locale = 'ar']) : super(locale);

  @override
  String get phoneKey => 'أدخل  هاتفك';

  @override
  String get passwordKey => 'أدخل كلمة المرور';

  @override
  String get accept => 'قبول';

  @override
  String get reject => 'رفض';

  @override
  String get edit => 'تعديل';

  @override
  String get remove => 'حذف';

  @override
  String get cancel => 'إلغاء';

  @override
  String get amount => 'القيمة';

  @override
  String get tag => 'الفئه';

  @override
  String get description => 'الوصف';

  @override
  String get descriptionRequired => 'الوصف مطلوب';

  @override
  String get tagRequired => 'الفئه مطلوبة';

  @override
  String get amountRequired => 'القيمة مطلوبة';

  @override
  String get beneficiaryRequired => 'المستفيد مطلوب';

  @override
  String get notifications => 'الإشعارات';

  @override
  String get darkMode => 'الوضع الليلي';

  @override
  String get save => 'حفظ';

  @override
  String get signIn => 'دخول';

  @override
  String get loginToAccount => 'تسجيل الدخول';

  @override
  String get signUp => 'إنشاء حساب';

  @override
  String get createNewAccount => 'انشاء حساب';

  @override
  String get resetPassword => 'إعادة تعيين كلمة المرور';

  @override
  String get resetPasswordCode => 'أدخل الرمز لإعادة تعيين كلمة المرور الخاصة بك';

  @override
  String get changePassword => 'تغيير كلمة المرور';

  @override
  String get change => 'تغيير';

  @override
  String get changePasswordSuccess => 'تم تغيير كلمة المرور';

  @override
  String get username => 'اسم المستخدم';

  @override
  String get name => 'الإسم';

  @override
  String get password => 'كلمة المرور';

  @override
  String get confirmPassword => 'تأكيد كلمة المرور';

  @override
  String get email => 'البريد الإلكتروني';

  @override
  String get code => 'الرمز';

  @override
  String get codeNotValid => 'رمز إعادة التعيين غير صالح';

  @override
  String get phone => 'رقم الهاتف';

  @override
  String get forgotPassword => 'نسيت كلمة المرور ؟';

  @override
  String get changeLanguage => 'تغيير اللغة';

  @override
  String get incorrectData => 'البيانات التى أدخلتها غير صحيحة';

  @override
  String get typeToReset => 'يرجى كتابة رقم الهاتف لإعادة تعيين كلمة المرور الخاصة بك';

  @override
  String get noAccount => 'ليس لديك حساب ؟';

  @override
  String get haveAccount => 'لديك حساب ؟';

  @override
  String get registerNow => 'سجل الان';

  @override
  String get verifyMobile => 'تأكيد الهاتف';

  @override
  String get verify => 'تأكيد';

  @override
  String get notReceived => 'لم تتلق الرمز؟';

  @override
  String get mobileVerificationTitle => 'تأكيد الهاتف';

  @override
  String get mobileVerificationMsg => 'تم إرسال رسالة تحتوي على رمز تنشيط إلى هاتفك المحمول';

  @override
  String get register => 'إنشاء حساب';

  @override
  String get terms => 'الشروط و الأحكام';

  @override
  String get logout => 'تسجيل الخروج';

  @override
  String get logoutConfirm => 'هل أنت متأكد أنك تريد تسجيل الخروج ؟';

  @override
  String get currentPassword => 'كلمة المرور الحالية';

  @override
  String get newPassword => 'كلمة المرور الجديدة';

  @override
  String get confirmNewPassword => 'تأكيد كلمة المرور الجديدة';

  @override
  String get sendResetCode => 'أرسل كود إعادة التعيين';

  @override
  String get value => 'القيمة';

  @override
  String get send => 'أرسل';

  @override
  String get view => 'عرض';

  @override
  String get approved => 'موافقة';

  @override
  String get rejected => 'مرفوض';

  @override
  String get canceled => 'ألغيت';

  @override
  String get pending => 'قيد الانتظار';

  @override
  String get skip => 'تخطي';

  @override
  String get next => 'التالي';

  @override
  String get finish => 'إنهاء';

  @override
  String get search => 'ابحث';

  @override
  String get more => 'المزيد';

  @override
  String get passwordChanged => 'تم تغيير كلمة المرور بنجاح';

  @override
  String get fromDate => 'التاريخ من';

  @override
  String get toDate => 'التاريخ الي';

  @override
  String get searchAndSort => 'بحث و ترتيب';

  @override
  String get largestAmount => 'اكبر قيمة';

  @override
  String get smallestAmount => 'اقل قيمة';

  @override
  String get apply => 'تطبيق';

  @override
  String get searchCountry => 'ابحث عن دولة';

  @override
  String get reset => 'مسح';

  @override
  String get verified => 'تم التحقق';

  @override
  String get done => 'إنهاء';

  @override
  String get phoneChanged => 'لقد تم تغيير رقم هاتفك بنجاح.';

  @override
  String get all => 'الكل';

  @override
  String get newUpdateKey => 'تحديث جديد';

  @override
  String get updateAppKey => 'تحديث';

  @override
  String get successfullySaved => 'تم الحفظ بنجاح';

  @override
  String get pleaseSelectBranch => 'قم بتحديد فرع';

  @override
  String get sessionDeliverFrom => 'استلام الوردية من';

  @override
  String get pettyCashAmount => 'المبلغ المستلم';

  @override
  String get totalCashAmount => 'المبلغ الكلى';

  @override
  String get variance => 'الفرق';

  @override
  String get justification => 'التبرير';

  @override
  String get continueSession => 'استكمال الوردية';

  @override
  String get startSession => 'ابدأ الوردية';

  @override
  String get payment => 'الدفع';

  @override
  String get paidAmount => 'المبلغ المدفوع';

  @override
  String get remaining => 'المبلغ المتبقي';

  @override
  String get printLang => 'لغه الطباعة';

  @override
  String get ar => 'عربي';

  @override
  String get en => 'انجليزيه';

  @override
  String get fr => 'فرنسيه';

  @override
  String get orderCount => 'المطلوب';

  @override
  String get payableTotal => 'المبلغ المدفوع';

  @override
  String get totalBeforeDiscount => 'الاجمالي بدون الضريبة';

  @override
  String get totalIncTax => 'شامل الضريبة';

  @override
  String get totalAddons => 'اجمالي الاضافات';

  @override
  String get totalOutTax => 'بدون الضريبة';

  @override
  String get receivingProducts => 'إستلام المنتجات';

  @override
  String get selectBranch => 'قم بتحديد الفرع';

  @override
  String get selectReceivingDate => 'قم بتحديد تاريخ الاستلام';

  @override
  String get selectSupplier => 'قم بتحديد الموزع';

  @override
  String get start => 'ابدأ';

  @override
  String get availableQuantity => 'الكميه المتاحه';

  @override
  String get orderQuantity => 'الكمية المطلوبة';

  @override
  String get preservedQuantity => 'الكميه المحجوزه';

  @override
  String get purchasingPrice => 'سعر الشراء';

  @override
  String get lastPurchasingPrice => 'اخر سعر شراء';

  @override
  String get expiryDate => 'تاريخ انتهاء الصلاحيه';

  @override
  String get branch => 'الفرع';

  @override
  String get supplier => 'المورد';

  @override
  String get receivingDate => 'تاريخ الاستلام';

  @override
  String get searchByNameOrBarcode => 'ابحث بالاسم أو بالباركود';

  @override
  String get selectReceivingOrderStatus => 'اختر حاله الطلب';

  @override
  String get itemColor => 'اللون';

  @override
  String get itemSize => 'المقاس';

  @override
  String get isPriceWithTax => 'السعر شامل الضربيه ؟';

  @override
  String get barcode => 'الباركود';

  @override
  String get month => 'الشهر';

  @override
  String get year => 'السنة';

  @override
  String get quarters => 'الربع';

  @override
  String get purchaseTax => 'ضريبة الشراء';

  @override
  String get salesTax => 'ضريبة المبيعات';

  @override
  String get difference => 'الفرق';

  @override
  String get detailedReport => 'تقرير مفصل';

  @override
  String get taxDetails => 'تفاصيل الضربية';

  @override
  String get invoiceNumber => 'رقم الفاتورة';

  @override
  String get invoiceDate => 'تاريخ الفاتورة';

  @override
  String get invoiceAmountWithTax => 'القيمة بالضريبة';

  @override
  String get invoiceAmountWithoutTax => 'القيمة بدون الضريبة';

  @override
  String get taxAmount => 'قيمة الضريبة';

  @override
  String get purchaseSales => 'ضريبة الشراء والمبيعات';

  @override
  String get priceWithTax => 'السعر شامل الضربيه';

  @override
  String get priceWithoutTax => 'السعر بدون الضربيه';

  @override
  String get taxNumber => 'الرقم الضريبي';

  @override
  String get discount => 'الخصم';

  @override
  String get pos => 'نقاط البيع';

  @override
  String get itemsInTheCart => ' المضاف الي السله:-';

  @override
  String get deliveryEmployee => 'موظف الدليفري';

  @override
  String get ordersPage => 'الطلبات';

  @override
  String get filter => ' ابحث برقم الطلب';

  @override
  String get salesOfOrders => 'مبيعات النظام';

  @override
  String get scheduleOrders => 'طلبات مجدولة';

  @override
  String get revenu => 'الإيراد';

  @override
  String get canceledOrders => 'الطلبات الملغاه';

  @override
  String get points => 'النقاط';

  @override
  String get customerHistory => 'احصائيات العميل';

  @override
  String get orderItems => 'منتجات الطلب';

  @override
  String get tax => 'ضريبه';

  @override
  String get price => 'السعر';

  @override
  String get total => 'الاجمالى';

  @override
  String get returnCount => 'عدد المرتجع';

  @override
  String get totalReturnedNet => 'اجمالي المرتجعات';

  @override
  String get size => 'المقاس';

  @override
  String get color => 'اللون';

  @override
  String get totalAfterDiscount => 'السعر بعد الخصم';

  @override
  String get totalPayingAfterDiscount => 'صافي سعر الشراء';

  @override
  String get totalSellingAfterDiscount => 'صافي سعر البيع';

  @override
  String get totalSellingPrice => 'اجمالي سعر البيع';

  @override
  String get totalPayingPrice => 'اجمالي سعر الشراء';

  @override
  String get expectedProfit => 'الربح المتوقع';

  @override
  String get returnOrder => 'استرجاع الطلب';

  @override
  String get quantity => 'الكميه';

  @override
  String get cancelOrder => 'إلغاء الطلب';

  @override
  String get externalOrder => 'طلب خارجى';

  @override
  String get internalOrder => 'طلب داخلى';

  @override
  String get orderNumber => 'رقم الطلب';

  @override
  String get returnOrderNumber => 'رقم الطلب المرتجع';

  @override
  String get customerName => 'اسم العميل';

  @override
  String get customerSearch => 'ابحث من خلال رقم الموبايل';

  @override
  String get addProduct => 'اضافة منتج';

  @override
  String get customerMobile => 'رقم الهاتف العميل';

  @override
  String get preparedByName => 'أعدت بواسطة';

  @override
  String get deliveryMethod => 'طريقة التوصيل';

  @override
  String get receiptNumber => 'رقم الفاتوره';

  @override
  String get servedBy => 'انشأ بواسطه';

  @override
  String get driverName => 'اسم السائق';

  @override
  String get paymentMethod => 'طريقه الدفع';

  @override
  String get status => 'الحاله';

  @override
  String get subTotal => 'صافى';

  @override
  String get selectCustomer => 'اختر العميل ';

  @override
  String get addOrder => 'إضافة طلب';

  @override
  String get editOrder => 'تعديل طلب';

  @override
  String get orderDetails => 'تفاصيل الطلب';

  @override
  String get customer => 'العميل';

  @override
  String get orderDate => 'تاريخ الطلب';

  @override
  String get orderIssuingDate => 'تاريخ اصدار الطلب';

  @override
  String get orderType => 'نوع الطلب';

  @override
  String get branchName => 'اسم الفرع';

  @override
  String get totalItemsQty => 'اجمالي عدد المنتجات';

  @override
  String get duplicateOrder => 'تكرار الطلب';

  @override
  String get viewOrder => 'عرض الطلب';

  @override
  String get orderNotExists => 'الطلب غير موجود ';

  @override
  String get saveEdits => 'حفظ التعديلات';

  @override
  String get prepared => 'تم الاعداد';

  @override
  String get withDriver => 'مع الطيار';

  @override
  String get delivered => 'تم التوصيل';

  @override
  String get closeOrder => 'انهاء الطلب';

  @override
  String get orderProcess => 'انتظار';

  @override
  String get selectConsumerAddress => 'قم بتحديد العنوان';

  @override
  String get moreInformation => 'التوصيل و معلومات اضافية';

  @override
  String get enterMoreInformation => 'قم بادخل ما تريد هنا';

  @override
  String get newCustomer => 'عميل جديد';

  @override
  String get customerNotFound => 'هذا العميل غير موجود';

  @override
  String get unit => 'الوحدة';

  @override
  String get id => 'الرقم';

  @override
  String get print => 'طباعة';

  @override
  String get saveAndPrint => 'حفظ و طباعة';

  @override
  String get saveAndClose => 'حفظ و إغلاق';

  @override
  String get back => 'رجوع';

  @override
  String get emptyCart => 'تفريغ السلة';

  @override
  String get addItem => 'إضافة منتج';

  @override
  String get country => 'الدولة';

  @override
  String get city => 'المدينة';

  @override
  String get governorate => 'المحافظة';

  @override
  String get street => 'الشارع';

  @override
  String get street2 => 'الشارع 2';

  @override
  String get postalCode => 'رقم البريد';

  @override
  String get building => 'المبني';

  @override
  String get floor => 'الدور';

  @override
  String get flatNumber => 'رقم الشقة';

  @override
  String get landMark => 'معلم معروف';

  @override
  String get foodOrders => 'مبيعات الطعام';

  @override
  String get shopNow => 'تسوق الأن';

  @override
  String get restaurants => 'المطاعم';

  @override
  String get mainPage => 'الرئيسية';

  @override
  String get transportation => 'المشاوير';

  @override
  String get welcome => 'أهلا بيك';

  @override
  String get chooseService => 'من فضلك اختار الخدمة اللي انت عايزها !';
}
