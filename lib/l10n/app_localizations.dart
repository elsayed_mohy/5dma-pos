import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_ar.dart';
import 'app_localizations_en.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'l10n/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('ar'),
    Locale('en')
  ];

  /// No description provided for @phoneKey.
  ///
  /// In en, this message translates to:
  /// **'Enter your phone'**
  String get phoneKey;

  /// No description provided for @passwordKey.
  ///
  /// In en, this message translates to:
  /// **'Enter your password'**
  String get passwordKey;

  /// No description provided for @accept.
  ///
  /// In en, this message translates to:
  /// **'Accept'**
  String get accept;

  /// No description provided for @reject.
  ///
  /// In en, this message translates to:
  /// **'Reject'**
  String get reject;

  /// No description provided for @edit.
  ///
  /// In en, this message translates to:
  /// **'Edit'**
  String get edit;

  /// No description provided for @remove.
  ///
  /// In en, this message translates to:
  /// **'Delete'**
  String get remove;

  /// No description provided for @cancel.
  ///
  /// In en, this message translates to:
  /// **'Cancel'**
  String get cancel;

  /// No description provided for @amount.
  ///
  /// In en, this message translates to:
  /// **'Amount'**
  String get amount;

  /// No description provided for @tag.
  ///
  /// In en, this message translates to:
  /// **'Category'**
  String get tag;

  /// No description provided for @description.
  ///
  /// In en, this message translates to:
  /// **'Description'**
  String get description;

  /// No description provided for @descriptionRequired.
  ///
  /// In en, this message translates to:
  /// **'Description is required'**
  String get descriptionRequired;

  /// No description provided for @tagRequired.
  ///
  /// In en, this message translates to:
  /// **'Category is required'**
  String get tagRequired;

  /// No description provided for @amountRequired.
  ///
  /// In en, this message translates to:
  /// **'Amount is required'**
  String get amountRequired;

  /// No description provided for @beneficiaryRequired.
  ///
  /// In en, this message translates to:
  /// **'Beneficiary is required'**
  String get beneficiaryRequired;

  /// No description provided for @notifications.
  ///
  /// In en, this message translates to:
  /// **'Notifications'**
  String get notifications;

  /// No description provided for @darkMode.
  ///
  /// In en, this message translates to:
  /// **'Dark Mode'**
  String get darkMode;

  /// No description provided for @save.
  ///
  /// In en, this message translates to:
  /// **'Save'**
  String get save;

  /// No description provided for @signIn.
  ///
  /// In en, this message translates to:
  /// **'Sign in'**
  String get signIn;

  /// No description provided for @loginToAccount.
  ///
  /// In en, this message translates to:
  /// **'Login'**
  String get loginToAccount;

  /// No description provided for @signUp.
  ///
  /// In en, this message translates to:
  /// **'Sign up'**
  String get signUp;

  /// No description provided for @createNewAccount.
  ///
  /// In en, this message translates to:
  /// **'Create New'**
  String get createNewAccount;

  /// No description provided for @resetPassword.
  ///
  /// In en, this message translates to:
  /// **'Reset Password'**
  String get resetPassword;

  /// No description provided for @resetPasswordCode.
  ///
  /// In en, this message translates to:
  /// **'Enter code to reset your password'**
  String get resetPasswordCode;

  /// No description provided for @changePassword.
  ///
  /// In en, this message translates to:
  /// **'Change Password'**
  String get changePassword;

  /// No description provided for @change.
  ///
  /// In en, this message translates to:
  /// **'Change'**
  String get change;

  /// No description provided for @changePasswordSuccess.
  ///
  /// In en, this message translates to:
  /// **'Password has been changed'**
  String get changePasswordSuccess;

  /// No description provided for @username.
  ///
  /// In en, this message translates to:
  /// **'Full Name'**
  String get username;

  /// No description provided for @name.
  ///
  /// In en, this message translates to:
  /// **'Full Name'**
  String get name;

  /// No description provided for @password.
  ///
  /// In en, this message translates to:
  /// **'Password'**
  String get password;

  /// No description provided for @confirmPassword.
  ///
  /// In en, this message translates to:
  /// **'Confirm Password'**
  String get confirmPassword;

  /// No description provided for @email.
  ///
  /// In en, this message translates to:
  /// **'Email'**
  String get email;

  /// No description provided for @code.
  ///
  /// In en, this message translates to:
  /// **'Code'**
  String get code;

  /// No description provided for @codeNotValid.
  ///
  /// In en, this message translates to:
  /// **'Reset Code Is Not Valid'**
  String get codeNotValid;

  /// No description provided for @phone.
  ///
  /// In en, this message translates to:
  /// **'Phone Number'**
  String get phone;

  /// No description provided for @forgotPassword.
  ///
  /// In en, this message translates to:
  /// **'Forgot Password ?'**
  String get forgotPassword;

  /// No description provided for @changeLanguage.
  ///
  /// In en, this message translates to:
  /// **'Change Language'**
  String get changeLanguage;

  /// No description provided for @incorrectData.
  ///
  /// In en, this message translates to:
  /// **'The data you entered are incorrect'**
  String get incorrectData;

  /// No description provided for @typeToReset.
  ///
  /// In en, this message translates to:
  /// **'Please Type Your Phone Number To Reset Your Password'**
  String get typeToReset;

  /// No description provided for @noAccount.
  ///
  /// In en, this message translates to:
  /// **'Don\'t have an account ?'**
  String get noAccount;

  /// No description provided for @haveAccount.
  ///
  /// In en, this message translates to:
  /// **'Already have an account ?'**
  String get haveAccount;

  /// No description provided for @registerNow.
  ///
  /// In en, this message translates to:
  /// **'Register Now'**
  String get registerNow;

  /// No description provided for @verifyMobile.
  ///
  /// In en, this message translates to:
  /// **'Verify Mobile'**
  String get verifyMobile;

  /// No description provided for @verify.
  ///
  /// In en, this message translates to:
  /// **'Verify'**
  String get verify;

  /// No description provided for @notReceived.
  ///
  /// In en, this message translates to:
  /// **'Did not receive the code ?'**
  String get notReceived;

  /// No description provided for @mobileVerificationTitle.
  ///
  /// In en, this message translates to:
  /// **'Mobile Verification'**
  String get mobileVerificationTitle;

  /// No description provided for @mobileVerificationMsg.
  ///
  /// In en, this message translates to:
  /// **'A message containing an activation code has been sent to your mobile'**
  String get mobileVerificationMsg;

  /// No description provided for @register.
  ///
  /// In en, this message translates to:
  /// **'New Account'**
  String get register;

  /// No description provided for @terms.
  ///
  /// In en, this message translates to:
  /// **'Terms And Conditions'**
  String get terms;

  /// No description provided for @logout.
  ///
  /// In en, this message translates to:
  /// **'Logout'**
  String get logout;

  /// No description provided for @logoutConfirm.
  ///
  /// In en, this message translates to:
  /// **'Are you sure you want to log out ?'**
  String get logoutConfirm;

  /// No description provided for @currentPassword.
  ///
  /// In en, this message translates to:
  /// **'Current Password'**
  String get currentPassword;

  /// No description provided for @newPassword.
  ///
  /// In en, this message translates to:
  /// **'New Password'**
  String get newPassword;

  /// No description provided for @confirmNewPassword.
  ///
  /// In en, this message translates to:
  /// **'Confirm New Password'**
  String get confirmNewPassword;

  /// No description provided for @sendResetCode.
  ///
  /// In en, this message translates to:
  /// **'Send reset code'**
  String get sendResetCode;

  /// No description provided for @value.
  ///
  /// In en, this message translates to:
  /// **'Value'**
  String get value;

  /// No description provided for @send.
  ///
  /// In en, this message translates to:
  /// **'Send'**
  String get send;

  /// No description provided for @view.
  ///
  /// In en, this message translates to:
  /// **'View'**
  String get view;

  /// No description provided for @approved.
  ///
  /// In en, this message translates to:
  /// **'Approved'**
  String get approved;

  /// No description provided for @rejected.
  ///
  /// In en, this message translates to:
  /// **'Rejected'**
  String get rejected;

  /// No description provided for @canceled.
  ///
  /// In en, this message translates to:
  /// **'Canceled'**
  String get canceled;

  /// No description provided for @pending.
  ///
  /// In en, this message translates to:
  /// **'Pending'**
  String get pending;

  /// No description provided for @skip.
  ///
  /// In en, this message translates to:
  /// **'Skip'**
  String get skip;

  /// No description provided for @next.
  ///
  /// In en, this message translates to:
  /// **'Next'**
  String get next;

  /// No description provided for @finish.
  ///
  /// In en, this message translates to:
  /// **'Finish'**
  String get finish;

  /// No description provided for @search.
  ///
  /// In en, this message translates to:
  /// **'Search'**
  String get search;

  /// No description provided for @more.
  ///
  /// In en, this message translates to:
  /// **'More'**
  String get more;

  /// No description provided for @passwordChanged.
  ///
  /// In en, this message translates to:
  /// **'successfully changed password'**
  String get passwordChanged;

  /// No description provided for @fromDate.
  ///
  /// In en, this message translates to:
  /// **'From date'**
  String get fromDate;

  /// No description provided for @toDate.
  ///
  /// In en, this message translates to:
  /// **'To date'**
  String get toDate;

  /// No description provided for @searchAndSort.
  ///
  /// In en, this message translates to:
  /// **'search and sort'**
  String get searchAndSort;

  /// No description provided for @largestAmount.
  ///
  /// In en, this message translates to:
  /// **'Largest amount'**
  String get largestAmount;

  /// No description provided for @smallestAmount.
  ///
  /// In en, this message translates to:
  /// **'Smallest amount'**
  String get smallestAmount;

  /// No description provided for @apply.
  ///
  /// In en, this message translates to:
  /// **'Apply'**
  String get apply;

  /// No description provided for @searchCountry.
  ///
  /// In en, this message translates to:
  /// **'search a country'**
  String get searchCountry;

  /// No description provided for @reset.
  ///
  /// In en, this message translates to:
  /// **'Reset'**
  String get reset;

  /// No description provided for @verified.
  ///
  /// In en, this message translates to:
  /// **'Verified'**
  String get verified;

  /// No description provided for @done.
  ///
  /// In en, this message translates to:
  /// **'Done'**
  String get done;

  /// No description provided for @phoneChanged.
  ///
  /// In en, this message translates to:
  /// **'Your phone number has been changed successfully.'**
  String get phoneChanged;

  /// No description provided for @all.
  ///
  /// In en, this message translates to:
  /// **'All'**
  String get all;

  /// No description provided for @newUpdateKey.
  ///
  /// In en, this message translates to:
  /// **'New update'**
  String get newUpdateKey;

  /// No description provided for @updateAppKey.
  ///
  /// In en, this message translates to:
  /// **'update'**
  String get updateAppKey;

  /// No description provided for @successfullySaved.
  ///
  /// In en, this message translates to:
  /// **'Successfully Saved'**
  String get successfullySaved;

  /// No description provided for @pleaseSelectBranch.
  ///
  /// In en, this message translates to:
  /// **'please select branch'**
  String get pleaseSelectBranch;

  /// No description provided for @sessionDeliverFrom.
  ///
  /// In en, this message translates to:
  /// **'session deliver from'**
  String get sessionDeliverFrom;

  /// No description provided for @pettyCashAmount.
  ///
  /// In en, this message translates to:
  /// **'Petty Cash Amount'**
  String get pettyCashAmount;

  /// No description provided for @totalCashAmount.
  ///
  /// In en, this message translates to:
  /// **'Total Cash Amount'**
  String get totalCashAmount;

  /// No description provided for @variance.
  ///
  /// In en, this message translates to:
  /// **'Variance'**
  String get variance;

  /// No description provided for @justification.
  ///
  /// In en, this message translates to:
  /// **'Justification'**
  String get justification;

  /// No description provided for @continueSession.
  ///
  /// In en, this message translates to:
  /// **'continue session'**
  String get continueSession;

  /// No description provided for @startSession.
  ///
  /// In en, this message translates to:
  /// **'start session'**
  String get startSession;

  /// No description provided for @payment.
  ///
  /// In en, this message translates to:
  /// **'Payment'**
  String get payment;

  /// No description provided for @paidAmount.
  ///
  /// In en, this message translates to:
  /// **'Paid amount'**
  String get paidAmount;

  /// No description provided for @remaining.
  ///
  /// In en, this message translates to:
  /// **'Remaining'**
  String get remaining;

  /// No description provided for @printLang.
  ///
  /// In en, this message translates to:
  /// **'Print language'**
  String get printLang;

  /// No description provided for @ar.
  ///
  /// In en, this message translates to:
  /// **'AR'**
  String get ar;

  /// No description provided for @en.
  ///
  /// In en, this message translates to:
  /// **'EN'**
  String get en;

  /// No description provided for @fr.
  ///
  /// In en, this message translates to:
  /// **'FR'**
  String get fr;

  /// No description provided for @orderCount.
  ///
  /// In en, this message translates to:
  /// **'Count'**
  String get orderCount;

  /// No description provided for @payableTotal.
  ///
  /// In en, this message translates to:
  /// **'Paid'**
  String get payableTotal;

  /// No description provided for @totalBeforeDiscount.
  ///
  /// In en, this message translates to:
  /// **'Total without tax'**
  String get totalBeforeDiscount;

  /// No description provided for @totalIncTax.
  ///
  /// In en, this message translates to:
  /// **'With tax'**
  String get totalIncTax;

  /// No description provided for @totalAddons.
  ///
  /// In en, this message translates to:
  /// **'Total addons'**
  String get totalAddons;

  /// No description provided for @totalOutTax.
  ///
  /// In en, this message translates to:
  /// **'Without tax'**
  String get totalOutTax;

  /// No description provided for @receivingProducts.
  ///
  /// In en, this message translates to:
  /// **'Receiving Products'**
  String get receivingProducts;

  /// No description provided for @selectBranch.
  ///
  /// In en, this message translates to:
  /// **'Select Branch'**
  String get selectBranch;

  /// No description provided for @selectReceivingDate.
  ///
  /// In en, this message translates to:
  /// **'Select Receiving Date'**
  String get selectReceivingDate;

  /// No description provided for @selectSupplier.
  ///
  /// In en, this message translates to:
  /// **'Select Supplier'**
  String get selectSupplier;

  /// No description provided for @start.
  ///
  /// In en, this message translates to:
  /// **'Start'**
  String get start;

  /// No description provided for @availableQuantity.
  ///
  /// In en, this message translates to:
  /// **'Available Quantity'**
  String get availableQuantity;

  /// No description provided for @orderQuantity.
  ///
  /// In en, this message translates to:
  /// **'Order Quantity'**
  String get orderQuantity;

  /// No description provided for @preservedQuantity.
  ///
  /// In en, this message translates to:
  /// **'Preserved Quantity'**
  String get preservedQuantity;

  /// No description provided for @purchasingPrice.
  ///
  /// In en, this message translates to:
  /// **'Purchasing Price'**
  String get purchasingPrice;

  /// No description provided for @lastPurchasingPrice.
  ///
  /// In en, this message translates to:
  /// **'Last Purchasing Price'**
  String get lastPurchasingPrice;

  /// No description provided for @expiryDate.
  ///
  /// In en, this message translates to:
  /// **'Expiry Date'**
  String get expiryDate;

  /// No description provided for @branch.
  ///
  /// In en, this message translates to:
  /// **'Branches'**
  String get branch;

  /// No description provided for @supplier.
  ///
  /// In en, this message translates to:
  /// **'Supplier'**
  String get supplier;

  /// No description provided for @receivingDate.
  ///
  /// In en, this message translates to:
  /// **'Receiving Date'**
  String get receivingDate;

  /// No description provided for @searchByNameOrBarcode.
  ///
  /// In en, this message translates to:
  /// **'Search By Name Or Barcode'**
  String get searchByNameOrBarcode;

  /// No description provided for @selectReceivingOrderStatus.
  ///
  /// In en, this message translates to:
  /// **'Select Receiving Order Status'**
  String get selectReceivingOrderStatus;

  /// No description provided for @itemColor.
  ///
  /// In en, this message translates to:
  /// **'Color'**
  String get itemColor;

  /// No description provided for @itemSize.
  ///
  /// In en, this message translates to:
  /// **'Size'**
  String get itemSize;

  /// No description provided for @isPriceWithTax.
  ///
  /// In en, this message translates to:
  /// **'price include tax ?'**
  String get isPriceWithTax;

  /// No description provided for @barcode.
  ///
  /// In en, this message translates to:
  /// **'Barcode'**
  String get barcode;

  /// No description provided for @month.
  ///
  /// In en, this message translates to:
  /// **'Month'**
  String get month;

  /// No description provided for @year.
  ///
  /// In en, this message translates to:
  /// **'Year'**
  String get year;

  /// No description provided for @quarters.
  ///
  /// In en, this message translates to:
  /// **'Quarters'**
  String get quarters;

  /// No description provided for @purchaseTax.
  ///
  /// In en, this message translates to:
  /// **'Purchase tax'**
  String get purchaseTax;

  /// No description provided for @salesTax.
  ///
  /// In en, this message translates to:
  /// **'Sales tax'**
  String get salesTax;

  /// No description provided for @difference.
  ///
  /// In en, this message translates to:
  /// **'Difference'**
  String get difference;

  /// No description provided for @detailedReport.
  ///
  /// In en, this message translates to:
  /// **'Detailed report'**
  String get detailedReport;

  /// No description provided for @taxDetails.
  ///
  /// In en, this message translates to:
  /// **'Tax details'**
  String get taxDetails;

  /// No description provided for @invoiceNumber.
  ///
  /// In en, this message translates to:
  /// **'Invoice Number'**
  String get invoiceNumber;

  /// No description provided for @invoiceDate.
  ///
  /// In en, this message translates to:
  /// **'Invoice Date'**
  String get invoiceDate;

  /// No description provided for @invoiceAmountWithTax.
  ///
  /// In en, this message translates to:
  /// **'Amount With Tax'**
  String get invoiceAmountWithTax;

  /// No description provided for @invoiceAmountWithoutTax.
  ///
  /// In en, this message translates to:
  /// **'Amount Without Tax'**
  String get invoiceAmountWithoutTax;

  /// No description provided for @taxAmount.
  ///
  /// In en, this message translates to:
  /// **'Tax Amount'**
  String get taxAmount;

  /// No description provided for @purchaseSales.
  ///
  /// In en, this message translates to:
  /// **'Purchase and Sales tax'**
  String get purchaseSales;

  /// No description provided for @priceWithTax.
  ///
  /// In en, this message translates to:
  /// **'Price with tax'**
  String get priceWithTax;

  /// No description provided for @priceWithoutTax.
  ///
  /// In en, this message translates to:
  /// **'Price without tax'**
  String get priceWithoutTax;

  /// No description provided for @taxNumber.
  ///
  /// In en, this message translates to:
  /// **'Tax number'**
  String get taxNumber;

  /// No description provided for @discount.
  ///
  /// In en, this message translates to:
  /// **'Discount'**
  String get discount;

  /// No description provided for @pos.
  ///
  /// In en, this message translates to:
  /// **'Point of Sale'**
  String get pos;

  /// No description provided for @itemsInTheCart.
  ///
  /// In en, this message translates to:
  /// **'Items In The Cart :-'**
  String get itemsInTheCart;

  /// No description provided for @deliveryEmployee.
  ///
  /// In en, this message translates to:
  /// **'Delivery Employee'**
  String get deliveryEmployee;

  /// No description provided for @ordersPage.
  ///
  /// In en, this message translates to:
  /// **'Orders'**
  String get ordersPage;

  /// No description provided for @filter.
  ///
  /// In en, this message translates to:
  /// **'Search By Order Number'**
  String get filter;

  /// No description provided for @salesOfOrders.
  ///
  /// In en, this message translates to:
  /// **'Sales Of Orders'**
  String get salesOfOrders;

  /// No description provided for @scheduleOrders.
  ///
  /// In en, this message translates to:
  /// **'Schedule Orders'**
  String get scheduleOrders;

  /// No description provided for @revenu.
  ///
  /// In en, this message translates to:
  /// **'Revenu'**
  String get revenu;

  /// No description provided for @canceledOrders.
  ///
  /// In en, this message translates to:
  /// **'Canceled Orders'**
  String get canceledOrders;

  /// No description provided for @points.
  ///
  /// In en, this message translates to:
  /// **'Points'**
  String get points;

  /// No description provided for @customerHistory.
  ///
  /// In en, this message translates to:
  /// **'Customer History'**
  String get customerHistory;

  /// No description provided for @orderItems.
  ///
  /// In en, this message translates to:
  /// **'Order Items'**
  String get orderItems;

  /// No description provided for @tax.
  ///
  /// In en, this message translates to:
  /// **'Tax'**
  String get tax;

  /// No description provided for @price.
  ///
  /// In en, this message translates to:
  /// **'Price'**
  String get price;

  /// No description provided for @total.
  ///
  /// In en, this message translates to:
  /// **'Total'**
  String get total;

  /// No description provided for @returnCount.
  ///
  /// In en, this message translates to:
  /// **'Count'**
  String get returnCount;

  /// No description provided for @totalReturnedNet.
  ///
  /// In en, this message translates to:
  /// **'total net returned amount'**
  String get totalReturnedNet;

  /// No description provided for @size.
  ///
  /// In en, this message translates to:
  /// **'Size'**
  String get size;

  /// No description provided for @color.
  ///
  /// In en, this message translates to:
  /// **'Color'**
  String get color;

  /// No description provided for @totalAfterDiscount.
  ///
  /// In en, this message translates to:
  /// **'Total After Discount'**
  String get totalAfterDiscount;

  /// No description provided for @totalPayingAfterDiscount.
  ///
  /// In en, this message translates to:
  /// **'Paying After Discount'**
  String get totalPayingAfterDiscount;

  /// No description provided for @totalSellingAfterDiscount.
  ///
  /// In en, this message translates to:
  /// **'Selling After Discount'**
  String get totalSellingAfterDiscount;

  /// No description provided for @totalSellingPrice.
  ///
  /// In en, this message translates to:
  /// **'Total Selling Price'**
  String get totalSellingPrice;

  /// No description provided for @totalPayingPrice.
  ///
  /// In en, this message translates to:
  /// **'Total Paying price'**
  String get totalPayingPrice;

  /// No description provided for @expectedProfit.
  ///
  /// In en, this message translates to:
  /// **'Expected Profit'**
  String get expectedProfit;

  /// No description provided for @returnOrder.
  ///
  /// In en, this message translates to:
  /// **'Return Order'**
  String get returnOrder;

  /// No description provided for @quantity.
  ///
  /// In en, this message translates to:
  /// **'Quantity'**
  String get quantity;

  /// No description provided for @cancelOrder.
  ///
  /// In en, this message translates to:
  /// **'Cancel Order'**
  String get cancelOrder;

  /// No description provided for @externalOrder.
  ///
  /// In en, this message translates to:
  /// **'External Order'**
  String get externalOrder;

  /// No description provided for @internalOrder.
  ///
  /// In en, this message translates to:
  /// **'Internal Order'**
  String get internalOrder;

  /// No description provided for @orderNumber.
  ///
  /// In en, this message translates to:
  /// **'Order Number'**
  String get orderNumber;

  /// No description provided for @returnOrderNumber.
  ///
  /// In en, this message translates to:
  /// **'Return order number'**
  String get returnOrderNumber;

  /// No description provided for @customerName.
  ///
  /// In en, this message translates to:
  /// **'Customer Name'**
  String get customerName;

  /// No description provided for @customerSearch.
  ///
  /// In en, this message translates to:
  /// **'Search by phone number'**
  String get customerSearch;

  /// No description provided for @addProduct.
  ///
  /// In en, this message translates to:
  /// **'Add Product'**
  String get addProduct;

  /// No description provided for @customerMobile.
  ///
  /// In en, this message translates to:
  /// **'Customer Mobile'**
  String get customerMobile;

  /// No description provided for @preparedByName.
  ///
  /// In en, this message translates to:
  /// **'Prepared By Name'**
  String get preparedByName;

  /// No description provided for @deliveryMethod.
  ///
  /// In en, this message translates to:
  /// **'Delivery Method'**
  String get deliveryMethod;

  /// No description provided for @receiptNumber.
  ///
  /// In en, this message translates to:
  /// **'Receipt Number'**
  String get receiptNumber;

  /// No description provided for @servedBy.
  ///
  /// In en, this message translates to:
  /// **'Served By'**
  String get servedBy;

  /// No description provided for @driverName.
  ///
  /// In en, this message translates to:
  /// **'Driver Name'**
  String get driverName;

  /// No description provided for @paymentMethod.
  ///
  /// In en, this message translates to:
  /// **'Payment Method'**
  String get paymentMethod;

  /// No description provided for @status.
  ///
  /// In en, this message translates to:
  /// **'Status'**
  String get status;

  /// No description provided for @subTotal.
  ///
  /// In en, this message translates to:
  /// **'SubTotal'**
  String get subTotal;

  /// No description provided for @selectCustomer.
  ///
  /// In en, this message translates to:
  /// **'Please Select Customer'**
  String get selectCustomer;

  /// No description provided for @addOrder.
  ///
  /// In en, this message translates to:
  /// **'Add Order'**
  String get addOrder;

  /// No description provided for @editOrder.
  ///
  /// In en, this message translates to:
  /// **'Edit Order'**
  String get editOrder;

  /// No description provided for @orderDetails.
  ///
  /// In en, this message translates to:
  /// **'Order Details'**
  String get orderDetails;

  /// No description provided for @customer.
  ///
  /// In en, this message translates to:
  /// **'Customer'**
  String get customer;

  /// No description provided for @orderDate.
  ///
  /// In en, this message translates to:
  /// **'Order Date'**
  String get orderDate;

  /// No description provided for @orderIssuingDate.
  ///
  /// In en, this message translates to:
  /// **'Order Issuing Date'**
  String get orderIssuingDate;

  /// No description provided for @orderType.
  ///
  /// In en, this message translates to:
  /// **'Order Type'**
  String get orderType;

  /// No description provided for @branchName.
  ///
  /// In en, this message translates to:
  /// **'Branch Name'**
  String get branchName;

  /// No description provided for @totalItemsQty.
  ///
  /// In en, this message translates to:
  /// **'Total Items Quantity'**
  String get totalItemsQty;

  /// No description provided for @duplicateOrder.
  ///
  /// In en, this message translates to:
  /// **'Duplicate Order'**
  String get duplicateOrder;

  /// No description provided for @viewOrder.
  ///
  /// In en, this message translates to:
  /// **'View Order'**
  String get viewOrder;

  /// No description provided for @orderNotExists.
  ///
  /// In en, this message translates to:
  /// **'Order Not Exists'**
  String get orderNotExists;

  /// No description provided for @saveEdits.
  ///
  /// In en, this message translates to:
  /// **'save Edits'**
  String get saveEdits;

  /// No description provided for @prepared.
  ///
  /// In en, this message translates to:
  /// **'Prepared'**
  String get prepared;

  /// No description provided for @withDriver.
  ///
  /// In en, this message translates to:
  /// **'With Driver'**
  String get withDriver;

  /// No description provided for @delivered.
  ///
  /// In en, this message translates to:
  /// **'Delivered'**
  String get delivered;

  /// No description provided for @closeOrder.
  ///
  /// In en, this message translates to:
  /// **'Close Order'**
  String get closeOrder;

  /// No description provided for @orderProcess.
  ///
  /// In en, this message translates to:
  /// **'Process'**
  String get orderProcess;

  /// No description provided for @selectConsumerAddress.
  ///
  /// In en, this message translates to:
  /// **'Select Consumer Address'**
  String get selectConsumerAddress;

  /// No description provided for @moreInformation.
  ///
  /// In en, this message translates to:
  /// **'Delivery And More Information'**
  String get moreInformation;

  /// No description provided for @enterMoreInformation.
  ///
  /// In en, this message translates to:
  /// **'Write what you want here'**
  String get enterMoreInformation;

  /// No description provided for @newCustomer.
  ///
  /// In en, this message translates to:
  /// **'New Customer'**
  String get newCustomer;

  /// No description provided for @customerNotFound.
  ///
  /// In en, this message translates to:
  /// **'Customer not found'**
  String get customerNotFound;

  /// No description provided for @unit.
  ///
  /// In en, this message translates to:
  /// **'unit'**
  String get unit;

  /// No description provided for @id.
  ///
  /// In en, this message translates to:
  /// **'id'**
  String get id;

  /// No description provided for @print.
  ///
  /// In en, this message translates to:
  /// **'Print'**
  String get print;

  /// No description provided for @saveAndPrint.
  ///
  /// In en, this message translates to:
  /// **'Save And Print'**
  String get saveAndPrint;

  /// No description provided for @saveAndClose.
  ///
  /// In en, this message translates to:
  /// **'Save And Close'**
  String get saveAndClose;

  /// No description provided for @back.
  ///
  /// In en, this message translates to:
  /// **'Back'**
  String get back;

  /// No description provided for @emptyCart.
  ///
  /// In en, this message translates to:
  /// **'Empty Cart'**
  String get emptyCart;

  /// No description provided for @addItem.
  ///
  /// In en, this message translates to:
  /// **'Add Item'**
  String get addItem;

  /// No description provided for @country.
  ///
  /// In en, this message translates to:
  /// **'Country'**
  String get country;

  /// No description provided for @city.
  ///
  /// In en, this message translates to:
  /// **'City'**
  String get city;

  /// No description provided for @governorate.
  ///
  /// In en, this message translates to:
  /// **'Governorate'**
  String get governorate;

  /// No description provided for @street.
  ///
  /// In en, this message translates to:
  /// **'Street'**
  String get street;

  /// No description provided for @street2.
  ///
  /// In en, this message translates to:
  /// **'Street 2'**
  String get street2;

  /// No description provided for @postalCode.
  ///
  /// In en, this message translates to:
  /// **'Postal Code'**
  String get postalCode;

  /// No description provided for @building.
  ///
  /// In en, this message translates to:
  /// **'Building'**
  String get building;

  /// No description provided for @floor.
  ///
  /// In en, this message translates to:
  /// **'Floor'**
  String get floor;

  /// No description provided for @flatNumber.
  ///
  /// In en, this message translates to:
  /// **'Flat Number'**
  String get flatNumber;

  /// No description provided for @landMark.
  ///
  /// In en, this message translates to:
  /// **'Land Mark'**
  String get landMark;

  /// No description provided for @foodOrders.
  ///
  /// In en, this message translates to:
  /// **'Food'**
  String get foodOrders;

  /// No description provided for @shopNow.
  ///
  /// In en, this message translates to:
  /// **'Shop Now'**
  String get shopNow;

  /// No description provided for @restaurants.
  ///
  /// In en, this message translates to:
  /// **'Restaurants'**
  String get restaurants;

  /// No description provided for @mainPage.
  ///
  /// In en, this message translates to:
  /// **'Main'**
  String get mainPage;

  /// No description provided for @transportation.
  ///
  /// In en, this message translates to:
  /// **'transportation'**
  String get transportation;

  /// No description provided for @welcome.
  ///
  /// In en, this message translates to:
  /// **'welcome'**
  String get welcome;

  /// No description provided for @chooseService.
  ///
  /// In en, this message translates to:
  /// **'Please choose the service you want !'**
  String get chooseService;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['ar', 'en'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'ar': return AppLocalizationsAr();
    case 'en': return AppLocalizationsEn();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
