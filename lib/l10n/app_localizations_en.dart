import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get phoneKey => 'Enter your phone';

  @override
  String get passwordKey => 'Enter your password';

  @override
  String get accept => 'Accept';

  @override
  String get reject => 'Reject';

  @override
  String get edit => 'Edit';

  @override
  String get remove => 'Delete';

  @override
  String get cancel => 'Cancel';

  @override
  String get amount => 'Amount';

  @override
  String get tag => 'Category';

  @override
  String get description => 'Description';

  @override
  String get descriptionRequired => 'Description is required';

  @override
  String get tagRequired => 'Category is required';

  @override
  String get amountRequired => 'Amount is required';

  @override
  String get beneficiaryRequired => 'Beneficiary is required';

  @override
  String get notifications => 'Notifications';

  @override
  String get darkMode => 'Dark Mode';

  @override
  String get save => 'Save';

  @override
  String get signIn => 'Sign in';

  @override
  String get loginToAccount => 'Login';

  @override
  String get signUp => 'Sign up';

  @override
  String get createNewAccount => 'Create New';

  @override
  String get resetPassword => 'Reset Password';

  @override
  String get resetPasswordCode => 'Enter code to reset your password';

  @override
  String get changePassword => 'Change Password';

  @override
  String get change => 'Change';

  @override
  String get changePasswordSuccess => 'Password has been changed';

  @override
  String get username => 'Full Name';

  @override
  String get name => 'Full Name';

  @override
  String get password => 'Password';

  @override
  String get confirmPassword => 'Confirm Password';

  @override
  String get email => 'Email';

  @override
  String get code => 'Code';

  @override
  String get codeNotValid => 'Reset Code Is Not Valid';

  @override
  String get phone => 'Phone Number';

  @override
  String get forgotPassword => 'Forgot Password ?';

  @override
  String get changeLanguage => 'Change Language';

  @override
  String get incorrectData => 'The data you entered are incorrect';

  @override
  String get typeToReset => 'Please Type Your Phone Number To Reset Your Password';

  @override
  String get noAccount => 'Don\'t have an account ?';

  @override
  String get haveAccount => 'Already have an account ?';

  @override
  String get registerNow => 'Register Now';

  @override
  String get verifyMobile => 'Verify Mobile';

  @override
  String get verify => 'Verify';

  @override
  String get notReceived => 'Did not receive the code ?';

  @override
  String get mobileVerificationTitle => 'Mobile Verification';

  @override
  String get mobileVerificationMsg => 'A message containing an activation code has been sent to your mobile';

  @override
  String get register => 'New Account';

  @override
  String get terms => 'Terms And Conditions';

  @override
  String get logout => 'Logout';

  @override
  String get logoutConfirm => 'Are you sure you want to log out ?';

  @override
  String get currentPassword => 'Current Password';

  @override
  String get newPassword => 'New Password';

  @override
  String get confirmNewPassword => 'Confirm New Password';

  @override
  String get sendResetCode => 'Send reset code';

  @override
  String get value => 'Value';

  @override
  String get send => 'Send';

  @override
  String get view => 'View';

  @override
  String get approved => 'Approved';

  @override
  String get rejected => 'Rejected';

  @override
  String get canceled => 'Canceled';

  @override
  String get pending => 'Pending';

  @override
  String get skip => 'Skip';

  @override
  String get next => 'Next';

  @override
  String get finish => 'Finish';

  @override
  String get search => 'Search';

  @override
  String get more => 'More';

  @override
  String get passwordChanged => 'successfully changed password';

  @override
  String get fromDate => 'From date';

  @override
  String get toDate => 'To date';

  @override
  String get searchAndSort => 'search and sort';

  @override
  String get largestAmount => 'Largest amount';

  @override
  String get smallestAmount => 'Smallest amount';

  @override
  String get apply => 'Apply';

  @override
  String get searchCountry => 'search a country';

  @override
  String get reset => 'Reset';

  @override
  String get verified => 'Verified';

  @override
  String get done => 'Done';

  @override
  String get phoneChanged => 'Your phone number has been changed successfully.';

  @override
  String get all => 'All';

  @override
  String get newUpdateKey => 'New update';

  @override
  String get updateAppKey => 'update';

  @override
  String get successfullySaved => 'Successfully Saved';

  @override
  String get pleaseSelectBranch => 'please select branch';

  @override
  String get sessionDeliverFrom => 'session deliver from';

  @override
  String get pettyCashAmount => 'Petty Cash Amount';

  @override
  String get totalCashAmount => 'Total Cash Amount';

  @override
  String get variance => 'Variance';

  @override
  String get justification => 'Justification';

  @override
  String get continueSession => 'continue session';

  @override
  String get startSession => 'start session';

  @override
  String get payment => 'Payment';

  @override
  String get paidAmount => 'Paid amount';

  @override
  String get remaining => 'Remaining';

  @override
  String get printLang => 'Print language';

  @override
  String get ar => 'AR';

  @override
  String get en => 'EN';

  @override
  String get fr => 'FR';

  @override
  String get orderCount => 'Count';

  @override
  String get payableTotal => 'Paid';

  @override
  String get totalBeforeDiscount => 'Total without tax';

  @override
  String get totalIncTax => 'With tax';

  @override
  String get totalAddons => 'Total addons';

  @override
  String get totalOutTax => 'Without tax';

  @override
  String get receivingProducts => 'Receiving Products';

  @override
  String get selectBranch => 'Select Branch';

  @override
  String get selectReceivingDate => 'Select Receiving Date';

  @override
  String get selectSupplier => 'Select Supplier';

  @override
  String get start => 'Start';

  @override
  String get availableQuantity => 'Available Quantity';

  @override
  String get orderQuantity => 'Order Quantity';

  @override
  String get preservedQuantity => 'Preserved Quantity';

  @override
  String get purchasingPrice => 'Purchasing Price';

  @override
  String get lastPurchasingPrice => 'Last Purchasing Price';

  @override
  String get expiryDate => 'Expiry Date';

  @override
  String get branch => 'Branches';

  @override
  String get supplier => 'Supplier';

  @override
  String get receivingDate => 'Receiving Date';

  @override
  String get searchByNameOrBarcode => 'Search By Name Or Barcode';

  @override
  String get selectReceivingOrderStatus => 'Select Receiving Order Status';

  @override
  String get itemColor => 'Color';

  @override
  String get itemSize => 'Size';

  @override
  String get isPriceWithTax => 'price include tax ?';

  @override
  String get barcode => 'Barcode';

  @override
  String get month => 'Month';

  @override
  String get year => 'Year';

  @override
  String get quarters => 'Quarters';

  @override
  String get purchaseTax => 'Purchase tax';

  @override
  String get salesTax => 'Sales tax';

  @override
  String get difference => 'Difference';

  @override
  String get detailedReport => 'Detailed report';

  @override
  String get taxDetails => 'Tax details';

  @override
  String get invoiceNumber => 'Invoice Number';

  @override
  String get invoiceDate => 'Invoice Date';

  @override
  String get invoiceAmountWithTax => 'Amount With Tax';

  @override
  String get invoiceAmountWithoutTax => 'Amount Without Tax';

  @override
  String get taxAmount => 'Tax Amount';

  @override
  String get purchaseSales => 'Purchase and Sales tax';

  @override
  String get priceWithTax => 'Price with tax';

  @override
  String get priceWithoutTax => 'Price without tax';

  @override
  String get taxNumber => 'Tax number';

  @override
  String get discount => 'Discount';

  @override
  String get pos => 'Point of Sale';

  @override
  String get itemsInTheCart => 'Items In The Cart :-';

  @override
  String get deliveryEmployee => 'Delivery Employee';

  @override
  String get ordersPage => 'Orders';

  @override
  String get filter => 'Search By Order Number';

  @override
  String get salesOfOrders => 'Sales Of Orders';

  @override
  String get scheduleOrders => 'Schedule Orders';

  @override
  String get revenu => 'Revenu';

  @override
  String get canceledOrders => 'Canceled Orders';

  @override
  String get points => 'Points';

  @override
  String get customerHistory => 'Customer History';

  @override
  String get orderItems => 'Order Items';

  @override
  String get tax => 'Tax';

  @override
  String get price => 'Price';

  @override
  String get total => 'Total';

  @override
  String get returnCount => 'Count';

  @override
  String get totalReturnedNet => 'total net returned amount';

  @override
  String get size => 'Size';

  @override
  String get color => 'Color';

  @override
  String get totalAfterDiscount => 'Total After Discount';

  @override
  String get totalPayingAfterDiscount => 'Paying After Discount';

  @override
  String get totalSellingAfterDiscount => 'Selling After Discount';

  @override
  String get totalSellingPrice => 'Total Selling Price';

  @override
  String get totalPayingPrice => 'Total Paying price';

  @override
  String get expectedProfit => 'Expected Profit';

  @override
  String get returnOrder => 'Return Order';

  @override
  String get quantity => 'Quantity';

  @override
  String get cancelOrder => 'Cancel Order';

  @override
  String get externalOrder => 'External Order';

  @override
  String get internalOrder => 'Internal Order';

  @override
  String get orderNumber => 'Order Number';

  @override
  String get returnOrderNumber => 'Return order number';

  @override
  String get customerName => 'Customer Name';

  @override
  String get customerSearch => 'Search by phone number';

  @override
  String get addProduct => 'Add Product';

  @override
  String get customerMobile => 'Customer Mobile';

  @override
  String get preparedByName => 'Prepared By Name';

  @override
  String get deliveryMethod => 'Delivery Method';

  @override
  String get receiptNumber => 'Receipt Number';

  @override
  String get servedBy => 'Served By';

  @override
  String get driverName => 'Driver Name';

  @override
  String get paymentMethod => 'Payment Method';

  @override
  String get status => 'Status';

  @override
  String get subTotal => 'SubTotal';

  @override
  String get selectCustomer => 'Please Select Customer';

  @override
  String get addOrder => 'Add Order';

  @override
  String get editOrder => 'Edit Order';

  @override
  String get orderDetails => 'Order Details';

  @override
  String get customer => 'Customer';

  @override
  String get orderDate => 'Order Date';

  @override
  String get orderIssuingDate => 'Order Issuing Date';

  @override
  String get orderType => 'Order Type';

  @override
  String get branchName => 'Branch Name';

  @override
  String get totalItemsQty => 'Total Items Quantity';

  @override
  String get duplicateOrder => 'Duplicate Order';

  @override
  String get viewOrder => 'View Order';

  @override
  String get orderNotExists => 'Order Not Exists';

  @override
  String get saveEdits => 'save Edits';

  @override
  String get prepared => 'Prepared';

  @override
  String get withDriver => 'With Driver';

  @override
  String get delivered => 'Delivered';

  @override
  String get closeOrder => 'Close Order';

  @override
  String get orderProcess => 'Process';

  @override
  String get selectConsumerAddress => 'Select Consumer Address';

  @override
  String get moreInformation => 'Delivery And More Information';

  @override
  String get enterMoreInformation => 'Write what you want here';

  @override
  String get newCustomer => 'New Customer';

  @override
  String get customerNotFound => 'Customer not found';

  @override
  String get unit => 'unit';

  @override
  String get id => 'id';

  @override
  String get print => 'Print';

  @override
  String get saveAndPrint => 'Save And Print';

  @override
  String get saveAndClose => 'Save And Close';

  @override
  String get back => 'Back';

  @override
  String get emptyCart => 'Empty Cart';

  @override
  String get addItem => 'Add Item';

  @override
  String get country => 'Country';

  @override
  String get city => 'City';

  @override
  String get governorate => 'Governorate';

  @override
  String get street => 'Street';

  @override
  String get street2 => 'Street 2';

  @override
  String get postalCode => 'Postal Code';

  @override
  String get building => 'Building';

  @override
  String get floor => 'Floor';

  @override
  String get flatNumber => 'Flat Number';

  @override
  String get landMark => 'Land Mark';

  @override
  String get foodOrders => 'Food';

  @override
  String get shopNow => 'Shop Now';

  @override
  String get restaurants => 'Restaurants';

  @override
  String get mainPage => 'Main';

  @override
  String get transportation => 'transportation';

  @override
  String get welcome => 'welcome';

  @override
  String get chooseService => 'Please choose the service you want !';
}
