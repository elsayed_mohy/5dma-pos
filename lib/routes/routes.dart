import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:khdma_pos/logic/binding/auth_binding.dart';
import 'package:khdma_pos/logic/binding/food_pos_binding.dart';
import 'package:khdma_pos/logic/binding/view_order_binding.dart';
import 'package:khdma_pos/view/screens/auth/login_screen.dart';
import 'package:khdma_pos/view/screens/food_orders/food_order_method_screen.dart';
import 'package:khdma_pos/view/screens/orders/view_edit_order_screen.dart';
import 'package:khdma_pos/view/screens/pos/food_pos_screen.dart';
import 'package:khdma_pos/view/screens/pos/pos_screen.dart';
import 'package:khdma_pos/view/screens/pos/start_session_screen.dart';

import '../logic/binding/home_binding.dart';
import '../logic/binding/pos_binding.dart';
import '../view/screens/food_orders/tables_screen.dart';
import '../view/screens/food_orders/view_edit_food_order_screen.dart';
import '../view/screens/user_layout/user_layout.dart';


class AppRoutes {
  static const homeRoute = Routes.home;
  static const loginScreen = Routes.loginScreen;
  static const pos = Routes.pos;
  static const foodMethodScreen = Routes.selectFoodMethodScreen;
  static const viewOrder = Routes.viewOrder;
  static final routes = [
    GetPage(
      name: Routes.loginScreen,
      page: () => LoginScreen(),
      binding: AuthBinding()
    ),
    GetPage(
        name: Routes.home,
        page: () => UserLayout(),
        binding: HomeBinding(),
        children:[
          GetPage(name: Routes.pos, page: () => POSScreen()),
        ]
    ),
    GetPage(
        name: Routes.pos,
        page: () => POSScreen(),
        binding: POSBinding()
    ),
    GetPage(
        name: Routes.foodPos,
        page: () => FoodPOSScreen(),
        binding: FoodPOSBinding()
    ),
    GetPage(
        name: Routes.viewOrder,
        page: () => ViewEditOrderScreen(),
        binding: ViewOrderBinding()
    ),
    GetPage(
        name: Routes.viewFoodOrder,
        page: () => ViewEditFoodOrderScreen(),
        binding: ViewOrderBinding()
    ),
    GetPage(
        name: Routes.startOrdersSessionScreen,
        page: () => StartSessionScreen(food:false),
        binding: ViewOrderBinding()
    ),
    GetPage(
        name: Routes.startFoodSessionScreen,
        page: () => StartSessionScreen(food:true),
        binding: ViewOrderBinding()
    ),
    GetPage(
        name: Routes.tablesScreen,
        page: () => TablesLayoutScreen(),
    ),
    GetPage(
        name: Routes.selectFoodMethodScreen,
        page: () => FoodOrderMethodScreen(),
    ),

  ];
}

class Routes {
  static const loginScreen = "/loginScreen";
  static const home = "/home";
  static const pos = "/home/pos";
  static const foodPos = "/home/foodPos";
  static const viewOrder = "/home/viewOrder";
  static const viewFoodOrder = "/home/viewFoodOrder";
  static const startOrdersSessionScreen = "/home/startOrdersSessionScreen";
  static const startFoodSessionScreen = "/home/startFoodSessionScreen";
  static const selectFoodMethodScreen = "/home/selectFoodMethodScreen";
  static const tablesScreen = "/home/tables";
}
