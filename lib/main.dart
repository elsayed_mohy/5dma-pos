import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/services/sunmi/sunmi_service.dart';
import 'package:khdma_pos/utils/theme.dart';

import 'l10n/app_localizations.dart';
import 'routes/routes.dart';
import 'services/base_dio.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  BaseDio.initializeInterceptors();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(const MyApp());
}

String getDeviceLanguage() {
  return WidgetsBinding.instance.platformDispatcher.locale.languageCode;
}

String getCurrentLanguage() {
  return GetStorage().read<String>('lang') != null
      ? GetStorage().read<String>('lang').toString()
      : getDeviceLanguage();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final deviceLanguage = getDeviceLanguage();
    return GetMaterialApp(
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        localeResolutionCallback: localeCallBack,
        title: '5dma POS',
        locale: GetStorage().read<String>('lang') != null
            ? Locale(GetStorage().read<String>('lang').toString())
            : Locale(deviceLanguage),
        debugShowCheckedModeBanner: false,
        theme: ThemesManager().light,
        darkTheme: ThemesManager().dark,
        themeMode: ThemesManager().getThemeMode(),
        initialRoute: GetStorage().read('loggedIn') == true
            ? AppRoutes.homeRoute
            :AppRoutes.loginScreen ,
        getPages: AppRoutes.routes);
  }

  Locale localeCallBack(Locale? locale, Iterable<Locale> supportedLocales) {
    if (locale == null) {
      return supportedLocales.first;
    }

    for (var supportedLocale in supportedLocales) {
      if (locale.languageCode == supportedLocale.languageCode) {
        return supportedLocale;
      }
    }

    return supportedLocales.first;
  }
}
