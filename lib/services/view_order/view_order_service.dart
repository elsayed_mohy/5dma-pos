import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import "package:flutter/material.dart";
import 'package:image/image.dart' as im;
import 'package:khdma_pos/l10n/app_localizations.dart';
import 'package:khdma_pos/services/base_dio.dart';
import 'package:khdma_pos/services/sunmi/sunmi_service.dart';
import 'package:khdma_pos/utils/constants/app_urls.dart';
import 'package:open_app_file/open_app_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:printing/printing.dart';
import 'package:sunmi_printer_plus/sunmi_printer_plus.dart';
import 'package:pdfx/pdfx.dart';
import 'package:pdf/widgets.dart' as pw;

class ViewOrderService {
  final base = BaseDio();

  Future<String> downFile(
      String filename, dynamic orderId, dynamic taxNumber) async {
    String dir = (await getTemporaryDirectory()).path;
    File targetFile = File('$dir/$filename');

    Map<String, dynamic> requestBody = {
      "reportName": "pos_invoice_${AppLocalizations.supportedLocales.first}",
      "fileName": "pos_invoice_${AppLocalizations.supportedLocales.first}",
      "paramters": {"orderId": orderId, "taxNumber": taxNumber}
    };
    final response = await base.downLoad(
        url: '${AppUrls.reportEndPoint}', requestBody: jsonEncode(requestBody));
    if (response.statusCode == 200) {
    } else {
      return throw Exception("error while download box pdf ");
    }

    await targetFile.writeAsBytes(response.data);
    await pdfPageToImage(targetFile.path);
    return targetFile.path;
  }

  Future<void> openFile(String filePath) async {
    final result = await OpenAppFile.open(filePath);
    print(result.message);
  }

  printFile(dynamic data) async {
    await Sunmi().initialize();
    await SunmiPrinter.lineWrap(1); // creates one line space
    await SunmiPrinter.printImage(data);

    await SunmiPrinter.lineWrap(5);
    // await SunmiPrinter.printText("data");
    // await printLogoImage();
    // await SunmiPrinter.printText("Flutter is awesome");
    // await Sunmi().printRowAndColumns(
    //     column1: "Column 1", column2: "Column 2", column3: "Column 3");
    await SunmiPrinter.cut();
    // print("Reached ==>> 6");
    await Sunmi().closePrinter();
  }

  Future<void> pdfPageToImage(String pdfFilePath) async {
    final pdfDocument = await PdfDocument.openFile(pdfFilePath);
    final page = await pdfDocument.getPage(1);
    final pageImage = await page.render(
        width: page.width * 1.85,
        height: 1250,
        forPrint: true,
        backgroundColor: "#ffffff00");
    await page.close();
    await pdfDocument.close();
    // Image  image = Image.memory(pageImage!.bytes);
    // final imageData = await image.toByteData();
    await printFile(pageImage!.bytes);
  }

  Future<Uint8List> readFileBytes(File file) async {
    final bytes = await file.readAsBytes();
    return bytes;
  }

  Future<Image> pdfToImage(File pdfFile) async {
    final pdfData = await readFileBytes(pdfFile);
    final pages = await Printing.raster(pdfData);
    final imageData = await (await pages.first).asImage();
    return Image.memory(imageData.buffer.asUint8List());
  }
}
