import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:khdma_pos/data/model/order/order_details_model.dart';
import 'package:khdma_pos/services/base_dio.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import '../../utils/constants/app_urls.dart';

class OrderStatusService {
  final base = BaseDio();

  Future<Status> getOrderStatusByCode(
      {required String statusCode}) async {

    Response response = await base.get(
        url: "${AppUrls.orderStatusByCodeEndPoint}$statusCode");
    var res = response.data;
    return Status.fromJson(res);
  }

  Future<Status> changeStatus(order ,int? branchEmployeeId) async {
    Response response = await  base.put( url: "${AppUrls.issuingOrderEndPoint}/status${branchEmployeeId != null ? '?branchEmployeeId=$branchEmployeeId' : ''}", requestBody: json.encode(order.toJson()));
     var res = response.data;
     return Status.fromJson(res);
  }
}
