import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/data/model/common/branch_employee_model.dart';
import 'package:khdma_pos/data/model/common/walking_consumer_v2_model.dart';
import 'package:khdma_pos/data/model/order/branch_category.dart';
import 'package:khdma_pos/data/model/order/consumer.dart';
import 'package:khdma_pos/data/model/order/order_details_model.dart';
import 'package:khdma_pos/data/model/order/order_model.dart';
import 'package:khdma_pos/data/model/order/order_search_criteria_model.dart';
import 'package:khdma_pos/data/model/order/return_order_details_model.dart';
import 'package:khdma_pos/data/model/session_model.dart';

import '../../data/model/common/delivery_method_model.dart';
import '../../data/model/common/walking_consumer_model.dart';
import '../../data/model/order/branch_item.dart';
import '../../utils/constants/app_urls.dart';
import '../base_dio.dart';

class OrdersService extends GetxService {
  final base = BaseDio();
  List<BranchItem> selectedItems = [];
  Session? sessionData;


  setSelectedItems(List<BranchItem> items) {
    selectedItems = items;
  }

  setSessionData(Session session) {
    sessionData = session;
  }

  Future<List<BranchItem>> getBranchItems(
      {required int branchId, int? categoryId, int? subCategoryId}) async {
    Map<String, dynamic> queryParameters = {};
    queryParameters.addIf(categoryId != null, "categoryId", categoryId);
    queryParameters.addIf(
        subCategoryId != null, "subCategoryId", subCategoryId);
    Response response = await base.get(
        url: "${AppUrls.branchItemsEndPoint}$branchId",
        queryParameters: queryParameters);
    var branchItems = response.data['data'] as List;
    await GetStorage().write('branch_items', branchItems);
    return branchItems
        .map((branchItem) => BranchItem.fromJson(branchItem))
        .toList();
  }

  Future<List<DeliveryMethod>> getBranchDeliveryMethods( branchId) async {
    Response response = await base.get(
        url: "${AppUrls.deliveryMethodsEndPoint}$branchId");
    print(response.data['data']);
    var res = response.data['data']['deliveryMethods'] as List;
    return res
        .map((method) => DeliveryMethod.fromJson(method))
        .toList();
  }

  Future<OrderDetails> saveOrder(order) async {
    print("order $order");
    Response response = await base.post( url: AppUrls.issuingOrderEndPoint, requestBody: json.encode(order));
    var res = response.data;
    return OrderDetails.fromJson(res);
  }

   updateOrder(order) async {
    Response response = await base.put( url: AppUrls.issuingOrderEndPoint, requestBody: json.encode(order.toJson()));
    var res = response.data["data"];
    return res;
  }

  Future<List<ReturnOrderDetails>> getReturnOrderDetails(orderId) async {
    Response response = await base.get(
        url: "${AppUrls.returnOrderDetailsEndPoint}$orderId");
    var res = response.data['data'] as List;
    return res
        .map((returnOrder) => ReturnOrderDetails.fromJson(returnOrder))
        .toList();
  }

  Future<List<BranchCategory>> getBranchCategories(
      {required int branchId}) async {
    Map<String, dynamic> queryParameters = {};

    Response response = await base.get(
        url: "${AppUrls.branchCategoriesEndPoint}$branchId",
        queryParameters: queryParameters);
    var branchCategories = response.data['data'] as List;
    return branchCategories
        .map((branchCategory) => BranchCategory.fromJson(branchCategory))
        .toList();
  }

  Future<OrderDetails> getOrderDetails(
      {required String orderId}) async {

    Response response = await base.get(
        url: "${AppUrls.issuingOrderDetailsEndPoint}$orderId");
    var res = response.data;
    return OrderDetails.fromJson(res);
  }

  Future<List<Consumer>> getConsumerBySearch(
      {required String searchValue}) async {

    Response response = await base.get(
        url: "${AppUrls.consumerDetailsEndPoint}$searchValue");
    var res = response.data as List;
    return res
        .map((consumer) => Consumer.fromJson(consumer))
        .toList();
  }

  Future<List<OrderModel>> filterOrders(
      {required OrderSearchCriteria requestBody}) async {

    Response response = await base.post(
        url: "${AppUrls.issuingOrdersEndPoint}",
        requestBody: orderSearchCriteriaToJson(requestBody));
    var ordersList = response.data['data']['content'] as List;
    return ordersList
        .map((order) => OrderModel.fromJson(order))
        .toList();
  }

  Future<List<BranchEmployee>> getDrivers(branchId) async {
    Response response = await base.get(
        url: "${AppUrls.driversEndPoint}$branchId");
    print("drivers ${response.data}");
    var res = response.data['data'] as List;
    return res
        .map((employee) => BranchEmployee.fromJson(employee))
        .toList();
  }

  Future<List<WalkingConsumer>> getConsumerByPhoneNumber(String phoneNumber) async {
    Response response = await base.get(
        url: "${AppUrls.consumersEndPoint}$phoneNumber");
    var res = response.data['data'] as List;
    return res
        .map((consumer) => WalkingConsumer.fromJson(consumer))
        .toList();
  }

  Future<WalkingConsumerV2> createWalkingConsumer( requestBody) async {
    Response response = await base.post(
        url: "${AppUrls.addConsumerEndPoint}",
        requestBody: requestBody);
    var res = response.data['data'];
    return WalkingConsumerV2.fromJson(res);
  }

}
