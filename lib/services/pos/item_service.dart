import 'package:khdma_pos/data/model/order/branch_item.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import '../../data/model/order/item_size_model.dart';
import '../../utils/constants/app_urls.dart';
import '../base_dio.dart';

class ItemService {
  final base = BaseDio();

  Future<List<BaseUnit>> getUnits() async {
    Response response = await base.get(
        url: "${AppUrls.unitsEndPoint}");
    var units = response.data['data'] as List;
    return units
        .map((unit) => BaseUnit.fromJson(unit))
        .toList();
  }
  Future<List<Color>> getItemColors(mainBranchItemId) async {
    Response response = await base.get(
        url: "${AppUrls.itemColorsEndPoint}",
    queryParameters: {"mainBranchItemId":mainBranchItemId});
    var colors = response.data['data'] as List;
    return colors
        .map((color) => Color.fromJson(color))
        .toList();
  }

  Future<List<ItemSize>> getItemSizes(mainBranchItemId,colorId) async {
    Response response = await base.get(
        url: "${AppUrls.itemSizesEndPoint}?mainBranchItemId=$mainBranchItemId&colorId=${colorId}",
);
    print(response.data);
    var sizes = response.data['data'] as List;
    return sizes
        .map((size) => ItemSize.fromJson(size))
        .toList();
  }
}