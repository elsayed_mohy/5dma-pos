import '../../data/model/session_model.dart';
import '../../utils/constants/app_urls.dart';
import '../base_dio.dart';
import 'package:dio/dio.dart';

class SessionService {
  final base = BaseDio();


  Future getLastSessionByUser() async{
   final  response = await base.get(url: AppUrls.lastSessionByUserEndPoint);
    return response;
  }
  Future<Session> getLastSessionFromUserId(int fromUserId) async{
    Response response = await base.get(url: AppUrls.lastClosedSessionEndPoint,queryParameters: {"fromUserId":fromUserId});
    return Session.fromJson(response.data["data"]);
  }

  Future<Session> startSession({required sessionData}) async {
    Response response = await base.post(url: AppUrls.startSessionEndPoint, requestBody: sessionToJson(sessionData));
    return Session.fromJson(response.data["data"]);
  }

}