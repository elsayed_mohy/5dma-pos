import 'package:flutter/services.dart';

class MaxValueFormatter extends TextInputFormatter {
  final double maxValue;

  MaxValueFormatter(this.maxValue);

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue,
      TextEditingValue newValue,
      ) {
    // Parse the input value as an integer
    int? inputValue = int.tryParse(newValue.text);

    // Check if the parsed value is greater than the max value
    if (inputValue != null && inputValue > maxValue) {
      // If it is, limit the value to the max value
      return TextEditingValue(
        text: '$maxValue',
        selection: TextSelection.collapsed(offset: '$maxValue'.length),
      );
    }

    // Otherwise, allow the input value
    return newValue;
  }
}