
import 'package:dio/dio.dart';

import '../../data/model/branch_user_model.dart';
import '../../data/model/customer_branch_model.dart';
import '../../utils/constants/app_urls.dart';
import '../base_dio.dart';

class CustomerService {
  final base = BaseDio();

  Future<List<dynamic>> getCustomerBranches() async{
    Response response = await base.get(url: AppUrls.customerBranchesEndPoint);
    var branches =  response.data['data'] as List;
    return branches;
  }
  Future<List<BranchUser>> getBranchUsers(branchId) async{
    Response response = await base.get(url: '${AppUrls.branchUsersEndPoint}$branchId',);
    var users =  response.data as List;
    return users.map((user)=> BranchUser.fromJson(user)).toList();
  }
}