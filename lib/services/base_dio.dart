
import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/services/auth_service.dart';

import '../data/model/user.dart';
import '../main.dart';
import '../utils/constants/app_urls.dart';
import 'base_service.dart';

class BaseDio {
  dynamic userAgent;

  static final dio = Dio(
    BaseOptions(baseUrl: AppUrls.apiUrl, receiveDataWhenStatusError: true),
  );

  static void initializeInterceptors() {
    final GetStorage storage = GetStorage();


    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (request, handler) async{
        User user = userFromJson(GetStorage().read("user"));
        var headers = {
          'Accept': "application/json",
          'Authorization': 'Bearer ${user.accessToken}',
          'accept-language': getCurrentLanguage()
        };
        request.headers.addAll(headers);
        print(request.path);
        return handler.next(request);
      },
      onResponse: (response, handler) {
        return handler.next(response);
      },
      onError: ( error, handler) async {
        User user = userFromJson(GetStorage().read("user"));
        if (error.response?.statusCode == 401) {
          if (user.refreshToken != null) {
            Response<dynamic> response = await dio.post(
                "${AppUrls.apiUrl}api/v1/auth/refresh-access-token",
                data: {
                  'refreshToken': user.refreshToken,
                  'attendToken': false,
                });
            if (response.statusCode == 200) {
              if (response.data['data'] != null) {
                user.accessToken = response.data['data']['jwt'];
                storage.write('user', userToJson(user));
                RequestOptions options = error.requestOptions;
                try {
                  error.requestOptions.headers['Authorization'] =
                      'Bearer ${response.data['data']['jwt']}';

                  // Repeat the request with the updated header
                  handler.resolve(await dio.fetch(error.requestOptions));
                  //handler.resolve(resp);
                } on DioError catch (error) {
                  handler.reject(error);
                }
              } else if(response.data['data'] == null) {
                AuthService().logout();
                BaseService().showError("expired refresh token");
                handler.reject(error);
              }
            } else {
              AuthService().logout();
              handler.reject(error);
            }
          } else {
            handler.reject(error);
          }
        } else {
          print("error $error");
          // BaseService().showError(error.response?.data['message']);
          handler.reject(error);
        }
        handler.next(error);
      },
    ));
  }

  Future<Response> get({
    required String url,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.get(url,
        queryParameters: queryParameters,);
  }

  Future<Response> post({
    required String url,
    required Object requestBody,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.post(url,
        data: requestBody,
        queryParameters: queryParameters,
);
  }

  Future<Response> put({
    required String url,
    required Object requestBody,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.put(url,
        data: requestBody,
        queryParameters: queryParameters,
);
  }

  Future<Response> delete({
    required String url,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.delete(url,
        queryParameters: queryParameters,);
  }

  Future<Response> downLoad({
    required String url,
    required Object requestBody,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.post(url,
        data: requestBody,
        queryParameters: queryParameters,
        options: Options(
            method: 'post',
            responseType: ResponseType.bytes,
            followRedirects: false));
  }
}
