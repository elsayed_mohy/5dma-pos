import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/services/base_dio.dart';

import '../data/model/user.dart';
import '../routes/routes.dart';
import '../utils/constants/app_urls.dart';
import 'base_service.dart';

class AuthService {
  final base = BaseDio();

  void logout() async {
    final User user = userFromJson(GetStorage().read("user"));
    final response = await base.post(
        url: AppUrls.logoutEndPoint,
        requestBody: {'refreshToken': user.refreshToken});
    if (response.statusCode == 200) {
      GetStorage().erase();
      GetStorage().write('loggedIn', false);

      Get.offAllNamed(Routes.loginScreen);
    }
  }

  void getConfigData() async {
    final response = await base.get(url: AppUrls.portalConfigEndPoint);
    if (response.statusCode == 200) {
      GetStorage().write('config', response.data["data"]);
    }
  }
}
