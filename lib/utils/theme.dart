import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'constants/colors.dart';


class ThemesManager {
  final light = ThemeData.light().copyWith(
      textTheme: ThemeData.dark().textTheme.apply(
            fontFamily: 'Poppins',
            bodyColor: ColorsManager.darkGrey,
          ),
      primaryTextTheme: ThemeData.dark().textTheme.apply(
            fontFamily: 'Poppins',
            bodyColor: ColorsManager.darkGrey,
          ),
      primaryColor: ColorsManager.main,
      backgroundColor:  Color(0xffF5F5F5),
    colorScheme: ColorScheme(brightness: Brightness.light, primary: ColorsManager.main, onPrimary: ColorsManager.white, secondary: ColorsManager.secondary, onSecondary: ColorsManager.white, error: ColorsManager.warning, onError: ColorsManager.darkGrey, background: Color(0xffF5F5F5), onBackground: Color(0xffF5F5F5), surface: ColorsManager.white, onSurface: ColorsManager.darkGrey),

      scaffoldBackgroundColor:  Colors.white,
      // textTheme:  lightThemeBaseTextTheme,
      appBarTheme: AppBarTheme(
        backgroundColor: ColorsManager.main,
        foregroundColor: ColorsManager.white,
        elevation: 0,
        // toolbarHeight: 55,
        titleTextStyle: TextStyle(
            fontSize: 18,
            color: ColorsManager.white,
            fontWeight: FontWeight.w500),
      ),
      textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all(ColorsManager.main))),
      iconTheme: const IconThemeData(size: 20.0, color: ColorsManager.main),
      switchTheme: SwitchThemeData(
        thumbColor: MaterialStateProperty.all(ColorsManager.main),
        trackColor:
            MaterialStateProperty.all(ColorsManager.main.withOpacity(0.5)),
      ),
      tabBarTheme: TabBarTheme(
        labelPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 0),
        labelStyle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15
        ),
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.normal,
          fontSize: 15
        ),
        // indicator: CircleTabIndicator(color: ColorsManager.main, radius: 4),
        labelColor: ColorsManager.main,
        unselectedLabelColor: ColorsManager.disabled,
      ),
      brightness: Brightness.light,
    cardTheme: CardTheme(
      color: Colors.white,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
    ),
    expansionTileTheme: ExpansionTileThemeData(
      shape: Border.all(color: Colors.transparent),
    ),

  );

  final dark = ThemeData.dark().copyWith(
    textTheme: ThemeData.dark()
        .textTheme
        .apply(fontFamily: 'Poppins', bodyColor: ColorsManager.white),
    primaryTextTheme: ThemeData.dark()
        .textTheme
        .apply(fontFamily: 'Poppins', bodyColor: ColorsManager.white),
    primaryColor: ColorsManager.main,
    backgroundColor: ColorsManager.darkGrey,
    colorScheme: ColorScheme(brightness: Brightness.dark, primary: ColorsManager.main, onPrimary: ColorsManager.white, secondary: ColorsManager.secondary, onSecondary: ColorsManager.white, error: ColorsManager.warning, onError: ColorsManager.white, background: ColorsManager.darkGrey, onBackground: ColorsManager.disabled, surface: ColorsManager.darkDisabled, onSurface: ColorsManager.disabled),
    scaffoldBackgroundColor: ColorsManager.darkGrey,
    appBarTheme: AppBarTheme(
      backgroundColor: ColorsManager.main,
      foregroundColor: ColorsManager.white,
      elevation: 0,
      titleTextStyle: TextStyle(
          fontSize: 18,
          color: ColorsManager.white,
          fontWeight: FontWeight.w500),
    ),
    textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all(ColorsManager.main))),
    iconTheme: const IconThemeData(size: 20.0, color: ColorsManager.main),
    switchTheme: SwitchThemeData(
      thumbColor: MaterialStateProperty.all(ColorsManager.main),
      trackColor:
          MaterialStateProperty.all(ColorsManager.main.withOpacity(0.5)),
    ),
    brightness: Brightness.dark,
    tabBarTheme: TabBarTheme(
      labelPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 0),
      labelStyle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15
      ),
      unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.normal,
          fontSize: 15
      ),
      // indicator: CircleTabIndicator(color: ColorsManager.main, radius: 4),
      labelColor: ColorsManager.secondary,
      unselectedLabelColor: ColorsManager.disabled,
    ),
    cardTheme: CardTheme(
      color: Colors.black,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
    ),
  );

  final getStorage = GetStorage();
  final darkThemeKey = "isDarkTheme";

  void saveThemeData(bool isDarkMode) {
    getStorage.write(darkThemeKey, isDarkMode);
  }

  bool isSavedDarkMode() {
    return getStorage.read(darkThemeKey) ?? false;
  }

  ThemeMode getThemeMode() {
    return isSavedDarkMode() ? ThemeMode.dark : ThemeMode.light;
  }

  void changeTheme() {
    Get.changeThemeMode(isSavedDarkMode() ? ThemeMode.light : ThemeMode.dark);
    saveThemeData(!isSavedDarkMode());
  }
}
