import 'package:get_storage/get_storage.dart';

import '../../data/model/user.dart';

enum UserType {
  ADMIN,
  CUST,
  CONS,
  ACCM,
  EMP,
  SUPERVISOR,
  BACKOF,
}
