class AppUrls {
   // dev
  static const apiUrl = 'https://www.5dmadev.online/service-5dma/';

  // uat
//  static const apiUrl = 'https://www.5dmauat.website/service-5dma/';

  // prod
  // static const apiUrl = 'https://www.5dma.vip/service-5dma/';

  // baseImageUrl dev
  //static const baseImageUrl = 'https://khdma-bucket.s3.eu-west-2.amazonaws.com/';

  // baseImageUrl uat
  static const baseImageUrl = 'https://khdma-uat-bucket.s3.eu-west-2.amazonaws.com/';

  // baseImageUrl prod
  // static const baseImageUrl = 'https://khdma-production-bucket.s3.eu-west-2.amazonaws.com/';


  // **** AUTH MODULE ****

  static const logInEndPoint = '${apiUrl}api/v1/auth/portal/login';
  static const logoutEndPoint = '${apiUrl}api/v1/auth/logout';
  static const portalConfigEndPoint = '${apiUrl}api/v1/portal-config/get-config';

  // **** CUSTOMER MODULE ****
  static const customerBranchesEndPoint = '${apiUrl}api/v1/customer-branch/customer';
  static const branchUsersEndPoint = '${apiUrl}api/v1/appuser/pos/';


  // **** SESSION MODULE ****
  static const lastSessionByUserEndPoint = '${apiUrl}api/v1/pos-session/last-session-user';
  static const lastClosedSessionEndPoint = '${apiUrl}api/v1/pos-session/last-closed-session-user';
  static const startSessionEndPoint = '${apiUrl}api/v1/pos-session';

  // **** orders MODULE ****
  static const branchItemsEndPoint = '${apiUrl}api/v1/branch-items/pos/';
  static const branchCategoriesEndPoint = '${apiUrl}api/v1/category/findByBranch?branchId=';
  static const unitsEndPoint = '${apiUrl}api/v1/unit';
  static const itemColorsEndPoint = '${apiUrl}api/v1/item-rev-values/item/color';
  static const itemSizesEndPoint = '${apiUrl}api/v1/branch-items/size';
  static const issuingOrderEndPoint = '${apiUrl}api/v1/issuing-order';
  static const issuingOrdersEndPoint = '${apiUrl}api/v1/issuing-order/filter';
  static const issuingOrderDetailsEndPoint = '${apiUrl}api/v1/issuing-order/order-details/';
  static const deliveryMethodsEndPoint = '${apiUrl}api/v1/customer-branch/delivery-methods/';
  static const returnOrderDetailsEndPoint = '${apiUrl}api/v1/issuing-order/returnOrderDetails/';
  static const consumerDetailsEndPoint = '${apiUrl}api/v1/consumer/findByNameOrMobile/';
  static const driversEndPoint = '${apiUrl}api/v1/branch-employee/employees/';
  static const consumersEndPoint = '${apiUrl}api/v1/walking-consumer/all/';
  static const addConsumerEndPoint = '${apiUrl}api/v1/walking-consumer';

  // api/v1/report/pdf
  static const reportEndPoint= '${apiUrl}api/v1/report/pdf';
  static const orderStatusByCodeEndPoint= '${apiUrl}api/v1/order-status/findByCode?statusCode=';


  // **** food orders MODULE ****
  static const foodItemsEndPoint = '${apiUrl}api/v1/food/menu-item/branch/';
  static const branchMenuCategoriesEndPoint = '${apiUrl}api/v1/food/menu-category/branch/';
}
