import 'dart:math';

import 'package:flutter/material.dart';

class ColorsManager {
 static const Color main = Color(0xFF7209b7);
 static const Color secondary = Color(0xFFf72585);
 static const Color success = Color(0xff37B24D);
 static const Color warning = Color(0xffF03E3E);
 static const Color disabled = Color(0xff828282);
 static const Color darkDisabled = Color(0xff333333);
 static const Color basic = Color(0xff0F172A);
 static const Color white = Color(0xffF5F6F8);
 static const Color darkGrey = Color(0xFF121212);
 static const Color svgLight = Color(0xFFF0F1F5);
 static const Color svgDark = Color(0xFF212324);
 static const Color rose = Color(0xFFf72585);
 static const Color fandango = Color(0xFFb5179e);
 static const Color grape = Color(0xFF7209b7);
 static const Color chryslerBlue = Color(0xFF560bad);
 static const Color darkBlue = Color(0xFF480ca8);
 static const Color zaffre = Color(0xFF3a0ca3);
 static const Color palatinateBlue = Color(0xFF3f37c9);
 static const Color neonBlue = Color(0xFF4361ee);
 static const Color vividSkyBlue = Color(0xFF4cc9f0);
 static const Color chefchaouenBlue = Color(0xFF4895ef);

 static double defaultPadding = 18.0;
 static BorderRadius defaultBorderRadius = BorderRadius.circular(20);

 static String generateRandomColor() {
  final random = Random();
  final hexCode = (random.nextInt(0xFFFFFF) + 0x1000000).toRadixString(16);
  return '#' + hexCode.substring(1);
 }
}