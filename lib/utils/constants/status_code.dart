enum StatusCode {
  draft(1),
  confirmed(2),
  approved(3),
  rejected(4),
  active(5),
  inactive(6),
  open(7),
  closed(8);
  const StatusCode(this.value);
  final int value;
}
