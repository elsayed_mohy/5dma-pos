import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/utils/constants/user_type.dart';

import '../../data/model/session_model.dart';
import '../../data/model/user.dart';

class Constants {
  User user = userFromJson(GetStorage().read("user"));

  int ROUND = 100;

  userIsAdmin() {
    return user.userTypes.where((userType) =>  userType.code == UserType.ADMIN.name).isNotEmpty ? true : false;
  }

  userIsCustomer() {
    return user.userTypes.where((userType) =>  userType.code == UserType.CUST.name).isNotEmpty ? true : false;
  }

  userIsConsumer() {
    return user.userTypes.where((userType) =>  userType.code == UserType.CONS.name).isNotEmpty ? true : false;
  }

  userIsAccManager() {
    return user.userTypes.where((userType) =>  userType.code == UserType.ACCM.name).isNotEmpty ? true : false;
  }

  userIsEmployee() {
    return user.userTypes.where((userType) =>  userType.code == UserType.EMP.name).isNotEmpty ? true : false;
  }

  userIsSupervisor() {
    return user.userTypes.where((userType) =>  userType.code == UserType.SUPERVISOR.name).isNotEmpty ? true : false;
  }

  userIsBackOffice() {
    return user.userTypes.where((userType) =>  userType.code == UserType.BACKOF.name).isNotEmpty ? true : false;
  }

  getTaxPercentage() {
    var taxPercentage = Session.fromJson(GetStorage().read('localSession')).taxPercentage / 100;

    return taxPercentage ? taxPercentage : .15;
  }
}