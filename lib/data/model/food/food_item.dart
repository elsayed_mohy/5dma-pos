// To parse this JSON data, do
//
//     final foodItem = foodItemFromJson(jsonString);

import 'dart:convert';

FoodItem foodItemFromJson(String str) => FoodItem.fromJson(json.decode(str));

String foodItemToJson(FoodItem data) => json.encode(data.toJson());

class FoodItem {
  int id;
  int? orderedCount;
  String name;
  dynamic imagePath;
  dynamic description;
  bool hasSize;
  bool hasAddOn;
  int defaultPrice;
  int menuCategoryId;
  List<dynamic> menuItemSizes;

  FoodItem({
    required this.id,
    required this.orderedCount,
    required this.name,
    required this.imagePath,
    required this.description,
    required this.hasSize,
    required this.hasAddOn,
    required this.defaultPrice,
    required this.menuCategoryId,
    required this.menuItemSizes,
  });

  factory FoodItem.fromJson(Map<String, dynamic> json) => FoodItem(
    id: json["id"],
    orderedCount: json["orderedCount"],
    name: json["name"],
    imagePath: json["imagePath"],
    description: json["description"],
    hasSize: json["hasSize"],
    hasAddOn: json["hasAddOn"],
    defaultPrice: json["defaultPrice"],
    menuCategoryId: json["menuCategoryId"],
    menuItemSizes: List<dynamic>.from(json["menuItemSizes"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "orderedCount": orderedCount,
    "name": name,
    "imagePath": imagePath,
    "description": description,
    "hasSize": hasSize,
    "hasAddOn": hasAddOn,
    "defaultPrice": defaultPrice,
    "menuCategoryId": menuCategoryId,
    "menuItemSizes": List<dynamic>.from(menuItemSizes.map((x) => x)),
  };
}
