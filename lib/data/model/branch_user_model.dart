import 'dart:convert';

BranchUser branchUserFromJson(String str) => BranchUser.fromJson(json.decode(str));

String branchUserToJson(BranchUser data) => json.encode(data.toJson());

class BranchUser {
  dynamic id;
  dynamic fullName;
  dynamic phoneNumber;
  dynamic email;

  BranchUser({
     this.id,
     this.fullName,
     this.phoneNumber,
     this.email,
  });

  factory BranchUser.fromJson(Map<String, dynamic> json) => BranchUser(
    id: json["id"],
    fullName: json["fullName"],
    phoneNumber: json["phoneNumber"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullName": fullName,
    "phoneNumber": phoneNumber,
    "email": email,
  };
}
