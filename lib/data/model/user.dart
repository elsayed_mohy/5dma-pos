import 'dart:convert';
import 'branch_config_model.dart';
import 'common/user_type_model.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  String accessToken;
  String refreshToken;
  int userId;
  String email;
  String fullName;
  dynamic imagePath;
  String phoneNumber;
  String preferLanguage;
  List<UserType> userTypes;
  BranchConfig branchConfig;
  int branchId;
  String branchName;
  dynamic customerId;
  dynamic countryId;
  dynamic startTime;
  dynamic endTime;
  dynamic discountPercentage;
  dynamic maxDiscountAmount;
  int remainingDay;
  bool isFreeTrial;

  User({
    required this.accessToken,
    required this.refreshToken,
    required this.userId,
    required this.email,
    required this.fullName,
    required this.imagePath,
    required this.phoneNumber,
    required this.preferLanguage,
    required this.userTypes,
    required this.branchConfig,
    required this.branchId,
    required this.branchName,
     this.customerId,
     this.countryId,
    required this.startTime,
    required this.endTime,
    required this.discountPercentage,
    required this.maxDiscountAmount,
    required this.remainingDay,
    required this.isFreeTrial,
  });

  User copyWith({
    String? accessToken,
    String? refreshToken,
    int? userId,
    String? email,
    String? fullName,
    dynamic imagePath,
    String? phoneNumber,
    String? preferLanguage,
    List<UserType>? userTypes,
    BranchConfig? branchConfig,
    int? branchId,
    String? branchName,
    dynamic customerId,
    dynamic countryId,
    dynamic startTime,
    dynamic endTime,
    dynamic discountPercentage,
    dynamic maxDiscountAmount,
    int? remainingDay,
    bool? isFreeTrial,
  }) =>
      User(
        accessToken: accessToken ?? this.accessToken,
        refreshToken: refreshToken ?? this.refreshToken,
        userId: userId ?? this.userId,
        email: email ?? this.email,
        fullName: fullName ?? this.fullName,
        imagePath: imagePath ?? this.imagePath,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        preferLanguage: preferLanguage ?? this.preferLanguage,
        userTypes: userTypes ?? this.userTypes,
        branchConfig: branchConfig ?? this.branchConfig,
        branchId: branchId ?? this.branchId,
        branchName: branchName ?? this.branchName,
        customerId: customerId ?? this.customerId,
        countryId: countryId ?? this.countryId,
        startTime: startTime ?? this.startTime,
        endTime: endTime ?? this.endTime,
        discountPercentage: discountPercentage ?? this.discountPercentage,
        maxDiscountAmount: maxDiscountAmount ?? this.maxDiscountAmount,
        remainingDay: remainingDay ?? this.remainingDay,
        isFreeTrial: isFreeTrial ?? this.isFreeTrial,
      );

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
    accessToken: json["accessToken"],
    refreshToken: json["refreshToken"],
    userId: json["userId"],
    email: json["email"],
    fullName: json["fullName"],
    imagePath: json["imagePath"],
    phoneNumber: json["phoneNumber"],
    preferLanguage: json["preferLanguage"],
    userTypes: List<UserType>.from(json["userTypes"].map((x) => UserType.fromJson(x))),
    branchConfig: BranchConfig.fromJson(json["branchConfig"]),
    branchId: json["branchId"],
    branchName: json["branchName"],
    customerId:json["customerId"],
    countryId: json["countryId"],
    startTime: json["startTime"],
    endTime: json["endTime"],
    discountPercentage: json["discountPercentage"],
    maxDiscountAmount: json["maxDiscountAmount"],
    remainingDay: json["remainingDay"],
    isFreeTrial: json["isFreeTrial"],
  );

  Map<String, dynamic> toJson() => {
    "accessToken": accessToken,
    "refreshToken": refreshToken,
    "userId": userId,
    "email": email,
    "fullName": fullName,
    "imagePath": imagePath,
    "phoneNumber": phoneNumber,
    "preferLanguage": preferLanguage,
    "userTypes": List<dynamic>.from(userTypes.map((x) => x.toJson())),
    "branchConfig": branchConfig.toJson(),
    "branchId": branchId,
    "branchName": branchName,
    "customerId": customerId,
    "countryId": countryId,
    "startTime": startTime,
    "endTime": endTime,
    "discountPercentage": discountPercentage,
    "maxDiscountAmount": maxDiscountAmount,
    "remainingDay": remainingDay,
    "isFreeTrial": isFreeTrial,
  };
}




