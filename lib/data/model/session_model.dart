// To parse this JSON data, do
//
//     final session = sessionFromJson(jsonString);

import 'dart:convert';

Session sessionFromJson(String str) => Session.fromJson(json.decode(str));

String sessionToJson(Session data) => json.encode(data.toJson());

class Session {
  dynamic id;
  dynamic statusCode;
  dynamic customerBranchId;
  dynamic fromUserId;
  dynamic toUserId;
  dynamic cashAmount;
  dynamic startTime;
  dynamic endTime;
  dynamic justification;
  dynamic totalOrderAmount;
  dynamic taxPercentage;
  dynamic safeRemainBalance;
  dynamic variance;
  Session({
     this.id,
     this.statusCode,
     this.customerBranchId,
     this.fromUserId,
     this.toUserId,
     this.cashAmount,
     this.startTime,
     this.endTime,
     this.justification,
     this.totalOrderAmount,
     this.taxPercentage,
     this.safeRemainBalance,
     this.variance,
  });

  factory Session.fromJson(Map<String, dynamic> json) => Session(
    id: json["id"],
    statusCode: json["statusCode"],
    customerBranchId: json["customerBranchId"],
    fromUserId: json["fromUserId"],
    toUserId: json["toUserId"],
    cashAmount: json["cashAmount"],
    startTime: json["startTime"],
    endTime: json["endTime"],
    justification: json["justification"],
    totalOrderAmount: json["totalOrderAmount"],
    taxPercentage: json["taxPercentage"],
    safeRemainBalance: json["safeRemainBalance"],
    variance: json["variance"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "customerBranchId": customerBranchId,
    "fromUserId": fromUserId,
    "toUserId": toUserId,
    "cashAmount": cashAmount,
    "startTime": startTime,
    "endTime": endTime,
    "justification": justification,
    "totalOrderAmount": totalOrderAmount,
    "taxPercentage": taxPercentage,
    "safeRemainBalance": safeRemainBalance,
    "variance": variance,
  };
}
