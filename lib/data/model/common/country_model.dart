import 'dart:convert';

import 'currency_model.dart';


class Country {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  String createdDate;
  String name;
  Currency currency;

  Country({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.currency,
  });

  Country copyWith({
    int? id,
    dynamic statusCode,
    dynamic createdFromChannelId,
    String? createdDate,
    String? name,
    Currency? currency,
  }) =>
      Country(
        id: id ?? this.id,
        statusCode: statusCode ?? this.statusCode,
        createdFromChannelId: createdFromChannelId ?? this.createdFromChannelId,
        createdDate: createdDate ?? this.createdDate,
        name: name ?? this.name,
        currency: currency ?? this.currency,
      );

  factory Country.fromRawJson(String str) => Country.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Country.fromJson(Map<String, dynamic> json) => Country(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    currency: Currency.fromJson(json["currency"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "currency": currency.toJson(),
  };
}
