import 'dart:convert';

class CheckInOption {
  int id;
  String code;
  String name;

  CheckInOption({
    required this.id,
    required this.code,
    required this.name,
  });

  CheckInOption copyWith({
    int? id,
    String? code,
    String? name,
  }) =>
      CheckInOption(
        id: id ?? this.id,
        code: code ?? this.code,
        name: name ?? this.name,
      );

  factory CheckInOption.fromRawJson(String str) => CheckInOption.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CheckInOption.fromJson(Map<String, dynamic> json) => CheckInOption(
    id: json["id"],
    code: json["code"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
  };
}
