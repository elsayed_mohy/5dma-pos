// To parse this JSON data, do
//
//     final walkingConsumer = walkingConsumerFromJson(jsonString);

import 'dart:convert';

WalkingConsumer walkingConsumerFromJson(String str) => WalkingConsumer.fromJson(json.decode(str));

String walkingConsumerToJson(WalkingConsumer data) => json.encode(data.toJson());

class WalkingConsumer {
  int id;
  String fullName;
  String email;
  String phoneNumber;
  bool isWalking;

  WalkingConsumer({
    required this.id,
    required this.fullName,
    required this.email,
    required this.phoneNumber,
    required this.isWalking,
  });

  factory WalkingConsumer.fromJson(Map<String, dynamic> json) => WalkingConsumer(
    id: json["id"],
    fullName: json["fullName"],
    email: json["email"],
    phoneNumber: json["phoneNumber"],
    isWalking: json["isWalking"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullName": fullName,
    "email": email,
    "phoneNumber": phoneNumber,
    "isWalking": isWalking,
  };
}
