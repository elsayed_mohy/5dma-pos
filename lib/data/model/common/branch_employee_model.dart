// To parse this JSON data, do
//
//     final branchEmployee = branchEmployeeFromJson(jsonString);

import 'dart:convert';

BranchEmployee branchEmployeeFromJson(String str) => BranchEmployee.fromJson(json.decode(str));

String branchEmployeeToJson(BranchEmployee data) => json.encode(data.toJson());

class BranchEmployee {
  int id;
  EmployeeType employeeType;
  dynamic startTime;
  dynamic endTime;
  User user;
  dynamic isResigned;
  dynamic isTerminated;
  EmployeeStatus employeeStatus;
  dynamic branchPosition;

  BranchEmployee({
    required this.id,
    required this.employeeType,
     this.startTime,
     this.endTime,
    required this.user,
     this.isResigned,
     this.isTerminated,
    required this.employeeStatus,
     this.branchPosition,
  });

  factory BranchEmployee.fromJson(Map<String, dynamic> json) => BranchEmployee(
    id: json["id"],
    employeeType: EmployeeType.fromJson(json["employeeType"]),
    startTime: json["startTime"],
    endTime: json["endTime"],
    user: User.fromJson(json["user"]),
    isResigned: json["isResigned"],
    isTerminated: json["isTerminated"],
    employeeStatus: EmployeeStatus.fromJson(json["employeeStatus"]),
    branchPosition: json["branchPosition"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "employeeType": employeeType.toJson(),
    "startTime": startTime,
    "endTime": endTime,
    "user": user.toJson(),
    "isResigned": isResigned,
    "isTerminated": isTerminated,
    "employeeStatus": employeeStatus.toJson(),
    "branchPosition": branchPosition,
  };
}

class EmployeeStatus {
  int id;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic color;
  dynamic isConditional;
  dynamic sortRanking;

  EmployeeStatus({
    required this.id,
     this.code,
     this.name,
     this.nameAr,
     this.nameEn,
     this.nameFr,
     this.color,
     this.isConditional,
     this.sortRanking,
  });

  factory EmployeeStatus.fromJson(Map<String, dynamic> json) => EmployeeStatus(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    color: json["color"],
    isConditional: json["isConditional"],
    sortRanking: json["sortRanking"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "color": color,
    "isConditional": isConditional,
    "sortRanking": sortRanking,
  };
}

class EmployeeType {
  int id;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic description;

  EmployeeType({
    required this.id,

     this.code,
     this.name,
     this.nameAr,
     this.nameEn,
     this.nameFr,
     this.description,
  });

  factory EmployeeType.fromJson(Map<String, dynamic> json) => EmployeeType(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "description": description,
  };
}

class User {
  int id;
  dynamic fullName;
  dynamic email;
  dynamic phoneNumber;
  dynamic imagePath;
  dynamic preferLanguage;
  dynamic isForceChangePass;

  User({
    required this.id,
     this.fullName,
     this.email,
     this.phoneNumber,
     this.imagePath,
     this.preferLanguage,
     this.isForceChangePass,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    fullName: json["fullName"],
    email: json["email"],
    phoneNumber: json["phoneNumber"],
    imagePath: json["imagePath"],
    preferLanguage: json["preferLanguage"],
    isForceChangePass: json["isForceChangePass"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullName": fullName,
    "email": email,
    "phoneNumber": phoneNumber,
    "imagePath": imagePath,
    "preferLanguage": preferLanguage,
    "isForceChangePass": isForceChangePass,
  };
}
