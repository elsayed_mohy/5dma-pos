import 'dart:convert';

class Currency {
  int id;
  String code;
  String name;
  String nameAr;
  String nameEn;
  String nameFr;
  String? symbol;
  String? fractionalUnit;

  Currency({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    this.symbol,
    this.fractionalUnit,
  });

  Currency copyWith({
    int? id,
    String? code,
    String? name,
    String? nameAr,
    String? nameEn,
    String? nameFr,
    String? symbol,
    String? fractionalUnit,
  }) =>
      Currency(
        id: id ?? this.id,
        code: code ?? this.code,
        name: name ?? this.name,
        nameAr: nameAr ?? this.nameAr,
        nameEn: nameEn ?? this.nameEn,
        nameFr: nameFr ?? this.nameFr,
        symbol: symbol ?? this.symbol,
        fractionalUnit: fractionalUnit ?? this.fractionalUnit,
      );

  factory Currency.fromRawJson(String str) => Currency.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Currency.fromJson(Map<String, dynamic> json) => Currency(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    symbol: json["symbol"],
    fractionalUnit: json["fractionalUnit"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "symbol": symbol,
    "fractionalUnit": fractionalUnit,
  };
}
