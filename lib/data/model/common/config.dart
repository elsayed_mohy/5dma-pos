// To parse this JSON data, do
//
//     final branchCategory = branchCategoryFromJson(jsonString);

import 'dart:convert';

import 'package:khdma_pos/data/model/common/check_in_options_model.dart';
import 'package:khdma_pos/data/model/common/currency_model.dart';
import 'package:khdma_pos/data/model/common/salary_element.dart';
import 'package:khdma_pos/data/model/common/user_type_model.dart';

import '../order/branch_item.dart';

Config configFromJson(String str) => Config.fromJson(json.decode(str));

String configToJson(Config data) => json.encode(data.toJson());

class Config {
  List<PortalConfig> portalConfig;
  List<Country> countries;
  List<Service> services;
  List<ServiceCategory> servicesCategories;
  List<GenericType> employeeTypes;
  List<GenericType> unitClasses;
  List<GenericType> addressTypes;
  List<GenericType> countingReasons;
  List<GenericType> dayOfWeeks;
  List<UserType> userTypes;
  List<PaymentMethod> paymentMethods;
  List<GenericType> deliveryMethods;
  List<RevisionGroup> revisionGroups;
  List<GenericType> foodOrderTypes;
  List<GenericType> fisicalYears;
  List<GenericType> quarters;
  List<GenericType> months;
  List<GenericType> paymentTerms;
  List<GenericType> menuItemTypes;
  List<AccountType> accountTypes;
  List<GenericType> payableInvoiceTypes;
  List<GenericStatus> payableInvoiceStatus;
  List<GenericType> bankAccountTypes;
  List<Currency> currencies;
  List<Bank> banks;
  List<GenericType> paymentPeriods;
  List<GenericType> journalSource;
  List<GenericStatus> employeeStatus;
  List<GenericType> itemTypes;
  List<GenericType> contractTypes;
  List<GenericType> employeePaymentMethods;
  List<GenericType> payrollTypes;
  List<GenericStatus> payrollStatus;
  List<EcommerceSlider> ecommerceSlider;
  List<GenericStatus> payableInvoicePayStatus;
  List<GenericType> consumerType;
  List<GenericStatus> receivingOrderStatus;
  List<GenericStatus> orderStatus;
  List<CheckInOption> checkInOptions;
  List<SalaryElement> salaryElements;
  List<GenericType> holidayTypes;
  List<GenericStatus> journalStatus;
  List<GenericType> serviceType;

  Config({
    required this.portalConfig,
    required this.countries,
    required this.services,
    required this.servicesCategories,
    required this.employeeTypes,
    required this.unitClasses,
    required this.addressTypes,
    required this.countingReasons,
    required this.dayOfWeeks,
    required this.userTypes,
    required this.paymentMethods,
    required this.deliveryMethods,
    required this.revisionGroups,
    required this.foodOrderTypes,
    required this.fisicalYears,
    required this.quarters,
    required this.months,
    required this.paymentTerms,
    required this.menuItemTypes,
    required this.accountTypes,
    required this.payableInvoiceTypes,
    required this.payableInvoiceStatus,
    required this.bankAccountTypes,
    required this.currencies,
    required this.banks,
    required this.paymentPeriods,
    required this.journalSource,
    required this.employeeStatus,
    required this.itemTypes,
    required this.contractTypes,
    required this.employeePaymentMethods,
    required this.payrollTypes,
    required this.payrollStatus,
    required this.ecommerceSlider,
    required this.payableInvoicePayStatus,
    required this.consumerType,
    required this.receivingOrderStatus,
    required this.orderStatus,
    required this.checkInOptions,
    required this.salaryElements,
    required this.holidayTypes,
    required this.journalStatus,
    required this.serviceType,
  });

  factory Config.fromJson(Map<String, dynamic> json) => Config(
    portalConfig: List<PortalConfig>.from(json["portalConfig"].map((x) => PortalConfig.fromJson(x))),
    countries: List<Country>.from(json["countries"].map((x) => Country.fromJson(x))),
    services: List<Service>.from(json["services"].map((x) => Service.fromJson(x))),
    servicesCategories: List<ServiceCategory>.from(json["servicesCategories"].map((x) => ServiceCategory.fromJson(x))),
    employeeTypes: List<GenericType>.from(json["employeeTypes"].map((x) => GenericType.fromJson(x))),
    unitClasses: List<GenericType>.from(json["unitClasses"].map((x) => GenericType.fromJson(x))),
    addressTypes: List<GenericType>.from(json["addressTypes"].map((x) => GenericType.fromJson(x))),
    countingReasons: List<GenericType>.from(json["countingReasons"].map((x) => GenericType.fromJson(x))),
    dayOfWeeks: List<GenericType>.from(json["dayOfWeeks"].map((x) => GenericType.fromJson(x))),
    userTypes: List<UserType>.from(json["userTypes"].map((x) => UserType.fromJson(x))),
    paymentMethods: List<PaymentMethod>.from(json["paymentMethods"].map((x) => PaymentMethod.fromJson(x))),
    deliveryMethods: List<GenericType>.from(json["deliveryMethods"].map((x) => GenericType.fromJson(x))),
    revisionGroups: List<RevisionGroup>.from(json["revisionGroups"].map((x) => RevisionGroup.fromJson(x))),
    foodOrderTypes: List<GenericType>.from(json["foodOrderTypes"].map((x) => GenericType.fromJson(x))),
    fisicalYears: List<GenericType>.from(json["fisicalYears"].map((x) => GenericType.fromJson(x))),
    quarters: List<GenericType>.from(json["quarters"].map((x) => GenericType.fromJson(x))),
    months: List<GenericType>.from(json["months"].map((x) => GenericType.fromJson(x))),
    paymentTerms: List<GenericType>.from(json["paymentTerms"].map((x) => GenericType.fromJson(x))),
    menuItemTypes: List<GenericType>.from(json["menuItemTypes"].map((x) => GenericType.fromJson(x))),
    accountTypes: List<AccountType>.from(json["accountTypes"].map((x) => AccountType.fromJson(x))),
    payableInvoiceTypes: List<GenericType>.from(json["payableInvoiceTypes"].map((x) => GenericType.fromJson(x))),
    payableInvoiceStatus: List<GenericStatus>.from(json["payableInvoiceStatus"].map((x) => GenericStatus.fromJson(x))),
    bankAccountTypes: List<GenericType>.from(json["bankAccountTypes"].map((x) => GenericType.fromJson(x))),
    currencies: List<Currency>.from(json["currencies"].map((x) => Currency.fromJson(x))),
    banks: List<Bank>.from(json["banks"].map((x) => Bank.fromJson(x))),
    paymentPeriods: List<GenericType>.from(json["paymentPeriods"].map((x) => GenericType.fromJson(x))),
    journalSource: List<GenericType>.from(json["journalSource"].map((x) => GenericType.fromJson(x))),
    employeeStatus: List<GenericStatus>.from(json["employeeStatus"].map((x) => GenericStatus.fromJson(x))),
    itemTypes: List<GenericType>.from(json["itemTypes"].map((x) => GenericType.fromJson(x))),
    contractTypes: List<GenericType>.from(json["contractTypes"].map((x) => GenericType.fromJson(x))),
    employeePaymentMethods: List<GenericType>.from(json["employeePaymentMethods"].map((x) => GenericType.fromJson(x))),
    payrollTypes: List<GenericType>.from(json["payrollTypes"].map((x) => GenericType.fromJson(x))),
    payrollStatus: List<GenericStatus>.from(json["payrollStatus"].map((x) => GenericStatus.fromJson(x))),
    ecommerceSlider: List<EcommerceSlider>.from(json["ecommerceSlider"].map((x) => EcommerceSlider.fromJson(x))),
    payableInvoicePayStatus: List<GenericStatus>.from(json["payableInvoicePayStatus"].map((x) => GenericStatus.fromJson(x))),
    consumerType: List<GenericType>.from(json["consumerType"].map((x) => GenericType.fromJson(x))),
    receivingOrderStatus: List<GenericStatus>.from(json["receivingOrderStatus"].map((x) => GenericStatus.fromJson(x))),
    orderStatus: List<GenericStatus>.from(json["orderStatus"].map((x) => GenericStatus.fromJson(x))),
    checkInOptions: List<CheckInOption>.from(json["checkInOptions"].map((x) => CheckInOption.fromJson(x))),
    salaryElements: List<SalaryElement>.from(json["salaryElements"].map((x) => SalaryElement.fromJson(x))),
    holidayTypes: List<GenericType>.from(json["holidayTypes"].map((x) => GenericType.fromJson(x))),
    journalStatus: List<GenericStatus>.from(json["journalStatus"].map((x) => GenericStatus.fromJson(x))),
    serviceType: List<GenericType>.from(json["serviceType"].map((x) => GenericType.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "portalConfig": List<dynamic>.from(portalConfig.map((x) => x.toJson())),
    "countries": List<dynamic>.from(countries.map((x) => x.toJson())),
    "services": List<dynamic>.from(services.map((x) => x.toJson())),
    "servicesCategories": List<dynamic>.from(servicesCategories.map((x) => x.toJson())),
    "employeeTypes": List<dynamic>.from(employeeTypes.map((x) => x.toJson())),
    "unitClasses": List<dynamic>.from(unitClasses.map((x) => x.toJson())),
    "addressTypes": List<dynamic>.from(addressTypes.map((x) => x.toJson())),
    "countingReasons": List<dynamic>.from(countingReasons.map((x) => x.toJson())),
    "dayOfWeeks": List<dynamic>.from(dayOfWeeks.map((x) => x.toJson())),
    "userTypes": List<dynamic>.from(userTypes.map((x) => x.toJson())),
    "paymentMethods": List<dynamic>.from(paymentMethods.map((x) => x.toJson())),
    "deliveryMethods": List<dynamic>.from(deliveryMethods.map((x) => x.toJson())),
    "revisionGroups": List<dynamic>.from(revisionGroups.map((x) => x.toJson())),
    "foodOrderTypes": List<dynamic>.from(foodOrderTypes.map((x) => x.toJson())),
    "fisicalYears": List<dynamic>.from(fisicalYears.map((x) => x.toJson())),
    "quarters": List<dynamic>.from(quarters.map((x) => x.toJson())),
    "months": List<dynamic>.from(months.map((x) => x.toJson())),
    "paymentTerms": List<dynamic>.from(paymentTerms.map((x) => x.toJson())),
    "menuItemTypes": List<dynamic>.from(menuItemTypes.map((x) => x.toJson())),
    "accountTypes": List<dynamic>.from(accountTypes.map((x) => x.toJson())),
    "payableInvoiceTypes": List<dynamic>.from(payableInvoiceTypes.map((x) => x.toJson())),
    "payableInvoiceStatus": List<dynamic>.from(payableInvoiceStatus.map((x) => x.toJson())),
    "bankAccountTypes": List<dynamic>.from(bankAccountTypes.map((x) => x.toJson())),
    "currencies": List<dynamic>.from(currencies.map((x) => x.toJson())),
    "banks": List<dynamic>.from(banks.map((x) => x.toJson())),
    "paymentPeriods": List<dynamic>.from(paymentPeriods.map((x) => x.toJson())),
    "journalSource": List<dynamic>.from(journalSource.map((x) => x.toJson())),
    "employeeStatus": List<dynamic>.from(employeeStatus.map((x) => x.toJson())),
    "itemTypes": List<dynamic>.from(itemTypes.map((x) => x.toJson())),
    "contractTypes": List<dynamic>.from(contractTypes.map((x) => x.toJson())),
    "employeePaymentMethods": List<dynamic>.from(employeePaymentMethods.map((x) => x.toJson())),
    "payrollTypes": List<dynamic>.from(payrollTypes.map((x) => x.toJson())),
    "payrollStatus": List<dynamic>.from(payrollStatus.map((x) => x.toJson())),
    "ecommerceSlider": List<dynamic>.from(ecommerceSlider.map((x) => x.toJson())),
    "payableInvoicePayStatus": List<dynamic>.from(payableInvoicePayStatus.map((x) => x.toJson())),
    "consumerType": List<dynamic>.from(consumerType.map((x) => x.toJson())),
    "receivingOrderStatus": List<dynamic>.from(receivingOrderStatus.map((x) => x.toJson())),
    "orderStatus": List<dynamic>.from(orderStatus.map((x) => x.toJson())),
    "checkInOptions": List<dynamic>.from(checkInOptions.map((x) => x.toJson())),
    "salaryElements": List<dynamic>.from(salaryElements.map((x) => x.toJson())),
    "holidayTypes": List<dynamic>.from(holidayTypes.map((x) => x.toJson())),
    "journalStatus": List<dynamic>.from(journalStatus.map((x) => x.toJson())),
    "serviceType": List<dynamic>.from(serviceType.map((x) => x.toJson())),
  };
}

class AccountType {
  int id;
  dynamic code;
  dynamic name;
  bool? credit;
  bool? increase;
  dynamic color;
  dynamic symbol;
  dynamic fractionalUnit;
  int? dayNumber;
  bool? isConditional;
  int? sortRanking;
  dynamic quarter;
  dynamic statusType;
  dynamic itemRevisionValues;

  AccountType({
    required this.id,
    required this.code,
    required this.name,

    this.credit,
    this.increase,
    this.color,
    this.symbol,
    this.fractionalUnit,
    this.dayNumber,
    this.isConditional,
    this.sortRanking,
    this.quarter,
    this.statusType,
    this.itemRevisionValues,
  });

  factory AccountType.fromJson(Map<String, dynamic> json) => AccountType(
    id: json["id"],
    code: json["code"],
    name: json["name"],

    credit: json["credit"],
    increase: json["increase"],
    color: json["color"],
    symbol: json["symbol"],
    fractionalUnit: json["fractionalUnit"],
    dayNumber: json["dayNumber"],
    isConditional: json["isConditional"],
    sortRanking: json["sortRanking"],
    quarter: json["quarter"],
    statusType: json["statusType"],
    itemRevisionValues: json["itemRevisionValues"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,

    "credit": credit,
    "increase": increase,
    "color": color,
    "symbol": symbol,
    "fractionalUnit": fractionalUnit,
    "dayNumber": dayNumber,
    "isConditional": isConditional,
    "sortRanking": sortRanking,
    "quarter": quarter,
    "statusType": statusType,
    "itemRevisionValues": itemRevisionValues,
  };
}

class GenericType {
  int id;
  dynamic code;
  dynamic name;
  dynamic color;
  dynamic description;
  GenericType({
    required this.id,
    required this.code,
    required this.name,
     this.color,
     this.description,
  });

  factory GenericType.fromJson(Map<String, dynamic> json) => GenericType(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    color: json["color"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "color": color,
    "description": description,
  };
}
class RevisionGroup {
  int id;
  dynamic code;
  dynamic name;
  dynamic itemRevisionValues;
  RevisionGroup({
    required this.id,
    required this.code,
    required this.name,
     this.itemRevisionValues,
  });

  factory RevisionGroup.fromJson(Map<String, dynamic> json) => RevisionGroup(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    itemRevisionValues: json["itemRevisionValues"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "itemRevisionValues": itemRevisionValues,
  };
}

class GenericStatus {
  int id;
  dynamic code;
  dynamic name;
  dynamic color;
  bool? isConditional;
  int? sortRanking;
  GenericStatus({
    required this.id,
    required this.code,
    required this.name,
     this.color,
     this.isConditional,
     this.sortRanking,
  });

  factory GenericStatus.fromJson(Map<String, dynamic> json) => GenericStatus(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    color: json["color"],
    sortRanking: json["sortRanking"],
    isConditional: json["isConditional"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "color": color,
    "sortRanking": sortRanking,
    "isConditional": isConditional,
  };
}

class Bank {
  int id;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic description;
  GenericType? salaryElementType;
  GenericType? salaryElementPeriod;
  dynamic imagePath;
  Service? service;

  Bank({
    required this.id,
    required this.createdFromChannelId,
    required this.createdDate,
    this.code,
    required this.name,
    this.nameAr,
    this.nameEn,
    this.nameFr,
    this.description,
    this.salaryElementType,
    this.salaryElementPeriod,
    this.imagePath,
    this.service,
  });

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
    id: json["id"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    description: json["description"],
    salaryElementType: json["salaryElementType"] == null ? null : GenericType.fromJson(json["salaryElementType"]),
    salaryElementPeriod: json["salaryElementPeriod"] == null ? null : GenericType.fromJson(json["salaryElementPeriod"]),
    imagePath: json["imagePath"],
    service: json["service"] == null ? null : Service.fromJson(json["service"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "description": description,
    "salaryElementType": salaryElementType?.toJson(),
    "salaryElementPeriod": salaryElementPeriod?.toJson(),
    "imagePath": imagePath,
    "service": service?.toJson(),
  };
}

class Country {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic name;
  dynamic countryCode;
  dynamic countryCallingCode;
  dynamic vat;
  Currency? currency;
  dynamic phoneRegex;
  dynamic imagePath;
  dynamic timezone;
  List<Governorate>? governorates;

  Country({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.countryCode,
    required this.countryCallingCode,
    required this.vat,
    this.currency,
    required this.phoneRegex,
    required this.imagePath,
    this.timezone,
    this.governorates,
  });

  factory Country.fromJson(Map<String, dynamic> json) => Country(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    countryCode: json["countryCode"],
    countryCallingCode: json["countryCallingCode"],
    vat: json["vat"]?.toDouble(),
    currency: json["currency"] == null ? null : Currency.fromJson(json["currency"]),
    phoneRegex: json["phoneRegex"],
    imagePath: json["imagePath"],
    timezone: json["timezone"],
    governorates: json["governorates"] == null ? [] : List<Governorate>.from(json["governorates"]!.map((x) => Governorate.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "countryCode": countryCode,
    "countryCallingCode": countryCallingCode,
    "vat": vat,
    "currency": currency?.toJson(),
    "phoneRegex": phoneRegex,
    "imagePath": imagePath,
    "timezone": timezone,
    "governorates": governorates == null ? [] : List<dynamic>.from(governorates!.map((x) => x.toJson())),
  };
}

class Governorate {
  int id;
  dynamic name;
  dynamic governorateCode;
  dynamic country;
  List<City> cities;

  Governorate({
    required this.id,
    required this.name,
    required this.governorateCode,
    required this.country,
    required this.cities,
  });

  factory Governorate.fromJson(Map<String, dynamic> json) => Governorate(
    id: json["id"],
    name: json["name"],
    governorateCode: json["governorateCode"],
    country: json["country"],
    cities: List<City>.from(json["cities"].map((x) => City.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "governorateCode": governorateCode,
    "country": country,
    "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
  };
}

class City {
  int id;
  dynamic name;
  dynamic cityCode;
  dynamic governorate;

  City({
    required this.id,
    required this.name,
    required this.cityCode,
    required this.governorate,
  });

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"],
    name: json["name"],
    cityCode: json["cityCode"],
    governorate: json["governorate"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "cityCode": cityCode,
    "governorate": governorate,
  };
}



class EcommerceSlider {
  int id;
  dynamic code;
  dynamic name;
  dynamic maxNumber;
  dynamic rotatable;
  dynamic intervals;

  EcommerceSlider({
    required this.id,
    required this.code,
    required this.name,
    required this.maxNumber,
    required this.rotatable,
    required this.intervals,
  });

  factory EcommerceSlider.fromJson(Map<String, dynamic> json) => EcommerceSlider(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    maxNumber: json["maxNumber"],
    rotatable: json["rotatable"],
    intervals: json["intervals"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "maxNumber": maxNumber,
    "rotatable": rotatable,
    "intervals": intervals,
  };
}

class PaymentMethod {
  int id;
  dynamic code;
  dynamic name;
  dynamic zatcaCode;
  dynamic description;
  dynamic zakatName;

  PaymentMethod({
    required this.id,
    required this.code,
    required this.name,
    required this.zatcaCode,
    required this.description,
    required this.zakatName,
  });

  factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    zatcaCode: json["zatcaCode"],
    description: json["description"],
    zakatName: json["zakatName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "zatcaCode": zatcaCode,
    "description": description,
    "zakatName": zakatName,
  };
}

class PortalConfig {
  int id;
  dynamic configKey;
  dynamic configValue;

  PortalConfig({
    required this.id,
    required this.configKey,
    required this.configValue,
  });

  factory PortalConfig.fromJson(Map<String, dynamic> json) => PortalConfig(
    id: json["id"],
    configKey: json["configKey"],
    configValue: json["configValue"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "configKey": configKey,
    "configValue": configValue,
  };
}

class Service {
  int id;
  dynamic code;
  dynamic name;
  dynamic description;
  dynamic imagePath;
  GenericType serviceType;
  List<ServiceCountry> countries;

  Service({
    required this.id,
    required this.code,
    required this.name,
    required this.description,
    required this.imagePath,
    required this.serviceType,
    required this.countries,
  });

  factory Service.fromJson(Map<String, dynamic> json) => Service(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    description: json["description"],
    imagePath: json["imagePath"],
    serviceType: GenericType.fromJson(json["serviceType"]),
    countries: List<ServiceCountry>.from(json["countries"].map((x) => ServiceCountry.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "description": description,
    "imagePath": imagePath,
    "serviceType": serviceType.toJson(),
    "countries": List<dynamic>.from(countries.map((x) => x.toJson())),
  };
}
class ServiceCountry {
  int id;
  dynamic name;
  dynamic countryCode;
  dynamic countryCallingCode;
  dynamic vat;
  dynamic phoneRegex;
  dynamic imagePath;

  ServiceCountry({
    required this.id,
    required this.name,
    required this.countryCode,
    required this.countryCallingCode,
    required this.vat,
    required this.phoneRegex,
    required this.imagePath,

  });

  factory ServiceCountry.fromJson(Map<String, dynamic> json) => ServiceCountry(
    id: json["id"],
    name: json["name"],
    countryCode: json["countryCode"],
    countryCallingCode: json["countryCallingCode"],
    vat: json["vat"],
    phoneRegex: json["phoneRegex"],
    imagePath: json["imagePath"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "countryCode": countryCode,
    "countryCallingCode": countryCallingCode,
    "vat": vat,
    "phoneRegex": phoneRegex,
    "imagePath": imagePath,
  };
}
class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
