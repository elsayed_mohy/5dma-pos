// To parse this JSON data, do
//
//     final serviceCategory = serviceCategoryFromJson(jsonString);

import 'dart:convert';

ServiceCategory serviceCategoryFromJson(String str) => ServiceCategory.fromJson(json.decode(str));

String serviceCategoryToJson(ServiceCategory data) => json.encode(data.toJson());

class ServiceCategory {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  String code;
  String name;
  String? nameEn;
  String description;
  String imagePath;
  ServiceCategory? service;

  ServiceCategory({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.code,
    required this.name,
    this.nameEn,
    required this.description,
    required this.imagePath,
    this.service,
  });

  factory ServiceCategory.fromJson(Map<String, dynamic> json) => ServiceCategory(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    code: json["code"],
    name: json["name"],
    nameEn: json["nameEn"],
    description: json["description"],
    imagePath: json["imagePath"],
    service: json["service"] == null ? null : ServiceCategory.fromJson(json["service"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "code": code,
    "name": name,
    "nameEn": nameEn,
    "description": description,
    "imagePath": imagePath,
    "service": service?.toJson(),
  };
}
