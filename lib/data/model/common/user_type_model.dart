import 'dart:convert';

class UserType {
  int id;
  String code;
  String name;
  String nameAr;
  String nameEn;
  String nameFr;


  UserType({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,

  });

  UserType copyWith({
    int? id,
    String? code,
    String? name,
    String? nameAr,
    String? nameEn,
    String? nameFr,
  }) =>
      UserType(
        id: id ?? this.id,
        code: code ?? this.code,
        name: name ?? this.name,
        nameAr: nameAr ?? this.nameAr,
        nameEn: nameEn ?? this.nameEn,
        nameFr: nameFr ?? this.nameFr,
      );

  factory UserType.fromRawJson(String str) => UserType.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserType.fromJson(Map<String, dynamic> json) => UserType(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
  };
}
