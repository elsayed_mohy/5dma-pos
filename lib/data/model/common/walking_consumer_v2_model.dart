// To parse this JSON data, do
//
//     final walkingConsumerV2 = walkingConsumerV2FromJson(jsonString);

import 'dart:convert';

import 'package:khdma_pos/data/model/common/config.dart';

WalkingConsumerV2 walkingConsumerV2FromJson(String str) => WalkingConsumerV2.fromJson(json.decode(str));

String walkingConsumerV2ToJson(WalkingConsumerV2 data) => json.encode(data.toJson());

class WalkingConsumerV2 {
  int id;
  String fullName;
  String email;
  GenericType consumerType;
  dynamic currency;
  dynamic paymentTerm;
  dynamic legalEntityNumber;
  dynamic contactPersonNumber;
  dynamic contactPersonName;
  dynamic taxRate;
  dynamic creditLimit;
  String phoneNumber;
  int taxNumber;
  GenericType addressType;
  String street;
  String street2;
  int postalCode;
  String building;
  int floor;
  int flatNumber;
  String landMark;
  dynamic additionalNo;
  String country;
  String governorate;
  String city;
  dynamic other;

  WalkingConsumerV2({
    required this.id,
    required this.fullName,
    required this.email,
    required this.consumerType,
    required this.currency,
    required this.paymentTerm,
    required this.legalEntityNumber,
    required this.contactPersonNumber,
    required this.contactPersonName,
    required this.taxRate,
    required this.creditLimit,
    required this.phoneNumber,
    required this.taxNumber,
    required this.addressType,
    required this.street,
    required this.street2,
    required this.postalCode,
    required this.building,
    required this.floor,
    required this.flatNumber,
    required this.landMark,
    required this.additionalNo,
    required this.country,
    required this.governorate,
    required this.city,
    required this.other,
  });

  factory WalkingConsumerV2.fromJson(Map<String, dynamic> json) => WalkingConsumerV2(
    id: json["id"],
    fullName: json["fullName"],
    email: json["email"],
    consumerType: GenericType.fromJson(json["consumerType"]),
    currency: json["currency"],
    paymentTerm: json["paymentTerm"],
    legalEntityNumber: json["legalEntityNumber"],
    contactPersonNumber: json["contactPersonNumber"],
    contactPersonName: json["contactPersonName"],
    taxRate: json["taxRate"],
    creditLimit: json["creditLimit"],
    phoneNumber: json["phoneNumber"],
    taxNumber: json["taxNumber"],
    addressType: GenericType.fromJson(json["addressType"]),
    street: json["street"],
    street2: json["street2"],
    postalCode: json["postalCode"],
    building: json["building"],
    floor: json["floor"],
    flatNumber: json["flatNumber"],
    landMark: json["landMark"],
    additionalNo: json["additionalNo"],
    country: json["country"],
    governorate: json["governorate"],
    city: json["city"],
    other: json["other"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullName": fullName,
    "email": email,
    "consumerType": consumerType.toJson(),
    "currency": currency,
    "paymentTerm": paymentTerm,
    "legalEntityNumber": legalEntityNumber,
    "contactPersonNumber": contactPersonNumber,
    "contactPersonName": contactPersonName,
    "taxRate": taxRate,
    "creditLimit": creditLimit,
    "phoneNumber": phoneNumber,
    "taxNumber": taxNumber,
    "addressType": addressType.toJson(),
    "street": street,
    "street2": street2,
    "postalCode": postalCode,
    "building": building,
    "floor": floor,
    "flatNumber": flatNumber,
    "landMark": landMark,
    "additionalNo": additionalNo,
    "country": country,
    "governorate": governorate,
    "city": city,
    "other": other,
  };
}

