// To parse this JSON data, do
//
//     final deliveryMethod = deliveryMethodFromJson(jsonString);

import 'dart:convert';

DeliveryMethod deliveryMethodFromJson(String str) => DeliveryMethod.fromJson(json.decode(str));

String deliveryMethodToJson(DeliveryMethod data) => json.encode(data.toJson());

class DeliveryMethod {
  int id;
  String code;
  String name;

  DeliveryMethod({
    required this.id,
    required this.code,
    required this.name,
  });

  factory DeliveryMethod.fromJson(Map<String, dynamic> json) => DeliveryMethod(
    id: json["id"],
    code: json["code"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
  };
}
