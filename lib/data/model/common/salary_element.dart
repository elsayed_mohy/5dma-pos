import 'package:khdma_pos/data/model/common/config.dart';

class SalaryElement {
  int id;

  String createdDate;
  String name;
  String nameAr;
  String nameEn;
  String nameFr;
  GenericType salaryElementType;
  GenericType salaryElementPeriod;

  SalaryElement({
    required this.id,

    required this.createdDate,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.salaryElementType,
    required this.salaryElementPeriod,
  });

  factory SalaryElement.fromJson(Map<String, dynamic> json) => SalaryElement(
    id: json["id"],

    createdDate: json["createdDate"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    salaryElementType: GenericType.fromJson(json["salaryElementType"]),
    salaryElementPeriod: GenericType.fromJson(json["salaryElementPeriod"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,

    "createdDate": createdDate,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "salaryElementType": salaryElementType.toJson(),
    "salaryElementPeriod": salaryElementPeriod.toJson(),
  };
}