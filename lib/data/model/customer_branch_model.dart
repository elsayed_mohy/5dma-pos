// To parse this JSON data, do
//
//     final customerBranch = customerBranchFromJson(jsonString);

import 'dart:convert';

import 'common/currency_model.dart';


CustomerBranch customerBranchFromJson(String str) =>
    CustomerBranch.fromJson(json.decode(str));

String customerBranchToJson(CustomerBranch data) => json.encode(data.toJson());

class CustomerBranch {
  int id;
  String branchName;
  dynamic fixedRate;
  dynamic percentageRate;
  dynamic branchDeliveryCost;
  dynamic branchDeliveryTime;
  dynamic imagePath;
  dynamic sortRank;
  dynamic mobileNumber;
  dynamic whatsAppNumber;
  dynamic slotDurationInMinutes;
  dynamic isMain;
  dynamic capacity;
  dynamic maxDistance;
  dynamic maxEmpDistance;
  dynamic maxPosBeforeStartTime;
  dynamic lat;
  dynamic lon;
  Currency currency;
  dynamic street;
  dynamic bank;
  List<Service> services;

  CustomerBranch({
    required this.id,
    required this.branchName,
    this.fixedRate,
    this.percentageRate,
    this.branchDeliveryCost,
    this.branchDeliveryTime,
    this.imagePath,
    this.sortRank,
    this.mobileNumber,
    this.whatsAppNumber,
    this.slotDurationInMinutes,
    this.isMain,
    this.capacity,
    this.maxDistance,
    this.maxEmpDistance,
    this.maxPosBeforeStartTime,
    this.lat,
    this.lon,
    required this.currency,
    this.street,
    this.bank,
    required this.services,
  });

  factory CustomerBranch.fromJson(Map<String, dynamic> json) => CustomerBranch(
        id: json["id"],
        branchName: json["branchName"],
        fixedRate: json["fixedRate"],
        percentageRate: json["percentageRate"],
        branchDeliveryCost: json["branchDeliveryCost"],
        branchDeliveryTime: json["branchDeliveryTime"],
        imagePath: json["imagePath"],
        sortRank: json["sortRank"],
        mobileNumber: json["mobileNumber"],
        whatsAppNumber: json["whatsAppNumber"],
        slotDurationInMinutes: json["slotDurationInMinutes"],
        isMain: json["isMain"],
        capacity: json["capacity"],
        maxDistance: json["maxDistance"],
        maxEmpDistance: json["maxEmpDistance"],
        maxPosBeforeStartTime: json["maxPosBeforeStartTime"],
        lat: json["lat"]?.toDouble(),
        lon: json["lon"]?.toDouble(),
        currency: Currency.fromJson(json["currency"]),
        street: json["street"],
        bank: json["bank"],
        services: List<Service>.from(
            json["services"].map((x) => Service.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "branchName": branchName,
        "fixedRate": fixedRate,
        "percentageRate": percentageRate,
        "branchDeliveryCost": branchDeliveryCost,
        "branchDeliveryTime": branchDeliveryTime,
        "imagePath": imagePath,
        "sortRank": sortRank,
        "mobileNumber": mobileNumber,
        "whatsAppNumber": whatsAppNumber,
        "slotDurationInMinutes": slotDurationInMinutes,
        "isMain": isMain,
        "capacity": capacity,
        "maxDistance": maxDistance,
        "maxEmpDistance": maxEmpDistance,
        "maxPosBeforeStartTime": maxPosBeforeStartTime,
        "lat": lat,
        "lon": lon,
        "currency": currency.toJson(),
        "street": street,
        "bank": bank,
        "services": List<dynamic>.from(services.map((x) => x.toJson())),
      };
}

class Service {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  String name;

  Service({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
  });

  factory Service.fromJson(Map<String, dynamic> json) => Service(
        id: json["id"],
        statusCode: json["statusCode"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "name": name,
      };
}
