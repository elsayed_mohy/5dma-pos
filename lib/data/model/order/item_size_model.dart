// To parse this JSON data, do
//
//     final itemSize = itemSizeFromJson(jsonString);

import 'dart:convert';

import 'package:khdma_pos/data/model/order/branch_item.dart';

ItemSize itemSizeFromJson(String str) => ItemSize.fromJson(json.decode(str));

String itemSizeToJson(ItemSize data) => json.encode(data.toJson());

class ItemSize {
  int id;
  double salesPrice;
  double discount;
  bool taxable;
  double salesTaxValue;
  int quantity;
  int gomlaMinQty;
  double salesTaxPercentage;
  Size size;

  ItemSize({
   required this.id,
   required this.salesPrice,
   required this.discount,
   required this.taxable,
   required this.salesTaxValue,
   required this.quantity,
   required this.gomlaMinQty,
   required this.salesTaxPercentage,
   required this.size,
  });

  factory ItemSize.fromJson(Map<String, dynamic> json) => ItemSize(
    id: json["id"],
    salesPrice: json["salesPrice"].toDouble(),
    discount: json["discount"],
    taxable: json["taxable"],
    salesTaxValue: json["salesTaxValue"].toDouble(),
    quantity: json["quantity"],
    gomlaMinQty: json["gomlaMinQty"],
    salesTaxPercentage: json["salesTaxPercentage"],
    size: Size.fromJson(json["size"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "salesPrice": salesPrice,
    "discount": discount,
    "taxable": taxable,
    "salesTaxValue": salesTaxValue,
    "quantity": quantity,
    "gomlaMinQty": gomlaMinQty,
    "salesTaxPercentage": salesTaxPercentage,
    "size": size.toJson(),
  };
}

