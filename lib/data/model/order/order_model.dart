// To parse this JSON data, do
//
//     final order = orderFromJson(jsonString);

import 'dart:convert';

OrderModel orderFromJson(String str) => OrderModel.fromJson(json.decode(str));

String orderToJson(OrderModel data) => json.encode(data.toJson());

class OrderModel {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  String issuingNumber;
  String issuingDate;
  String branchName;
  dynamic consumerName;
  dynamic walkConsumerName;
  double totalAmount;
  double totalTax;
  double totalDiscount;
  OrderStatus orderStatus;
  PaymentMethod paymentMethod;
  String returnFlag;
  dynamic totalNetReturnedAmount;
  dynamic returnItemsCount;
  dynamic zatcaStatus;

  OrderModel({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.issuingNumber,
    required this.issuingDate,
    required this.branchName,
    required this.consumerName,
    required this.walkConsumerName,
    required this.totalAmount,
    required this.totalTax,
    required this.totalDiscount,
    required this.orderStatus,
    required this.paymentMethod,
    required this.returnFlag,
    required this.totalNetReturnedAmount,
    required this.returnItemsCount,
    required this.zatcaStatus,
  });

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    issuingNumber: json["issuingNumber"],
    issuingDate: json["issuingDate"],
    branchName: json["branchName"],
    consumerName: json["consumerName"],
    walkConsumerName: json["walkConsumerName"],
    totalAmount: json["totalAmount"]?.toDouble(),
    totalTax: json["totalTax"]?.toDouble(),
    totalDiscount: json["totalDiscount"],
    orderStatus: OrderStatus.fromJson(json["orderStatus"]),
    paymentMethod: PaymentMethod.fromJson(json["paymentMethod"]),
    returnFlag: json["returnFlag"],
    totalNetReturnedAmount: json["totalNetReturnedAmount"],
    returnItemsCount: json["returnItemsCount"],
    zatcaStatus: json["zatcaStatus"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "issuingNumber": issuingNumber,
    "issuingDate": issuingDate,
    "branchName": branchName,
    "consumerName": consumerName,
    "walkConsumerName": walkConsumerName,
    "totalAmount": totalAmount,
    "totalTax": totalTax,
    "totalDiscount": totalDiscount,
    "orderStatus": orderStatus.toJson(),
    "paymentMethod": paymentMethod.toJson(),
    "returnFlag": returnFlag,
    "totalNetReturnedAmount": totalNetReturnedAmount,
    "returnItemsCount": returnItemsCount,
    "zatcaStatus": zatcaStatus,
  };
}

class OrderStatus {
  dynamic id;
  dynamic code;
  String name;
  String color;

  OrderStatus({
    required this.id,
    required this.code,
    required this.name,
    required this.color,
  });

  factory OrderStatus.fromJson(Map<String, dynamic> json) => OrderStatus(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    color: json["color"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "color": color,
  };
}

class PaymentMethod {
  int id;
  String code;
  String name;
  String nameAr;
  String nameEn;
  String nameFr;
  dynamic zatcaCode;
  dynamic description;
  dynamic zakatName;

  PaymentMethod({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.zatcaCode,
    required this.description,
    required this.zakatName,
  });

  factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    zatcaCode: json["zatcaCode"],
    description: json["description"],
    zakatName: json["zakatName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "zatcaCode": zatcaCode,
    "description": description,
    "zakatName": zakatName,
  };
}
