// To parse this JSON data, do
//
//     final returnOrderDetails = returnOrderDetailsFromJson(jsonString);

import 'dart:convert';

ReturnOrderDetails returnOrderDetailsFromJson(String str) => ReturnOrderDetails.fromJson(json.decode(str));

String returnOrderDetailsToJson(ReturnOrderDetails data) => json.encode(data.toJson());

class ReturnOrderDetails {
  int qty;
  int branchItemId;
  String itemName;
  String unitName;

  ReturnOrderDetails({
    required this.qty,
    required this.branchItemId,
    required this.itemName,
    required this.unitName,
  });

  factory ReturnOrderDetails.fromJson(Map<String, dynamic> json) => ReturnOrderDetails(
    qty: json["qty"],
    branchItemId: json["branchItemId"],
    itemName: json["itemName"],
    unitName: json["unitName"],
  );

  Map<String, dynamic> toJson() => {
    "qty": qty,
    "branchItemId": branchItemId,
    "itemName": itemName,
    "unitName": unitName,
  };
}
