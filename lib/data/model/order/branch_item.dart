// To parse this JSON data, do
//
//     final branchItem = branchItemFromJson(jsonString);

import 'dart:convert';

BranchItem branchItemFromJson(String str) =>
    BranchItem.fromJson(json.decode(str));

String branchItemToJson(BranchItem data) => json.encode(data.toJson());

class BranchItem {
  int id;
  int? orderedCount;
  int? revisionId;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic itemId;
  dynamic branchId;
  dynamic salesPrice;
  dynamic discount;
  Item? item;
  dynamic taxable;
  dynamic salesTaxValue;
  dynamic quantity;
  dynamic gomlaMinQty;
  dynamic salesTaxPercentage;
  dynamic reserviedQty;
  dynamic availableQty;
  dynamic barCode;
  Size? size;
  Color? color;
  OfferDetails? offerDetails;
  dynamic giftItem;

  BranchItem(
      {required this.id,
      this.orderedCount,
      this.revisionId,
      required this.statusCode,
      required this.createdFromChannelId,
      required this.createdDate,
      required this.itemId,
      required this.branchId,
      required this.salesPrice,
      required this.discount,
      this.item,
      required this.taxable,
      required this.salesTaxValue,
      required this.quantity,
      required this.gomlaMinQty,
      required this.salesTaxPercentage,
      required this.reserviedQty,
      required this.availableQty,
      required this.barCode,
      required this.size,
      required this.color,
      required this.offerDetails,
      this.giftItem});

  factory BranchItem.fromJson(Map<String, dynamic> json) => BranchItem(
    id: json["id"],
    revisionId: json["revisionId"],
    orderedCount: json["orderedCount"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    itemId: json["itemId"],
    branchId: json["branchId"],
    salesPrice: json["salesPrice"]?.toDouble(),
    discount: json["discount"],
    item: json["item"] == null ? null : Item.fromJson(json["item"]),
    taxable: json["taxable"],
    salesTaxValue: json["salesTaxValue"]?.toDouble(),
    quantity: json["quantity"],
    gomlaMinQty: json["gomlaMinQty"],
    salesTaxPercentage: json["salesTaxPercentage"],
    reserviedQty: json["reserviedQty"],
    availableQty: json["availableQty"],
    barCode: json["barCode"],
    size: json["size"] == null ? null : Size.fromJson(json["size"]!),
    color: json["color"] == null ? null : Color.fromJson(json["color"]),
    offerDetails: json["offerDetails"] == null ? null : OfferDetails.fromJson(json["offerDetails"]),
  );

  Map<String, dynamic> toJson() => {
        "id": id,
        "revisionId": revisionId,
        "orderedCount": orderedCount,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "itemId": itemId,
        "branchId": branchId,
        "salesPrice": salesPrice,
        "discount": discount,
        "item": item?.toJson(),
        "taxable": taxable,
        "salesTaxValue": salesTaxValue,
        "quantity": quantity,
        "gomlaMinQty": gomlaMinQty,
        "salesTaxPercentage": salesTaxPercentage,
        "reserviedQty": reserviedQty,
        "availableQty": availableQty,
        "barCode": barCode,
        "size": size?.toJson(),
        "color": color?.toJson(),
        "offerDetails": offerDetails?.toJson(),
        "giftItem": giftItem
      };
}

class Item {
  int id;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic itemNumber;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic description;
  dynamic secondaryDescription;
  dynamic expiryDays;
  dynamic unitClassId;
  dynamic baseUnitId;
  dynamic categoryId;
  dynamic subCategoryId;
  dynamic countryId;
  dynamic serviceCategoryId;
  dynamic imagePath;
  dynamic customerCount;
  dynamic taxable;
  BaseUnit? baseUnit;
  Brand? brand;
  dynamic hasRevision;
  dynamic branchItemUsed;

  Item({
    required this.id,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.itemNumber,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.description,
    required this.secondaryDescription,
    required this.expiryDays,
    required this.unitClassId,
    required this.baseUnitId,
    required this.categoryId,
    required this.subCategoryId,
    required this.countryId,
    required this.serviceCategoryId,
    required this.imagePath,
    required this.customerCount,
    required this.taxable,
    this.baseUnit,
    this.brand,
    required this.hasRevision,
    required this.branchItemUsed,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        itemNumber: json["itemNumber"],
        name: json["name"],
        nameAr: json["nameAr"],
        nameEn: json["nameEn"],
        nameFr: json["nameFr"],
        description: json["description"],
        secondaryDescription: json["secondaryDescription"],
        expiryDays: json["expiryDays"],
        unitClassId: json["unitClassId"],
        baseUnitId: json["baseUnitId"],
        categoryId: json["categoryId"],
        subCategoryId: json["subCategoryId"],
        countryId: json["countryId"],
        serviceCategoryId: json["serviceCategoryId"],
        imagePath: json["imagePath"],
        customerCount: json["customerCount"],
        taxable: json["taxable"],
        baseUnit: json["baseUnit"] == null
            ? null
            : BaseUnit.fromJson(json["baseUnit"]),
        brand: json["brand"] == null ? null : Brand.fromJson(json["brand"]),
        hasRevision: json["hasRevision"],
        branchItemUsed: json["branchItemUsed"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "itemNumber": itemNumber,
        "name": name,
        "nameAr": nameAr,
        "nameEn": nameEn,
        "nameFr": nameFr,
        "description": description,
        "secondaryDescription": secondaryDescription,
        "expiryDays": expiryDays,
        "unitClassId": unitClassId,
        "baseUnitId": baseUnitId,
        "categoryId": categoryId,
        "subCategoryId": subCategoryId,
        "countryId": countryId,
        "serviceCategoryId": serviceCategoryId,
        "imagePath": imagePath,
        "customerCount": customerCount,
        "taxable": taxable,
        "baseUnit": baseUnit?.toJson(),
        "brand": brand?.toJson(),
        "hasRevision": hasRevision,
        "branchItemUsed": branchItemUsed,
      };
}

class BaseUnit {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic description;
  dynamic classId;

  BaseUnit({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.description,
    required this.classId,
  });

  factory BaseUnit.fromJson(Map<String, dynamic> json) => BaseUnit(
        id: json["id"],
        statusCode: json["statusCode"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        name: json["name"],
        nameAr: json["nameAr"],
        nameEn: json["nameEn"],
        nameFr: json["nameFr"],
        description: json["description"],
        classId: json["classId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "name": name,
        "nameAr": nameAr,
        "nameEn": nameEn,
        "nameFr": nameFr,
        "description": description,
        "classId": classId,
      };
}

class Brand {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic imagePath;
  List<ServiceCategory> serviceCategories;
  dynamic customer;

  Brand({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.imagePath,
    required this.serviceCategories,
    required this.customer,
  });

  factory Brand.fromJson(Map<String, dynamic> json) => Brand(
        id: json["id"],
        statusCode: json["statusCode"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        code: json["code"],
        name: json["name"],
        nameAr: json["nameAr"],
        nameEn: json["nameEn"],
        nameFr: json["nameFr"],
        imagePath: json["imagePath"],
        serviceCategories: List<ServiceCategory>.from(
            json["serviceCategories"].map((x) => ServiceCategory.fromJson(x))),
        customer: json["customer"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "code": code,
        "name": name,
        "nameAr": nameAr,
        "nameEn": nameEn,
        "nameFr": nameFr,
        "imagePath": imagePath,
        "serviceCategories":
            List<dynamic>.from(serviceCategories.map((x) => x.toJson())),
        "customer": customer,
      };
}

class ServiceCategory {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic code;
  dynamic name;
  dynamic description;
  dynamic imagePath;

  ServiceCategory({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.code,
    required this.name,
    required this.description,
    required this.imagePath,
  });

  factory ServiceCategory.fromJson(Map<String, dynamic> json) =>
      ServiceCategory(
        id: json["id"],
        statusCode: json["statusCode"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        code: json["code"],
        name: json["name"],
        description: json["description"],
        imagePath: json["imagePath"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "code": code,
        "name": name,
        "description": description,
        "imagePath": imagePath,
      };
}

class OfferDetails {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  Offer offer;
  dynamic discountPercentage;
  dynamic newDiscount;
  dynamic offerPrice;
  dynamic offerRemainingQuantity;
  dynamic salesTargetQuantity;
  dynamic giftQuantity;
  List<dynamic> giftItems;

  OfferDetails({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.offer,
    required this.discountPercentage,
    required this.newDiscount,
    required this.offerPrice,
    required this.offerRemainingQuantity,
    required this.salesTargetQuantity,
    required this.giftQuantity,
    required this.giftItems,
  });

  factory OfferDetails.fromJson(Map<String, dynamic> json) => OfferDetails(
        id: json["id"],
        statusCode: json["statusCode"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        offer: Offer.fromJson(json["offer"]),
        discountPercentage: json["discountPercentage"],
        newDiscount: json["newDiscount"],
        offerPrice: json["offerPrice"],
        offerRemainingQuantity: json["offerRemainingQuantity"],
        salesTargetQuantity: json["salesTargetQuantity"],
        giftQuantity: json["giftQuantity"],
        giftItems: List<dynamic>.from(json["giftItems"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "offer": offer.toJson(),
        "discountPercentage": discountPercentage,
        "newDiscount": newDiscount,
        "offerPrice": offerPrice,
        "offerRemainingQuantity": offerRemainingQuantity,
        "salesTargetQuantity": salesTargetQuantity,
        "giftQuantity": giftQuantity,
        "giftItems": List<dynamic>.from(giftItems.map((x) => x)),
      };
}

class Offer {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic offerType;
  dynamic fromDate;
  dynamic endDate;
  dynamic description;

  Offer({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.offerType,
    required this.fromDate,
    required this.endDate,
    required this.description,
  });

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
        id: json["id"],
        statusCode: json["statusCode"],
        createdFromChannelId: json["createdFromChannelId"],
        createdDate: json["createdDate"],
        offerType: OfferType.fromJson(json["offerType"]),
        fromDate: DateTime.parse(json["fromDate"]),
        endDate: DateTime.parse(json["endDate"]),
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "statusCode": statusCode,
        "createdFromChannelId": createdFromChannelId,
        "createdDate": createdDate,
        "offerType": offerType.toJson(),
        "fromDate":
            "${fromDate.year.toString().padLeft(4, '0')}-${fromDate.month.toString().padLeft(2, '0')}-${fromDate.day.toString().padLeft(2, '0')}",
        "endDate":
            "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
        "description": description,
      };
}

class OfferType {
  int id;
  dynamic code;
  dynamic name;
  dynamic icon;
  dynamic description;

  OfferType({
    required this.id,
    required this.code,
    required this.name,
    required this.icon,
    required this.description,
  });

  factory OfferType.fromJson(Map<String, dynamic> json) => OfferType(
        id: json["id"],
        code: json["code"],
        name: json["name"],
        icon: json["icon"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
        "icon": icon,
        "description": description,
      };
}

class Color {
  dynamic id;
  dynamic code;
  dynamic name;

  Color({
    required this.id,
    required this.code,
    required this.name,
  });

  factory Color.fromJson(Map<String, dynamic> json) => Color(
        id: json["id"],
        code: json["code"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
      };
}

class Size {
  dynamic id;
  dynamic code;
  dynamic name;

  Size({
    required this.id,
    required this.code,
    required this.name,
  });

  factory Size.fromJson(Map<String, dynamic> json) => Size(
        id: json["id"],
        code: json["code"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
      };
}
