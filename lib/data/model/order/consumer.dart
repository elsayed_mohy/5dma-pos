// To parse this JSON data, do
//
//     final consumer = consumerFromJson(jsonString);

import 'dart:convert';

Consumer consumerFromJson(String str) => Consumer.fromJson(json.decode(str));

String consumerToJson(Consumer data) => json.encode(data.toJson());

class Consumer {
  int id;
  int statusCode;
  dynamic createdFromChannelId;
  String createdDate;
  User user;
  List<ConsumerAddress> consumerAddresses;

  Consumer({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.user,
    required this.consumerAddresses,
  });

  factory Consumer.fromJson(Map<String, dynamic> json) => Consumer(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    user: User.fromJson(json["user"]),
    consumerAddresses: List<ConsumerAddress>.from(json["consumerAddresses"].map((x) => ConsumerAddress.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "user": user.toJson(),
    "consumerAddresses": List<dynamic>.from(consumerAddresses.map((x) => x.toJson())),
  };
}

class ConsumerAddress {
  int id;
  AddressType addressType;
  dynamic isOneTimeUse;
  dynamic defaultAddress;
  dynamic phoneNumber;
  dynamic street;
  dynamic street2;
  dynamic postalCode;
  dynamic building;
  dynamic floor;
  dynamic flatNumber;
  dynamic landMark;
  dynamic lat;
  dynamic lon;
  dynamic country;
  dynamic governorate;
  dynamic city;
  dynamic title;

  ConsumerAddress({
    required this.id,
    required this.addressType,
    required this.isOneTimeUse,
    required this.defaultAddress,
    required this.phoneNumber,
    required this.street,
    required this.street2,
    required this.postalCode,
    required this.building,
    required this.floor,
    required this.flatNumber,
    required this.landMark,
    required this.lat,
    required this.lon,
    required this.country,
    required this.governorate,
    required this.city,
    required this.title,
  });

  factory ConsumerAddress.fromJson(Map<String, dynamic> json) => ConsumerAddress(
    id: json["id"],
    addressType: AddressType.fromJson(json["addressType"]),
    isOneTimeUse: json["isOneTimeUse"],
    defaultAddress: json["defaultAddress"],
    phoneNumber: json["phoneNumber"],
    street: json["street"],
    street2: json["street2"],
    postalCode: json["postalCode"],
    building: json["building"],
    floor: json["floor"],
    flatNumber: json["flatNumber"],
    landMark: json["landMark"],
    lat: json["lat"],
    lon: json["lon"],
    country: json["country"],
    governorate: json["governorate"],
    city: json["city"],
    title: json["title"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "addressType": addressType.toJson(),
    "isOneTimeUse": isOneTimeUse,
    "defaultAddress": defaultAddress,
    "phoneNumber": phoneNumber,
    "street": street,
    "street2": street2,
    "postalCode": postalCode,
    "building": building,
    "floor": floor,
    "flatNumber": flatNumber,
    "landMark": landMark,
    "lat": lat,
    "lon": lon,
    "country": country,
    "governorate": governorate,
    "city": city,
    "title": title,
  };
}

class AddressType {
  int id;
  String code;
  String name;
  String nameAr;
  String nameEn;
  String nameFr;

  AddressType({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
  });

  factory AddressType.fromJson(Map<String, dynamic> json) => AddressType(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
  };
}

class User {
  int id;
  int statusCode;
  dynamic createdFromChannelId;
  String createdDate;
  String fullName;
  String email;
  String phoneNumber;
  String preferLanguage;
  dynamic imagePath;
  dynamic expireDate;
  bool isEnabled;
  dynamic defaultAddress;
  dynamic deliverdOrders;

  User({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.fullName,
    required this.email,
    required this.phoneNumber,
    required this.preferLanguage,
    required this.imagePath,
    required this.expireDate,
    required this.isEnabled,
    required this.defaultAddress,
    required this.deliverdOrders,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    fullName: json["fullName"],
    email: json["email"],
    phoneNumber: json["phoneNumber"],
    preferLanguage: json["preferLanguage"],
    imagePath: json["imagePath"],
    expireDate: json["expireDate"],
    isEnabled: json["isEnabled"],
    defaultAddress: json["defaultAddress"],
    deliverdOrders: json["deliverdOrders"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "fullName": fullName,
    "email": email,
    "phoneNumber": phoneNumber,
    "preferLanguage": preferLanguage,
    "imagePath": imagePath,
    "expireDate": expireDate,
    "isEnabled": isEnabled,
    "defaultAddress": defaultAddress,
    "deliverdOrders": deliverdOrders,
  };
}
