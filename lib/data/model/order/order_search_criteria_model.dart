// To parse this JSON data, do
//
//     final orderSearchCriteria = orderSearchCriteriaFromJson(jsonString);

import 'dart:convert';

OrderSearchCriteria orderSearchCriteriaFromJson(String str) => OrderSearchCriteria.fromJson(json.decode(str));

String orderSearchCriteriaToJson(OrderSearchCriteria data) => json.encode(data.toJson());

class OrderSearchCriteria {
  SearchCriteria searchCriteria;
  int pageNumber;
  int pageSize;
  String sortableColumn;

  OrderSearchCriteria({
   required this.searchCriteria,
    this.pageNumber = 0,
    this.pageSize = 20,
    this.sortableColumn = 'id',
  });

  OrderSearchCriteria copyWith({
    SearchCriteria? searchCriteria,
    int? pageNumber,
    int? pageSize,
    String? sortableColumn,
  }) =>
      OrderSearchCriteria(
        searchCriteria: searchCriteria ?? this.searchCriteria,
        pageNumber: pageNumber ?? this.pageNumber,
        pageSize: pageSize ?? this.pageSize,
        sortableColumn: sortableColumn ?? this.sortableColumn,
      );

  factory OrderSearchCriteria.fromJson(Map<String, dynamic> json) => OrderSearchCriteria(
    searchCriteria: SearchCriteria.fromJson(json["searchCriteria"]),
    pageNumber: json["pageNumber"],
    pageSize: json["pageSize"],
    sortableColumn: json["sortableColumn"],
  );

  Map<String, dynamic> toJson() => {
    "searchCriteria": searchCriteria.toJson(),
    "pageNumber": pageNumber,
    "pageSize": pageSize,
    "sortableColumn": sortableColumn,
  };
}

class SearchCriteria {
  String createdFromChannelId;
  dynamic name;
  dynamic orderNumber;
  dynamic returnOrderNumber;
  dynamic orderDate;
  dynamic fromDate;
  dynamic toDate;
  dynamic orderDescription;
  dynamic consumerName;
  dynamic consumerPhoneNumber;
  dynamic preparedByName;
  dynamic deliveryMethodId;
  dynamic branchId;
  dynamic paymentMethodId;
  dynamic orderStatusId;
  dynamic itemName;
  dynamic barCode;

  SearchCriteria({
    this.createdFromChannelId = "PORTAL",
    this.name,
    this.orderNumber,
    this.returnOrderNumber,
    this.orderDate,
    this.fromDate,
    this.toDate,
    this.orderDescription,
    this.consumerName,
    this.consumerPhoneNumber,
    this.preparedByName,
    this.deliveryMethodId,
    this.branchId,
    this.paymentMethodId,
    this.orderStatusId,
    this.itemName,
    this.barCode,
  });

  SearchCriteria copyWith({
    String? createdFromChannelId,
    dynamic name,
    dynamic orderNumber,
    dynamic returnOrderNumber,
    dynamic orderDate,
    dynamic fromDate,
    dynamic toDate,
    dynamic orderDescription,
    dynamic consumerName,
    dynamic consumerPhoneNumber,
    dynamic preparedByName,
    dynamic deliveryMethodId,
    dynamic branchId,
    dynamic paymentMethodId,
    dynamic orderStatusId,
    dynamic itemName,
    dynamic barCode,
  }) =>
      SearchCriteria(
        createdFromChannelId: createdFromChannelId ?? this.createdFromChannelId,
        name: name ?? this.name,
        orderNumber: orderNumber ?? this.orderNumber,
        returnOrderNumber: returnOrderNumber ?? this.returnOrderNumber,
        orderDate: orderDate ?? this.orderDate,
        fromDate: fromDate ?? this.fromDate,
        toDate: toDate ?? this.toDate,
        orderDescription: orderDescription ?? this.orderDescription,
        consumerName: consumerName ?? this.consumerName,
        consumerPhoneNumber: consumerPhoneNumber ?? this.consumerPhoneNumber,
        preparedByName: preparedByName ?? this.preparedByName,
        deliveryMethodId: deliveryMethodId ?? this.deliveryMethodId,
        branchId: branchId ?? this.branchId,
        paymentMethodId: paymentMethodId ?? this.paymentMethodId,
        orderStatusId: orderStatusId ?? this.orderStatusId,
        itemName: itemName ?? this.itemName,
        barCode: barCode ?? this.barCode,
      );

  factory SearchCriteria.fromJson(Map<String, dynamic> json) => SearchCriteria(
    createdFromChannelId: json["createdFromChannelId"],
    name: json["name"],
    orderNumber: json["orderNumber"],
    returnOrderNumber: json["returnOrderNumber"],
    orderDate: json["orderDate"],
    fromDate: json["fromDate"],
    toDate: json["toDate"],
    orderDescription: json["orderDescription"],
    consumerName: json["consumerName"],
    consumerPhoneNumber: json["consumerPhoneNumber"],
    preparedByName: json["preparedByName"],
    deliveryMethodId: json["deliveryMethodId"],
    branchId: json["branchId"],
    paymentMethodId: json["paymentMethodId"],
    orderStatusId: json["orderStatusId"],
    itemName: json["itemName"],
    barCode: json["barCode"],
  );

  Map<String, dynamic> toJson() => {
    "createdFromChannelId": createdFromChannelId,
    "name": name,
    "orderNumber": orderNumber,
    "returnOrderNumber": returnOrderNumber,
    "orderDate": orderDate,
    "fromDate": fromDate,
    "toDate": toDate,
    "orderDescription": orderDescription,
    "consumerName": consumerName,
    "consumerPhoneNumber": consumerPhoneNumber,
    "preparedByName": preparedByName,
    "deliveryMethodId": deliveryMethodId,
    "branchId": branchId,
    "paymentMethodId": paymentMethodId,
    "orderStatusId": orderStatusId,
    "itemName": itemName,
    "barCode": barCode,
  };
}
