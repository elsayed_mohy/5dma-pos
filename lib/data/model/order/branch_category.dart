// To parse this JSON data, do
//
//     final branchCategory = branchCategoryFromJson(jsonString);

import 'dart:convert';

BranchCategory branchCategoryFromJson(String str) => BranchCategory.fromJson(json.decode(str));

String branchCategoryToJson(BranchCategory data) => json.encode(data.toJson());

class BranchCategory {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  String name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic imagePath;
  dynamic description;
  dynamic countryId;
  List<dynamic> serviceCategories;
  List<SubCategory> subCategories;
  dynamic customerCount;
  dynamic branchCategorySortRank;
  int subCategoriesCount;
  dynamic customer;

  BranchCategory({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.imagePath,
    required this.description,
    required this.countryId,
    required this.serviceCategories,
    required this.subCategories,
    required this.customerCount,
    required this.branchCategorySortRank,
    required this.subCategoriesCount,
    required this.customer,
  });

  factory BranchCategory.fromJson(Map<String, dynamic> json) => BranchCategory(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    imagePath: json["imagePath"],
    description: json["description"],
    countryId: json["countryId"],
    serviceCategories: List<dynamic>.from(json["serviceCategories"].map((x) => x)),
    subCategories: List<SubCategory>.from(json["subCategories"].map((x) => SubCategory.fromJson(x))),
    customerCount: json["customerCount"],
    branchCategorySortRank: json["branchCategorySortRank"],
    subCategoriesCount: json["subCategoriesCount"],
    customer: json["customer"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "imagePath": imagePath,
    "description": description,
    "countryId": countryId,
    "serviceCategories": List<dynamic>.from(serviceCategories.map((x) => x)),
    "subCategories": List<dynamic>.from(subCategories.map((x) => x.toJson())),
    "customerCount": customerCount,
    "branchCategorySortRank": branchCategorySortRank,
    "subCategoriesCount": subCategoriesCount,
    "customer": customer,
  };
}

class SubCategory {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  String name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  int categoryId;
  dynamic imagePath;
  dynamic description;
  List<dynamic> serviceCategories;
  List<dynamic> revisionGroups;
  dynamic customerCount;
  dynamic branchSubCategorySortRank;
  int itemCount;
  dynamic subCategoryRequestsCount;

  SubCategory({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.categoryId,
    required this.imagePath,
    required this.description,
    required this.serviceCategories,
    required this.revisionGroups,
    required this.customerCount,
    required this.branchSubCategorySortRank,
    required this.itemCount,
    required this.subCategoryRequestsCount,
  });

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    categoryId: json["categoryId"],
    imagePath: json["imagePath"],
    description: json["description"],
    serviceCategories: List<dynamic>.from(json["serviceCategories"].map((x) => x)),
    revisionGroups: List<dynamic>.from(json["revisionGroups"].map((x) => x)),
    customerCount: json["customerCount"],
    branchSubCategorySortRank: json["branchSubCategorySortRank"],
    itemCount: json["itemCount"],
    subCategoryRequestsCount: json["subCategoryRequestsCount"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "categoryId": categoryId,
    "imagePath": imagePath,
    "description": description,
    "serviceCategories": List<dynamic>.from(serviceCategories.map((x) => x)),
    "revisionGroups": List<dynamic>.from(revisionGroups.map((x) => x)),
    "customerCount": customerCount,
    "branchSubCategorySortRank": branchSubCategorySortRank,
    "itemCount": itemCount,
    "subCategoryRequestsCount": subCategoryRequestsCount,
  };
}
