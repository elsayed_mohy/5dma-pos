// To parse this JSON data, do
//
//     final returnOrders = returnOrdersFromJson(jsonString);

import 'dart:convert';

ReturnOrders returnOrdersFromJson(String str) => ReturnOrders.fromJson(json.decode(str));

String returnOrdersToJson(ReturnOrders data) => json.encode(data.toJson());

class ReturnOrders {
  int id;
  int statusCode;
  dynamic createdFromChannelId;
  String createdDate;
  String returnNumber;
  DateTime returnDate;
  String description;
  dynamic returnReasonId;
  ReturnReason returnReason;
  dynamic branchId;
  int issuingOrderId;
  int posSessionId;
  dynamic branchName;
  ReturnStatus returnStatus;
  List<ReturnOrderDetail> returnOrderDetails;

  ReturnOrders({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.returnNumber,
    required this.returnDate,
    required this.description,
    required this.returnReasonId,
    required this.returnReason,
    required this.branchId,
    required this.issuingOrderId,
    required this.posSessionId,
    required this.branchName,
    required this.returnStatus,
    required this.returnOrderDetails,
  });

  factory ReturnOrders.fromJson(Map<String, dynamic> json) => ReturnOrders(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    returnNumber: json["returnNumber"],
    returnDate: DateTime.parse(json["returnDate"]),
    description: json["description"],
    returnReasonId: json["returnReasonId"],
    returnReason: ReturnReason.fromJson(json["returnReason"]),
    branchId: json["branchId"],
    issuingOrderId: json["issuingOrderId"],
    posSessionId: json["posSessionId"],
    branchName: json["branchName"],
    returnStatus: ReturnStatus.fromJson(json["returnStatus"]),
    returnOrderDetails: List<ReturnOrderDetail>.from(json["returnOrderDetails"].map((x) => ReturnOrderDetail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "returnNumber": returnNumber,
    "returnDate": "${returnDate.year.toString().padLeft(4, '0')}-${returnDate.month.toString().padLeft(2, '0')}-${returnDate.day.toString().padLeft(2, '0')}",
    "description": description,
    "returnReasonId": returnReasonId,
    "returnReason": returnReason.toJson(),
    "branchId": branchId,
    "issuingOrderId": issuingOrderId,
    "posSessionId": posSessionId,
    "branchName": branchName,
    "returnStatus": returnStatus.toJson(),
    "returnOrderDetails": List<dynamic>.from(returnOrderDetails.map((x) => x.toJson())),
  };
}

class ReturnOrderDetail {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  String createdDate;
  int unitId;
  double qty;
  double price;
  double discount;
  double salesTaxValue;
  int branchItemId;
  int itemId;
  String itemName;
  String unitName;
  ReturnStatus returnStatus;
  dynamic cancelReason;
  dynamic description;
  int sizeId;
  int colorId;

  ReturnOrderDetail({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.unitId,
    required this.qty,
    required this.price,
    required this.discount,
    required this.salesTaxValue,
    required this.branchItemId,
    required this.itemId,
    required this.itemName,
    required this.unitName,
    required this.returnStatus,
    required this.cancelReason,
    required this.description,
    required this.sizeId,
    required this.colorId,
  });

  factory ReturnOrderDetail.fromJson(Map<String, dynamic> json) => ReturnOrderDetail(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    unitId: json["unitId"],
    qty: json["qty"],
    price: json["price"]?.toDouble(),
    discount: json["discount"],
    salesTaxValue: json["salesTaxValue"]?.toDouble(),
    branchItemId: json["branchItemId"],
    itemId: json["itemId"],
    itemName: json["itemName"],
    unitName: json["unitName"],
    returnStatus: ReturnStatus.fromJson(json["returnStatus"]),
    cancelReason: json["cancelReason"],
    description: json["description"],
    sizeId: json["sizeId"],
    colorId: json["colorId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "unitId": unitId,
    "qty": qty,
    "price": price,
    "discount": discount,
    "salesTaxValue": salesTaxValue,
    "branchItemId": branchItemId,
    "itemId": itemId,
    "itemName": itemName,
    "unitName": unitName,
    "returnStatus": returnStatus.toJson(),
    "cancelReason": cancelReason,
    "description": description,
    "sizeId": sizeId,
    "colorId": colorId,
  };
}

class ReturnStatus {
  int id;
  String code;
  String? name;
  String nameAr;
  String nameEn;
  String nameFr;
  String color;

  ReturnStatus({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.color,
  });

  factory ReturnStatus.fromJson(Map<String, dynamic> json) => ReturnStatus(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    color: json["color"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "color": color,
  };
}

class ReturnReason {
  int id;
  String code;
  String name;

  ReturnReason({
    required this.id,
    required this.code,
    required this.name,
  });

  factory ReturnReason.fromJson(Map<String, dynamic> json) => ReturnReason(
    id: json["id"],
    code: json["code"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
  };
}
