// To parse this JSON data, do
//
//     final orderDetails = orderDetailsFromJson(jsonString);

import 'dart:convert';

import 'package:khdma_pos/data/model/common/delivery_method_model.dart';
import 'package:khdma_pos/data/model/order/return_order_model.dart';

OrderDetails orderDetailsFromJson(String str) => OrderDetails.fromJson(json.decode(str));

String orderDetailsToJson(OrderDetails data) => json.encode(data.toJson());

class OrderDetails {
  Order order;
  List<Status> status;

  OrderDetails({
    required this.order,
    required this.status,
  });

  factory OrderDetails.fromJson(Map<String, dynamic> json) => OrderDetails(
    order: Order.fromJson(json["order"]),
    status: List<Status>.from(json["status"].map((x) => Status.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "order": order.toJson(),
    "status": List<dynamic>.from(status.map((x) => x.toJson())),
  };
}

class Order {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic issuingNumber;
  dynamic issuingDate;
  dynamic description;
  dynamic issuingReason;
  dynamic branchId;
  dynamic branchName;
  dynamic consumerName;
  dynamic issuingTypeId;
  dynamic consumerId;
  dynamic walkingConsumerId;
  dynamic totalAmount;
  dynamic phoneNumber;
  dynamic totalDiscount;
  dynamic totalTax;
  dynamic offers;
  Status orderStatus;
  PaymentMethod paymentMethod;
  DeliveryMethod? deliveryMethod;
  List<IssuingOrderDetail> issuingOrderDetails;
  List<ReturnOrders> returnOrders;
  dynamic preparedBy;
  dynamic consumerEmail;
  dynamic posSessionId;
  dynamic deliveryCost;
  dynamic branchEmployeeDto;
  dynamic consumerAddress;
  dynamic deliveryMethodId;
  dynamic deliveryEmployeeId;
  dynamic paidAmount;
  dynamic totalNetReturnedAmount;
  dynamic returnItemsCount;
  dynamic branchSeq;
  dynamic zatcaInvoiceStatus;
  dynamic zatcaTransactionDate;
  dynamic zatcaReportingSequence;
  dynamic zatcaReportingErrorMessage;
  dynamic zatcaReportingDate;
  dynamic xmlFilePath;
  dynamic taxExemptionReason;
  bool reported;
  dynamic qrCode;

  Order({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.issuingNumber,
    required this.issuingDate,
    required this.description,
    required this.issuingReason,
    required this.branchId,
    required this.branchName,
    required this.consumerName,
    required this.issuingTypeId,
    required this.consumerId,
    required this.walkingConsumerId,
    required this.totalAmount,
    required this.phoneNumber,
    required this.totalDiscount,
    required this.totalTax,
    required this.offers,
    required this.orderStatus,
    required this.paymentMethod,
     this.deliveryMethod,
    required this.issuingOrderDetails,
    required this.returnOrders,
    required this.preparedBy,
    required this.consumerEmail,
    required this.posSessionId,
    required this.deliveryCost,
    required this.branchEmployeeDto,
    required this.consumerAddress,
    required this.deliveryMethodId,
    required this.deliveryEmployeeId,
    required this.paidAmount,
    required this.totalNetReturnedAmount,
    required this.returnItemsCount,
    required this.branchSeq,
    required this.zatcaInvoiceStatus,
    required this.zatcaTransactionDate,
    required this.zatcaReportingSequence,
    required this.zatcaReportingErrorMessage,
    required this.zatcaReportingDate,
    required this.xmlFilePath,
    required this.taxExemptionReason,
    required this.reported,
    required this.qrCode,

  });

  factory Order.fromJson(Map<String, dynamic> json) => Order(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    issuingNumber: json["issuingNumber"],
    issuingDate: json["issuingDate"],
    description: json["description"],
    offers: json["offers"],
    issuingReason: json["issuingReason"],
    branchId: json["branchId"],
    branchName: json["branchName"],
    consumerName: json["consumerName"],
    issuingTypeId: json["issuingTypeId"],
    consumerId: json["consumerId"],
    walkingConsumerId: json["walkingConsumerId"],
    totalAmount: json["totalAmount"]?.toDouble(),
    phoneNumber: json["phoneNumber"],
    totalDiscount: json["totalDiscount"],
    totalTax: json["totalTax"]?.toDouble(),
    orderStatus: Status.fromJson(json["orderStatus"]),
    paymentMethod: PaymentMethod.fromJson(json["paymentMethod"]),
    deliveryMethod: json["deliveryMethod"]!=null ? DeliveryMethod.fromJson(json["deliveryMethod"]) :null,
    issuingOrderDetails: List<IssuingOrderDetail>.from(json["issuingOrderDetails"].map((x) => IssuingOrderDetail.fromJson(x))),
    returnOrders: List<ReturnOrders>.from(json["returnOrders"].map((x) => ReturnOrders.fromJson(x))),
    preparedBy: json["preparedBy"],
    consumerEmail: json["consumerEmail"],
    posSessionId: json["posSessionId"],
    deliveryCost: json["deliveryCost"],
    branchEmployeeDto: json["branchEmployeeDto"],
    consumerAddress: json["consumerAddress"],
    deliveryMethodId: json["deliveryMethodId"],
    deliveryEmployeeId: json["deliveryEmployeeId"],
    paidAmount: json["paidAmount"],
    totalNetReturnedAmount: json["totalNetReturnedAmount"],
    returnItemsCount: json["returnItemsCount"],
    branchSeq: json["branchSeq"],
    zatcaTransactionDate: json["zatcaTransactionDate"],
    zatcaReportingSequence: json["zatcaReportingSequence"],
    zatcaReportingErrorMessage: json["zatcaReportingErrorMessage"],
    zatcaReportingDate: json["zatcaReportingDate"],
    xmlFilePath: json["xmlFilePath"],
    taxExemptionReason: json["taxExemptionReason"],
    reported: json["reported"],
    qrCode: json["qrCode"],
    zatcaInvoiceStatus:ZatcaInvoiceStatus.fromJson(json["zatcaInvoiceStatus"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "issuingNumber": issuingNumber,
    "issuingDate": issuingDate,
    "description": description,
    "issuingReason": issuingReason,
    "branchId": branchId,
    "branchName": branchName,
    "offers": offers,
    "consumerName": consumerName,
    "issuingTypeId": issuingTypeId,
    "consumerId": consumerId,
    "walkingConsumerId": walkingConsumerId,
    "totalAmount": totalAmount,
    "phoneNumber": phoneNumber,
    "totalDiscount": totalDiscount,
    "totalTax": totalTax,
    "orderStatus": orderStatus.toJson(),
    "paymentMethod": paymentMethod.toJson(),
    "deliveryMethod": deliveryMethod?.toJson(),
    "issuingOrderDetails": List<dynamic>.from(issuingOrderDetails.map((x) => x.toJson())),
    "returnOrders": List<dynamic>.from(returnOrders.map((x) => x)),
    "preparedBy": preparedBy,
    "consumerEmail": consumerEmail,
    "posSessionId": posSessionId,
    "deliveryCost": deliveryCost,
    "branchEmployeeDto": branchEmployeeDto,
    "consumerAddress": consumerAddress,
    "deliveryMethodId": deliveryMethodId,
    "deliveryEmployeeId": deliveryEmployeeId,
    "paidAmount": paidAmount,
    "totalNetReturnedAmount": totalNetReturnedAmount,
    "returnItemsCount": returnItemsCount,
    "branchSeq": branchSeq,
    "zatcaInvoiceStatus": zatcaInvoiceStatus,
    "zatcaTransactionDate": zatcaTransactionDate,
    "zatcaReportingSequence": zatcaReportingSequence,
    "zatcaReportingErrorMessage": zatcaReportingErrorMessage,
    "zatcaReportingDate": zatcaReportingDate,
    "xmlFilePath": xmlFilePath,
    "taxExemptionReason": taxExemptionReason,
    "reported": reported,
    "qrCode": qrCode,
  };
}

class IssuingOrderDetail {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic branchItemId;
  dynamic issuingOrderId;
  dynamic unitId;
  dynamic costMethodId;
  dynamic price;
  dynamic qty;
  OrderDetailsBranchItem branchItem;
  dynamic salesTaxValue;
  dynamic discount;
  dynamic sizeId;
  dynamic colorId;
  dynamic sizeName;
  dynamic colorName;
  bool giftItem;
  dynamic giftQuantity;
  dynamic intialQty;
  dynamic itemName;
  dynamic offerDetail;
  dynamic salesTaxPercentage;
  TaxExemptionReasons? taxExemptionReasons;

  IssuingOrderDetail({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.branchItemId,
    required this.issuingOrderId,
    required this.unitId,
    required this.costMethodId,
    required this.price,
    required this.qty,
    required this.branchItem,
    required this.salesTaxValue,
    required this.discount,
    required this.sizeId,
    required this.colorId,
    required this.sizeName,
    required this.colorName,
     this.taxExemptionReasons,
    required this.giftItem,
    required this.giftQuantity,
    required this.intialQty,
    required this.itemName,
    required this.offerDetail,
    required this.salesTaxPercentage,
  });

  factory IssuingOrderDetail.fromJson(Map<String, dynamic> json) => IssuingOrderDetail(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    branchItemId: json["branchItemId"],
    issuingOrderId: json["issuingOrderId"],
    unitId: json["unitId"],
    costMethodId: json["costMethodId"],
    price: json["price"]?.toDouble(),
    qty: json["qty"],
    branchItem: OrderDetailsBranchItem.fromJson(json["branchItem"]),
    salesTaxValue: json["salesTaxValue"]?.toDouble(),
    discount: json["discount"],
    sizeId: json["sizeId"],
    colorId: json["colorId"],
    sizeName: json["sizeName"],
    colorName: json["colorName"],
    giftItem: json["giftItem"],
    giftQuantity: json["giftQuantity"],
    intialQty: json["intialQty"],
    itemName: json["itemName"],
    offerDetail: json["offerDetail"],
    salesTaxPercentage: json["salesTaxPercentage"],
    taxExemptionReasons:json["taxExemptionReasons"]!=null ? TaxExemptionReasons.fromJson(json["taxExemptionReasons"]) :null,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "branchItemId": branchItemId,
    "issuingOrderId": issuingOrderId,
    "unitId": unitId,
    "costMethodId": costMethodId,
    "price": price,
    "qty": qty,
    "branchItem": branchItem.toJson(),
    "salesTaxValue": salesTaxValue,
    "discount": discount,
    "sizeId": sizeId,
    "colorId": colorId,
    "sizeName": sizeName,
    "colorName": colorName,
    "giftItem": giftItem,
    "giftQuantity": giftQuantity,
    "intialQty": intialQty,
    "itemName": itemName,
    "offerDetail": offerDetail,
    "salesTaxPercentage": salesTaxPercentage,
    "taxExemptionReasons": taxExemptionReasons?.toJson(),
  };
}

class OrderDetailsBranchItem {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic itemId;
  dynamic branchId;
  dynamic salesPrice;
  dynamic salesPriceWithTax;
  dynamic costPrice;
  dynamic discount;
  Item item;
  bool taxable;
  double salesTaxValue;
  dynamic quantity;
  dynamic gomlaMinQty;
  dynamic salesTaxPercentage;
  dynamic reserviedQty;
  dynamic availableQty;
  bool branchHasGomla;
  dynamic mainBranchItemId;
  dynamic barCode;
  bool isSellingPriceWithTax;

  OrderDetailsBranchItem({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.itemId,
    required this.branchId,
    required this.salesPrice,
    required this.salesPriceWithTax,
    required this.costPrice,
    required this.discount,
    required this.item,
    required this.taxable,
    required this.salesTaxValue,
    required this.quantity,
    required this.gomlaMinQty,
    required this.salesTaxPercentage,
    required this.reserviedQty,
    required this.availableQty,
    required this.branchHasGomla,
    required this.mainBranchItemId,
    required this.barCode,
    required this.isSellingPriceWithTax,
  });

  factory OrderDetailsBranchItem.fromJson(Map<String, dynamic> json) => OrderDetailsBranchItem(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    itemId: json["itemId"],
    branchId: json["branchId"],
    salesPrice: json["salesPrice"]?.toDouble(),
    salesPriceWithTax: json["salesPriceWithTax"],
    costPrice: json["costPrice"],
    discount: json["discount"],
    item: Item.fromJson(json["item"]),
    taxable: json["taxable"],
    salesTaxValue: json["salesTaxValue"]?.toDouble(),
    quantity: json["quantity"],
    gomlaMinQty: json["gomlaMinQty"],
    salesTaxPercentage: json["salesTaxPercentage"],
    reserviedQty: json["reserviedQty"],
    availableQty: json["availableQty"],
    branchHasGomla: json["branchHasGomla"],
    mainBranchItemId: json["mainBranchItemId"],
    barCode: json["barCode"],
    isSellingPriceWithTax: json["isSellingPriceWithTax"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "itemId": itemId,
    "branchId": branchId,
    "salesPrice": salesPrice,
    "salesPriceWithTax": salesPriceWithTax,
    "costPrice": costPrice,
    "discount": discount,
    "item": item.toJson(),
    "taxable": taxable,
    "salesTaxValue": salesTaxValue,
    "quantity": quantity,
    "gomlaMinQty": gomlaMinQty,
    "salesTaxPercentage": salesTaxPercentage,
    "reserviedQty": reserviedQty,
    "availableQty": availableQty,
    "branchHasGomla": branchHasGomla,
    "mainBranchItemId": mainBranchItemId,
    "barCode": barCode,
    "isSellingPriceWithTax": isSellingPriceWithTax,
  };
}

class Item {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic itemNumber;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic description;
  dynamic secondaryDescription;
  dynamic expiryDays;
  BaseUnit unitClass;
  Category category;
  SubCategory subCategory;
  List<dynamic> countries;
  List<Service> serviceCategories;
  dynamic imagePath;
  dynamic customerCount;
  bool taxable;
  BaseUnit baseUnit;
  Brand? brand;
  bool hasRevision;
  Customer? customer;
  dynamic itemType;
  bool branchItemUsed;

  Item({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.itemNumber,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.description,
    required this.secondaryDescription,
    required this.expiryDays,
    required this.unitClass,
    required this.category,
    required this.subCategory,
    required this.countries,
    required this.serviceCategories,
    required this.imagePath,
    required this.customerCount,
    required this.taxable,
    required this.baseUnit,
     this.brand,
    required this.hasRevision,
     this.customer,
    required this.itemType,
    required this.branchItemUsed,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    itemNumber: json["itemNumber"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    description: json["description"],
    secondaryDescription: json["secondaryDescription"],
    expiryDays: json["expiryDays"],
    unitClass: BaseUnit.fromJson(json["unitClass"]),
    category: Category.fromJson(json["category"]),
    subCategory: SubCategory.fromJson(json["subCategory"]),
    countries: List<dynamic>.from(json["countries"].map((x) => x)),
    serviceCategories: List<Service>.from(json["serviceCategories"].map((x) => Service.fromJson(x))),
    imagePath: json["imagePath"],
    customerCount: json["customerCount"],
    taxable: json["taxable"],
    baseUnit: BaseUnit.fromJson(json["baseUnit"]),
    brand:json["brand"] != null ? Brand.fromJson(json["brand"]) : null,
    hasRevision: json["hasRevision"],
    customer:json["customer"] != null ? Customer.fromJson(json["customer"]) : null,
    itemType: json["itemType"],
    branchItemUsed: json["branchItemUsed"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "itemNumber": itemNumber,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "description": description,
    "secondaryDescription": secondaryDescription,
    "expiryDays": expiryDays,
    "unitClass": unitClass.toJson(),
    "category": category.toJson(),
    "subCategory": subCategory.toJson(),
    "countries": List<dynamic>.from(countries.map((x) => x)),
    "serviceCategories": List<dynamic>.from(serviceCategories.map((x) => x.toJson())),
    "imagePath": imagePath,
    "customerCount": customerCount,
    "taxable": taxable,
    "baseUnit": baseUnit.toJson(),
    "brand": brand?.toJson(),
    "hasRevision": hasRevision,
    "customer": customer?.toJson(),
    "itemType": itemType,
    "branchItemUsed": branchItemUsed,
  };
}

class BaseUnit {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic name;
  dynamic description;
  List<BaseUnit>? countries;
  Currency? currency;

  BaseUnit({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    this.description,
    this.countries,
    this.currency,
  });

  factory BaseUnit.fromJson(Map<String, dynamic> json) => BaseUnit(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    description: json["description"],
    countries: json["countries"] == null ? [] : List<BaseUnit>.from(json["countries"]!.map((x) => BaseUnit.fromJson(x))),
    currency: json["currency"] == null ? null : Currency.fromJson(json["currency"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "description": description,
    "countries": countries == null ? [] : List<dynamic>.from(countries!.map((x) => x.toJson())),
    "currency": currency?.toJson(),
  };
}

class Currency {
  int id;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic symbol;
  dynamic fractionalUnit;

  Currency({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.symbol,
    required this.fractionalUnit,
  });

  factory Currency.fromJson(Map<String, dynamic> json) => Currency(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    symbol: json["symbol"],
    fractionalUnit: json["fractionalUnit"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "symbol": symbol,
    "fractionalUnit": fractionalUnit,
  };
}

class Brand {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic name;
  dynamic code;
  dynamic brandRequestsCount;
  dynamic customer;

  Brand({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.code,
    required this.brandRequestsCount,
    required this.customer,
  });

  factory Brand.fromJson(Map<String, dynamic> json) => Brand(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    code: json["code"],
    brandRequestsCount: json["brandRequestsCount"],
    customer: json["customer"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "code": code,
    "brandRequestsCount": brandRequestsCount,
    "customer": customer,
  };
}

class Category {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic description;
  dynamic subCategoriesCount;
  dynamic customer;
  dynamic categoryRequestsCount;

  Category({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.description,
    required this.subCategoriesCount,
    required this.customer,
    required this.categoryRequestsCount,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    description: json["description"],
    subCategoriesCount: json["subCategoriesCount"],
    customer: json["customer"],
    categoryRequestsCount: json["categoryRequestsCount"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "description": description,
    "subCategoriesCount": subCategoriesCount,
    "customer": customer,
    "categoryRequestsCount": categoryRequestsCount,
  };
}

class Customer {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic customerCode;
  dynamic legalEntityName;
  dynamic legalEntityNumber;
  dynamic taxNumber;
  User user;
  dynamic domain;
  BaseUnit subscriptionPackage;
  dynamic maxWorkingHour;
  dynamic externalLinkForAttendance;

  Customer({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.customerCode,
    required this.legalEntityName,
    required this.legalEntityNumber,
    required this.taxNumber,
    required this.user,
    required this.domain,
    required this.subscriptionPackage,
    required this.maxWorkingHour,
    required this.externalLinkForAttendance,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    customerCode: json["customerCode"],
    legalEntityName: json["legalEntityName"],
    legalEntityNumber: json["legalEntityNumber"],
    taxNumber: json["taxNumber"],
    user: User.fromJson(json["user"]),
    domain: json["domain"],
    subscriptionPackage: BaseUnit.fromJson(json["subscriptionPackage"]),
    maxWorkingHour: json["maxWorkingHour"],
    externalLinkForAttendance: json["externalLinkForAttendance"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "customerCode": customerCode,
    "legalEntityName": legalEntityName,
    "legalEntityNumber": legalEntityNumber,
    "taxNumber": taxNumber,
    "user": user.toJson(),
    "domain": domain,
    "subscriptionPackage": subscriptionPackage.toJson(),
    "maxWorkingHour": maxWorkingHour,
    "externalLinkForAttendance": externalLinkForAttendance,
  };
}

class User {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic fullName;
  dynamic email;
  dynamic phoneNumber;
  dynamic imagePath;
  dynamic preferLanguage;
  bool isForceChangePass;

  User({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.fullName,
    required this.email,
    required this.phoneNumber,
    required this.imagePath,
    required this.preferLanguage,
    required this.isForceChangePass,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    fullName: json["fullName"],
    email: json["email"],
    phoneNumber: json["phoneNumber"],
    imagePath: json["imagePath"],
    preferLanguage: json["preferLanguage"],
    isForceChangePass: json["isForceChangePass"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "fullName": fullName,
    "email": email,
    "phoneNumber": phoneNumber,
    "imagePath": imagePath,
    "preferLanguage": preferLanguage,
    "isForceChangePass": isForceChangePass,
  };
}

class Service {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic code;
  dynamic name;
  dynamic nameEn;
  dynamic description;
  dynamic imagePath;
  Service? service;

  Service({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.code,
    required this.name,
    this.nameEn,
    required this.description,
    required this.imagePath,
    this.service,
  });

  factory Service.fromJson(Map<String, dynamic> json) => Service(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    code: json["code"],
    name: json["name"],
    nameEn: json["nameEn"],
    description: json["description"],
    imagePath: json["imagePath"],
    service: json["service"] == null ? null : Service.fromJson(json["service"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "code": code,
    "name": name,
    "nameEn": nameEn,
    "description": description,
    "imagePath": imagePath,
    "service": service?.toJson(),
  };
}

class SubCategory {
  int id;
  dynamic statusCode;
  dynamic createdFromChannelId;
  dynamic createdDate;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic imagePath;
  dynamic customerCount;
  dynamic branchSubCategorySortRank;
  dynamic itemCount;
  dynamic subCategoryRequestsCount;

  SubCategory({
    required this.id,
    required this.statusCode,
    required this.createdFromChannelId,
    required this.createdDate,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.imagePath,
    required this.customerCount,
    required this.branchSubCategorySortRank,
    required this.itemCount,
    required this.subCategoryRequestsCount,
  });

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
    id: json["id"],
    statusCode: json["statusCode"],
    createdFromChannelId: json["createdFromChannelId"],
    createdDate: json["createdDate"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    imagePath: json["imagePath"],
    customerCount: json["customerCount"],
    branchSubCategorySortRank: json["branchSubCategorySortRank"],
    itemCount: json["itemCount"],
    subCategoryRequestsCount: json["subCategoryRequestsCount"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "statusCode": statusCode,
    "createdFromChannelId": createdFromChannelId,
    "createdDate": createdDate,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "imagePath": imagePath,
    "customerCount": customerCount,
    "branchSubCategorySortRank": branchSubCategorySortRank,
    "itemCount": itemCount,
    "subCategoryRequestsCount": subCategoryRequestsCount,
  };
}

class TaxExemptionReasons {
  int id;
  dynamic code;
  dynamic name;
  dynamic taxExemptionReasonCode;
  dynamic taxExemptionReasonDescriptionAr;
  dynamic taxExemptionReasonDescriptionEn;
  dynamic taxExemptionReasonDescriptionFr;

  TaxExemptionReasons({
    required this.id,
    required this.code,
    required this.name,
    required this.taxExemptionReasonCode,
    required this.taxExemptionReasonDescriptionAr,
    required this.taxExemptionReasonDescriptionEn,
    required this.taxExemptionReasonDescriptionFr,
  });

  factory TaxExemptionReasons.fromJson(Map<String, dynamic> json) => TaxExemptionReasons(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    taxExemptionReasonCode: json["taxExemptionReasonCode"],
    taxExemptionReasonDescriptionAr: json["taxExemptionReasonDescriptionAr"],
    taxExemptionReasonDescriptionEn: json["taxExemptionReasonDescriptionEn"],
    taxExemptionReasonDescriptionFr: json["taxExemptionReasonDescriptionFr"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "taxExemptionReasonCode": taxExemptionReasonCode,
    "taxExemptionReasonDescriptionAr": taxExemptionReasonDescriptionAr,
    "taxExemptionReasonDescriptionEn": taxExemptionReasonDescriptionEn,
    "taxExemptionReasonDescriptionFr": taxExemptionReasonDescriptionFr,
  };
}

class Status {
  int id;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic color;
  dynamic statusType;
  bool isConditional;
  dynamic sortRanking;

  Status({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.color,
    required this.statusType,
    required this.isConditional,
    required this.sortRanking,
  });

  factory Status.fromJson(Map<String, dynamic> json) => Status(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    color: json["color"],
    statusType: json["statusType"],
    isConditional: json["isConditional"],
    sortRanking: json["sortRanking"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "color": color,
    "statusType": statusType,
    "isConditional": isConditional,
    "sortRanking": sortRanking,
  };
}

class PaymentMethod {
  int id;
  dynamic code;
  dynamic name;
  dynamic nameAr;
  dynamic nameEn;
  dynamic nameFr;
  dynamic zatcaCode;
  dynamic description;
  dynamic zakatName;

  PaymentMethod({
    required this.id,
    required this.code,
    required this.name,
    required this.nameAr,
    required this.nameEn,
    required this.nameFr,
    required this.zatcaCode,
    required this.description,
    required this.zakatName,
  });

  factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
    id: json["id"],
    code: json["code"],
    name: json["name"],
    nameAr: json["nameAr"],
    nameEn: json["nameEn"],
    nameFr: json["nameFr"],
    zatcaCode: json["zatcaCode"],
    description: json["description"],
    zakatName: json["zakatName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "code": code,
    "name": name,
    "nameAr": nameAr,
    "nameEn": nameEn,
    "nameFr": nameFr,
    "zatcaCode": zatcaCode,
    "description": description,
    "zakatName": zakatName,
  };
}
class ZatcaInvoiceStatus {
  String code;
  String color;
  int id;
  String name;

  ZatcaInvoiceStatus({
    required this.code,
    required this.color,
    required this.id,
    required this.name,
  });

  factory ZatcaInvoiceStatus.fromJson(Map<String, dynamic> json) => ZatcaInvoiceStatus(
    code: json["code"],
    color: json["color"],
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "color": color,
    "id": id,
    "name": name,
  };
}
