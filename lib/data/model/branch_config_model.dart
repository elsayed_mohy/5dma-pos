import 'dart:convert';

import 'common/check_in_options_model.dart';
import 'common/country_model.dart';

class BranchConfig {
  int id;
  String branchName;
  bool hasFood;
  dynamic customerId;
  dynamic roundOption;
  dynamic roundOptionReturn;
  dynamic posInvoiceType;
  dynamic legalEntityNumber;
  dynamic taxNumber;
  dynamic taxPercentageRate;
  bool? supervisorCanCheckIn;
  bool? enableGeolocationValidation;
  dynamic allowedMetersTocheckIn;
  double? lat;
  double? lon;
  List<CheckInOption> checkInOptions;
  Country? country;

  BranchConfig({
    required this.id,
    required this.branchName,
    required this.hasFood,
    required this.customerId,
    required this.roundOption,
    required this.roundOptionReturn,
    required this.posInvoiceType,
    required this.legalEntityNumber,
    required this.taxNumber,
    required this.taxPercentageRate,
     this.supervisorCanCheckIn,
     this.enableGeolocationValidation,
    required this.allowedMetersTocheckIn,
     this.lat,
     this.lon,
    required this.checkInOptions,
     this.country,
  });

  BranchConfig copyWith({
    int? id,
    String? branchName,
    bool? hasFood,
    int? customerId,
    dynamic roundOption,
    String? roundOptionReturn,
    String? posInvoiceType,
    String? legalEntityNumber,
    String? taxNumber,
    int? taxPercentageRate,
    dynamic supervisorCanCheckIn,
    dynamic enableGeolocationValidation,
    int? allowedMetersTocheckIn,
    double? lat,
    double? lon,
    List<CheckInOption>? checkInOptions,
    Country? country,
  }) =>
      BranchConfig(
        id: id ?? this.id,
        branchName: branchName ?? this.branchName,
        hasFood: hasFood ?? this.hasFood,
        customerId: customerId ?? this.customerId,
        roundOption: roundOption ?? this.roundOption,
        roundOptionReturn: roundOptionReturn ?? this.roundOptionReturn,
        posInvoiceType: posInvoiceType ?? this.posInvoiceType,
        legalEntityNumber: legalEntityNumber ?? this.legalEntityNumber,
        taxNumber: taxNumber ?? this.taxNumber,
        taxPercentageRate: taxPercentageRate ?? this.taxPercentageRate,
        supervisorCanCheckIn: supervisorCanCheckIn ?? this.supervisorCanCheckIn,
        enableGeolocationValidation: enableGeolocationValidation ?? this.enableGeolocationValidation,
        allowedMetersTocheckIn: allowedMetersTocheckIn ?? this.allowedMetersTocheckIn,
        lat: lat ?? this.lat,
        lon: lon ?? this.lon,
        checkInOptions: checkInOptions ?? this.checkInOptions,
        country: country ?? this.country,
      );

  factory BranchConfig.fromRawJson(String str) => BranchConfig.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BranchConfig.fromJson(Map<String, dynamic> json) => BranchConfig(
    id: json["id"],
    branchName: json["branchName"],
    hasFood: json["hasFood"],
    customerId: json["customerId"],
    roundOption: json["roundOption"],
    roundOptionReturn: json["roundOptionReturn"],
    posInvoiceType: json["posInvoiceType"],
    legalEntityNumber: json["legalEntityNumber"],
    taxNumber: json["taxNumber"],
    taxPercentageRate: json["taxPercentageRate"],
    supervisorCanCheckIn: json["supervisorCanCheckIn"],
    enableGeolocationValidation: json["enableGeolocationValidation"],
    allowedMetersTocheckIn: json["allowedMetersTocheckIn"],
    lat: json["lat"]?.toDouble(),
    lon: json["lon"]?.toDouble(),
    checkInOptions:json["checkInOptions"] == null ? [] : List<CheckInOption>.from(json["checkInOptions"].map((x) => CheckInOption.fromJson(x))),
    country:json["country"] == null ? null :Country.fromJson(json["country"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "branchName": branchName,
    "hasFood": hasFood,
    "customerId": customerId,
    "roundOption": roundOption,
    "roundOptionReturn": roundOptionReturn,
    "posInvoiceType": posInvoiceType,
    "legalEntityNumber": legalEntityNumber,
    "taxNumber": taxNumber,
    "taxPercentageRate": taxPercentageRate,
    "supervisorCanCheckIn": supervisorCanCheckIn,
    "enableGeolocationValidation": enableGeolocationValidation,
    "allowedMetersTocheckIn": allowedMetersTocheckIn,
    "lat": lat,
    "lon": lon,
    "checkInOptions": List<dynamic>.from(checkInOptions.map((x) => x.toJson())),
    "country": country?.toJson(),
  };
}
