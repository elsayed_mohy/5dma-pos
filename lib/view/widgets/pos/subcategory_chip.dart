import 'package:flutter/material.dart';

import '../../../data/model/order/branch_category.dart';
import '../../../utils/constants/colors.dart';
import '../shared/text_utils.dart';

Widget subCategoryChip(SubCategory subCategory) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 10),
    decoration: ShapeDecoration(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
    ),
    child: Chip(
      elevation: 0,
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      backgroundColor: ColorsManager.disabled.withOpacity(0.15),
      label: TextUtils(
        text: subCategory.name,
        fontSize: 10,
      ),
      // onDeleted: () {
      //   // Perform any action you want here when the user clicks on the delete icon
      // },
    ),
  );
}