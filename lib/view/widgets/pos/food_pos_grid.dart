import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/food/food_item.dart';
import 'package:khdma_pos/logic/controllers/food-pos_controller.dart';

import '../../../utils/constants/app_urls.dart';
import '../shared/add_button.dart';
import '../shared/card_container.dart';
import '../shared/text_utils.dart';

Widget FoodPosGrid(BuildContext context, List<FoodItem> foodItems) {
  return GetBuilder<FoodPOSController>(
      init: FoodPOSController(),
      builder: (FoodPOSController controller) {
        return MasonryGridView.count(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          mainAxisSpacing: 2,
          crossAxisSpacing: 2,
          itemCount: foodItems.length > 100 ? 50 : foodItems.length,
          itemBuilder: (context, index) {
            var foodItem = foodItems[index];
            return CardContainer(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        // getBottomSheet(
                        //   MediaQuery.of(context).size.height,
                        //   ProductDetailsBottomSheet(
                        //     branchItem: foodItem,
                        //   ),
                        // );
                      },
                      child: CachedNetworkImage(
                        imageUrl: foodItem?.imagePath != null
                            ? AppUrls.baseImageUrl +
                            foodItem?.imagePath
                            : '',
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Container(
                          height: 60,
                          child: SizedBox(
                              height: 30,
                              width: 30,
                              child: Center(
                                  child: CircularProgressIndicator(
                                      value: downloadProgress.progress))),
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          'assets/images/ui/placeholder.png',
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextUtils(
                              text: foodItem.name ?? '',
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                          SizedBox(
                            height: 5,
                          ),
                          AddButtons(
                            IncreaseAction: () {
                              // if (branchItem.item?.hasRevision !=
                              //         null &&
                              //     branchItem.item?.hasRevision ==
                              //         true && (branchItem.color==null && branchItem.size ==null)) {
                              //   getBottomSheet(
                              //     MediaQuery.of(context).size.height,
                              //     // ProductDetailsBottomSheet(
                              //     //   branchItem: branchItem,
                              //     // ),
                              //   );
                              // } else {
                              //   controller.selectItem(foodItem);
                              // }
                            },
                            decreaseAction: () {
                              // if (branchItem.item?.hasRevision !=
                              //         null &&
                              //     branchItem.item?.hasRevision ==
                              //         true && (branchItem.color==null && branchItem.size ==null)) {
                              //   getBottomSheet(
                              //     MediaQuery.of(context).size.height,
                              //     ProductDetailsBottomSheet(
                              //       branchItem: branchItem,
                              //     ),
                              //   );
                              // } else {
                              //   controller.deselectItem(foodItem);
                              // }
                            },
                            value: controller.getItemQty(foodItem),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      });
}
