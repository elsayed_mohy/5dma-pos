import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/view/widgets/pos/pos_grid.dart';
import 'package:khdma_pos/view/widgets/pos/subcategory_chip.dart';
import 'package:khdma_pos/view/widgets/shared/flib_card.dart';

import '../../../logic/controllers/pos_controller.dart';
import 'category_chip.dart';

class POSMainView extends StatelessWidget {
  const POSMainView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<POSController>(
        init: POSController(),
        builder: (POSController controller) {
          if (controller.isLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FlipCard(
                    frontComponent: SizedBox(
                      height: 30,
                      child: ListView(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        children: List<Widget>.from(
                          controller.branchCategories
                              .map((item) => GestureDetector(
                                    onTap: () {
                                      controller.showSubCategories(item);
                                    },
                                    child: categoryChip(item),
                                  )), // Replace Text() with your desired widget
                        ),
                      ),
                    ),
                    backComponent: SizedBox(
                      height: 30,
                      child: Row(
                        children: [
                          IconButton(
                            icon: Icon(CupertinoIcons.xmark_circle_fill),
                            color: Colors.white,
                            onPressed: () {
                              controller.hideSubCategories();
                            },
                          ),
                          SizedBox(width: 5,),
                          Expanded(
                            child: ListView(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              children: List<Widget>.from(
                                controller.selectedBranchCategory!.subCategories.map(
                                  (item) => GestureDetector(
                                      onTap: () {
                                        controller.showSubCategoriesItems(item);
                                      },
                                      child: subCategoryChip(item)),
                                ), // Replace Text() with your desired widget
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
                SizedBox(
                  height: 30,
                ),
                if (controller.isItemsLoading == true)
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                if (controller.isItemsLoading == false)
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: ListView(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        children: [
                          posGrid(context, controller.filteredBranchItems)
                        ],
                      ),
                    ),
                  ),
              ],
            );
          }
        });
  }
}
