import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';
import 'package:khdma_pos/view/widgets/product_details_bottom_sheet.dart';

import '../../../data/model/order/branch_item.dart';
import '../../../utils/constants/app_urls.dart';
import '../shared/add_button.dart';
import '../shared/card_container.dart';
import '../shared/get_bottom_sheet.dart';
import '../shared/text_utils.dart';

Widget posGrid(BuildContext context, List<BranchItem> branchItems) {
  return GetBuilder<POSController>(
      init: POSController(),
      builder: (POSController controller) {
        return MasonryGridView.count(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: 2,
          mainAxisSpacing: 2,
          crossAxisSpacing: 2,
          itemCount: branchItems.length > 100 ? 50 : branchItems.length,
          itemBuilder: (context, index) {
            var branchItem = branchItems[index];
            return CardContainer(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        getBottomSheet(
                          MediaQuery.of(context).size.height,
                          ProductDetailsBottomSheet(
                            branchItem: branchItem,
                          ),
                        );
                      },
                      child: CachedNetworkImage(
                        imageUrl: branchItem.item?.imagePath != null
                            ? AppUrls.baseImageUrl +
                            branchItem.item?.imagePath
                            : '',
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Container(
                          height: 60,
                          child: SizedBox(
                              height: 30,
                              width: 30,
                              child: Center(
                                  child: CircularProgressIndicator(
                                      value: downloadProgress.progress))),
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          'assets/images/ui/placeholder.png',
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextUtils(
                              text: branchItem.item?.name ?? '',
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                          SizedBox(
                            height: 5,
                          ),
                          AddButtons(
                            IncreaseAction: () {
                              if (branchItem.item?.hasRevision !=
                                      null &&
                                  branchItem.item?.hasRevision ==
                                      true && (branchItem.color==null && branchItem.size ==null)) {
                                getBottomSheet(
                                  MediaQuery.of(context).size.height,
                                  ProductDetailsBottomSheet(
                                    branchItem: branchItem,
                                  ),
                                );
                              } else {
                                controller.selectItem(branchItem);
                              }
                            },
                            decreaseAction: () {
                              if (branchItem.item?.hasRevision !=
                                      null &&
                                  branchItem.item?.hasRevision ==
                                      true && (branchItem.color==null && branchItem.size ==null)) {
                                getBottomSheet(
                                  MediaQuery.of(context).size.height,
                                  ProductDetailsBottomSheet(
                                    branchItem: branchItem,
                                  ),
                                );
                              } else {
                                controller.deselectItem(branchItem);
                              }
                            },
                            value: controller.getItemQty(branchItem),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      });
}
