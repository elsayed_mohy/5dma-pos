import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/l10n/app_localizations.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';
import 'package:khdma_pos/view/widgets/shared/secondary_button.dart';

import '../shared/text_utils.dart';

class SessionInformationPopup extends StatelessWidget {
  SessionInformationPopup({super.key});

  final TextEditingController nameController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: TextUtils(
            text: 'Session Information',
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: 30),
        Container(
          width: double.infinity,
          // height: MediaQuery.of(context).size.height / 1.6,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),

            child: GetBuilder<POSController>(
                init: POSController(),
                builder: (POSController controller) {
                  String startTime = controller.sessionData?.startTime;
                  List<String> lst = startTime.split(" ");
                  return Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextUtils(text: 'start session date'),
                          TextUtils(text:lst[0])
                        ],
                      ),
                      const SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextUtils(text: 'start session date'),
                          TextUtils(text: lst[1])
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: SecondaryButton(
                            text: AppLocalizations.of(context)!.cancel,
                            onPressed: () {
                              Get.back();
                            }),
                      )
                    ],
                  );
                }),


          ),
        ),
      ],
    );
  }
}
