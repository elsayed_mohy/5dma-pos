import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/data/model/order/branch_item.dart';

import 'package:khdma_pos/view/widgets/shared/card_container.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../l10n/app_localizations.dart';
import '../../../logic/controllers/pos_controller.dart';
import '../../../utils/constants/colors.dart';
import '../shared/scrollable_widget.dart';

class POSTableView extends StatelessWidget {
  POSTableView({super.key});

  final controller = Get.put(POSController());

  GetStorage storage = GetStorage();

  int? sortColumnIndex;

  bool isAscending = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          Obx(() {
            if (controller.isLoadingTableView.value) {
              return SizedBox(
                height: 300,
                child: CircularProgressIndicator(),
              );
            } else {
              return ScrollableWidget(
                  child: buildDataTable(controller.selectedItems,context));
            }
          }),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: CardContainer(
              // constraints: const BoxConstraints(maxWidth: 350),
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                children: [
                  buildExpansionList(children: [
                    buildListTile(title: AppLocalizations.of(context)!.priceWithoutTax,value: controller.totalPrice),
                    buildListTile(title: AppLocalizations.of(context)!.discount,value: controller.totalDiscount),
                    buildListTile(title: AppLocalizations.of(context)!.taxAmount,value: controller.totalTax),
                  ], title: AppLocalizations.of(context)!.total, value: ((controller.totalPrice + controller.totalTax) - controller.totalDiscount).toStringAsFixed(2)),

                  buildExpansionList(
                      title: AppLocalizations.of(context)!.paymentMethod,
                      value: controller.selectedPaymentMethod?.name ??'',
                      children: [
                        ToggleButtons(
                          borderRadius: BorderRadius.circular(5),
                          selectedColor: ColorsManager.white,
                          fillColor: ColorsManager.main,
                          // borderColor: Colors.blueAccent[100],
                          // selectedBorderColor: ColorsManager.palatinateBlue,
                          borderWidth: 1,
                          splashColor: Colors.blue[100],
                          constraints: BoxConstraints.expand(
                              width:
                                  MediaQuery.of(context).size.width / (1.2 * 5),
                              height: 40),
                          isSelected: controller.paymentIsSelected,
                          // color: Colors.black,
                          // selectedColor: Colors.blue,
                          children: List<Widget>.from(
                              controller.paymentMethods.map((method) => Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(
                                      method.name,
                                      style: TextStyle(fontSize: 13),
                                    ),
                                  ))),

                          onPressed: (int index) {
                            // transactionsController.changeButtonTabIndex(index);
                          },
                        )
                      ]),
                  buildExpansionList(
                      title: AppLocalizations.of(context)!.printLang,
                      value: AppLocalizations.of(context)!.ar,
                      children: [
                        ToggleButtons(
                          borderRadius: BorderRadius.circular(5),
                          selectedColor: ColorsManager.white,
                          fillColor: ColorsManager.main,
                          // borderColor: Colors.blueAccent[100],
                          // selectedBorderColor: ColorsManager.palatinateBlue,
                          borderWidth: 1,
                          splashColor: Colors.blue[100],
                          constraints: BoxConstraints.expand(
                              width:
                              MediaQuery.of(context).size.width / (1.2 * 4),
                              height: 40),
                          isSelected: [true, false, false],
                          // color: Colors.black,
                          // selectedColor: Colors.blue,
                          children: [
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                AppLocalizations.of(context)!.ar,
                                style: TextStyle(fontSize: 13),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                AppLocalizations.of(context)!.en,
                                style: TextStyle(fontSize: 13),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                AppLocalizations.of(context)!.fr,
                                style: TextStyle(fontSize: 13),
                              ),
                            ),
                          ],
                          onPressed: (int index) {
                            // transactionsController.changeButtonTabIndex(index);
                          },
                        )
                      ]),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildExpansionList(
      {required title, required value, required List<Widget> children}) {
    return ExpansionTile(
      title: Expanded(
        child: TextUtils(
          text: title,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
      trailing: SizedBox(
        width: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextUtils(
              text: value.toString(),
              fontWeight: FontWeight.w500,
              fontSize: 12,
            ),
            SizedBox(
              width: 12,
            ),
            Icon(
              CupertinoIcons.chevron_down,
              size: 17,
            )
          ],
        ),
      ),
      childrenPadding: EdgeInsets.symmetric(horizontal: 16),
      children: children,
    );
  }

  Widget buildListTile({required String title, dynamic value}) {
    return ListTile(
      dense: true,
      visualDensity: VisualDensity(vertical: -4),
      title: TextUtils(
        text: title,
        fontSize: 12,
        fontWeight: FontWeight.w500,
      ),
      trailing: TextUtils(
        text: value.toString(),
        fontSize: 12,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget buildDataTable(List<BranchItem> selectedItems,BuildContext context) {
    final columns = [
      AppLocalizations.of(context)!.barcode,
      AppLocalizations.of(context)!.description,
      AppLocalizations.of(context)!.availableQuantity,
      AppLocalizations.of(context)!.priceWithTax,
      AppLocalizations.of(context)!.priceWithoutTax,
      AppLocalizations.of(context)!.taxAmount,
      AppLocalizations.of(context)!.discount,
      AppLocalizations.of(context)!.amountRequired,
    ];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: CardContainer(
        child: DataTable(
          sortAscending: isAscending,
          sortColumnIndex: sortColumnIndex,
          columns: getColumns(columns),
          rows: getRows(selectedItems),
        ),
      ),
    );
  }

  List<DataColumn> getColumns(List<String> columns) => columns
      .map((String column) => DataColumn(
            label: TextUtils(
              text: column,
              fontSize: 12,
              fontWeight: FontWeight.w500,
            ),
            // onSort: onSort,
          ))
      .toList();

  List<DataRow> getRows(List<BranchItem> branchItems) =>
      branchItems.map((BranchItem branchItem) {
        final cells = [
          branchItem.barCode,
          branchItem.item?.description ?? '',
          branchItem.availableQty,
          branchItem.salesPrice,
          branchItem.salesTaxValue,
          branchItem.discount,
          branchItem.salesTaxValue,
          branchItem.orderedCount,
        ];

        return DataRow(cells: getCells(cells));
      }).toList();

  List<DataCell> getCells(List<dynamic> cells) => cells
      .map((data) => DataCell(TextUtils(
            text: '$data',
            fontSize: 11,
          )))
      .toList();

  int compareString(bool ascending, String value1, String value2) =>
      ascending ? value1.compareTo(value2) : value2.compareTo(value1);
}

// void onSort(int columnIndex, bool ascending) {
//   // if (columnIndex == 0) {
//   //   users.sort((user1, user2) =>
//   //       compareString(ascending, user1.firstName, user2.firstName));
//   // } else if (columnIndex == 1) {
//   //   users.sort((user1, user2) =>
//   //       compareString(ascending, user1.lastName, user2.lastName));
//   // } else if (columnIndex == 2) {
//   //   users.sort((user1, user2) =>
//   //       compareString(ascending, '${user1.age}', '${user2.age}'));
//   // }
//
//   // setState(() {
//   //   sortColumnIndex = columnIndex;
//   //   isAscending = ascending;
//   // });
// }
