import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/order/item_size_model.dart';
import 'package:khdma_pos/logic/controllers/item_controller.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';
import 'package:khdma_pos/view/widgets/shared/app_dropdown_input.dart';
import 'package:khdma_pos/view/widgets/shared/primary_button.dart';
import 'package:khdma_pos/view/widgets/shared/secondary_button.dart';
import 'package:khdma_pos/view/widgets/shared/shared_text_form_field.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../data/model/order/branch_item.dart';
import '../../l10n/app_localizations.dart';
import '../../logic/controllers/session_controller.dart';
import '../../services/common/max_value_formatter.dart';

class ProductDetailsBottomSheet extends StatefulWidget {
  ProductDetailsBottomSheet({super.key, required this.branchItem});

  final BranchItem branchItem;

  @override
  State<ProductDetailsBottomSheet> createState() =>
      _ProductDetailsBottomSheetState();
}

class _ProductDetailsBottomSheetState extends State<ProductDetailsBottomSheet> {
  final controller = Get.put(ItemController());

  bool enableDiscount = true;

  @override
  void initState() {
    // controller.getRevisionsItems();
    if (widget.branchItem.item?.hasRevision == true) {
      controller.getColors(widget.branchItem.id);
    }

    if (controller.hasDiscountPermission() == false) {
      setState(() {
        enableDiscount = false;
      });
    }
    controller.setMaxDiscountValue(
        widget.branchItem.salesPrice + widget.branchItem.salesTaxValue);
    controller.barcodeController.text = widget.branchItem.barCode;
    controller.nameController.text = widget.branchItem.item?.name ?? '';
    controller.priceWithTaxController.text =
        (widget.branchItem.salesPrice + widget.branchItem.salesTaxValue)
            .toString();
    controller.priceWithoutTaxController.text =
        widget.branchItem.salesPrice.toString();
    controller.discountController.text = widget.branchItem.discount.toString();
    controller.priceController.text = widget.branchItem.discount != null
        ? (widget.branchItem.salesPrice - widget.branchItem.discount).toString()
        : widget.branchItem.salesPrice.toString();
    controller.availableQtyController.text =
        widget.branchItem.availableQty.toString();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            title: Text(
          AppLocalizations.of(context)!.itemsInTheCart,
        )),
        body: SingleChildScrollView(
          child: Column(
            children: [
              GetBuilder<ItemController>(
                  init: ItemController(),
                  builder: (ItemController controller) {
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(25, 10, 25, 25),
                      child: Form(
                        key: controller.formKey,
                        child: Column(
                          children: [
                            SharedTextFormField(
                              controller: controller.barcodeController,
                              obscureText: false,
                              enabled: false,
                              keyboardType: TextInputType.number,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'barcode is required';
                                }
                                return null;
                              },
                              hintText: AppLocalizations.of(context)!.barcode,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              controller: controller.nameController,
                              obscureText: false,
                              enabled: false,
                              keyboardType: TextInputType.text,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'name is required';
                                }
                                return null;
                              },
                              hintText: AppLocalizations.of(context)!.name,
                            ),
                            const SizedBox(height: 15),
                            if (controller.units.isNotEmpty)
                              Column(
                                children: [
                                  AppDropdownInput(
                                    hintText:
                                        AppLocalizations.of(context)!.unit,
                                    options: controller.units,
                                    value:
                                        widget.branchItem.item?.baseUnit != null
                                            ? controller.units.firstWhere(
                                                (element) =>
                                                    element.id ==
                                                    widget.branchItem.item
                                                        ?.baseUnit?.id)
                                            : controller.units[0],
                                    onChanged: (BaseUnit value) {
                                      controller.update();
                                    },
                                    getLabel: (value) => value.name,
                                  ),
                                  const SizedBox(height: 15),
                                ],
                              ),
                            if (widget.branchItem.item?.hasRevision == true &&
                                controller.colors.isNotEmpty &&
                                (widget.branchItem.size == null ||
                                    widget.branchItem.color == null))
                              Column(
                                children: [
                                  AppDropdownInput(
                                    hintText:
                                        AppLocalizations.of(context)!.color,
                                    options: controller.colors,
                                    value: controller.colors[0],
                                    onChanged: (Color value) {
                                      print(widget.branchItem.id);
                                      controller.getSizes(
                                          widget.branchItem.id, value.id);
                                      controller.update();
                                    },
                                    getLabel: (value) => value.name,
                                  ),
                                  const SizedBox(height: 15),
                                ],
                              ),
                            if (widget.branchItem.item?.hasRevision == true &&
                                controller.sizes.isNotEmpty &&
                                (widget.branchItem.size == null ||
                                    widget.branchItem.color == null))
                              Column(
                                children: [
                                  AppDropdownInput(
                                    hintText:
                                        AppLocalizations.of(context)!.size,
                                    options: controller.sizes,
                                    value: controller.sizes[0],
                                    onChanged: (ItemSize value) {
                                      controller.update();
                                    },
                                    getLabel: (value) => value.size.name,
                                  ),
                                  const SizedBox(height: 15),
                                ],
                              ),
                            SharedTextFormField(
                              controller: controller.priceWithTaxController,
                              obscureText: false,
                              enabled: false,
                              keyboardType: TextInputType.text,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'Price With Tax is required';
                                }
                                return null;
                              },
                              hintText:
                                  AppLocalizations.of(context)!.priceWithTax,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              controller: controller.priceWithoutTaxController,
                              obscureText: false,
                              enabled: false,
                              keyboardType: TextInputType.text,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'Price Without Tax is required';
                                }
                                return null;
                              },
                              hintText:
                                  AppLocalizations.of(context)!.priceWithoutTax,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              controller: controller.discountController,
                              obscureText: false,
                              enabled: enableDiscount,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly,
                                MaxValueFormatter(controller.maxDiscount),
                                // Set your max value here
                              ],
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'discount is required';
                                }
                                return null;
                              },
                              hintText: AppLocalizations.of(context)!.discount,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              controller: controller.priceController,
                              obscureText: false,
                              keyboardType: TextInputType.text,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'price is required';
                                }
                                return null;
                              },
                              hintText: AppLocalizations.of(context)!.price,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              controller: controller.availableQtyController,
                              obscureText: false,
                              enabled: false,
                              keyboardType: TextInputType.text,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'quantity is required';
                                }
                                return null;
                              },
                              hintText: AppLocalizations.of(context)!
                                  .availableQuantity,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              controller: controller.orderQtyController,
                              obscureText: false,
                              enabled: false,
                              keyboardType: TextInputType.text,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'quantity is required';
                                }
                                return null;
                              },
                              hintText:
                                  AppLocalizations.of(context)!.orderQuantity,
                            ),
                            const SizedBox(height: 20),
                            if (controller.getRevItem(widget.branchItem.id) !=
                                null)
                              TextUtils(
                                text: AppLocalizations.of(context)!
                                    .itemsInTheCart,
                              ),
                            if (controller.getRevItem(widget.branchItem.id) !=
                                null)
                            Column(
                              children: List<Widget>.from(
                                controller.revisionsItems.map((revItem) {
                                  print(revItem.id);
                                  print(widget.branchItem.id);
                                  if(revItem.id == widget.branchItem.id) {
                                    return RevItems(
                                        color: revItem.color?.name ?? '',
                                        size: revItem.size?.name ?? '',
                                        orderedQty: revItem?.orderedCount ??
                                            0);
                                  }else {
                                    return Container();
                                  }
                                } ), // Replace Text() with your desired widget
                              ),
                            ),
                            const SizedBox(height: 20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                PrimaryButton(
                                  text: AppLocalizations.of(context)!.save,
                                  onPressed: () {},
                                  disabled: false,
                                ),
                                SecondaryButton(
                                    text: AppLocalizations.of(context)!.cancel,
                                    onPressed: () {
                                      Get.back();
                                    })
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ));
  }

  Widget RevItems(
      {required String color, required String size, required int orderedQty}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextUtils(text: "${AppLocalizations.of(context)!.color} : $color"),
        TextUtils(text: "${AppLocalizations.of(context)!.size} : $size"),
        TextUtils(
            text:
                "${AppLocalizations.of(context)!.orderQuantity} : $orderedQty"),
      ],
    );
  }
}
