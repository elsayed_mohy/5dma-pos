import 'package:flutter/material.dart';

class SideBackgroundPaint extends StatelessWidget {
  SideBackgroundPaint({this.height});
  final double? height;
  @override
  Widget build(BuildContext context) {
    return ClipPath(
        clipper: SideCurveClipper(),
    child: Container(
    width: double.infinity,
    height: height ?? 105,
    color: Color(0xff5457CD),
    ));
  }
}

class SideCurveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..lineTo(0, size.height * 0.5)
      ..quadraticBezierTo(
          size.width * 0.25, size.height, size.width * 0.5, size.height * 0.75)
      ..quadraticBezierTo(
          size.width * 0.75, size.height * 0.5, size.width, size.height * 0.75)
      ..lineTo(size.width, 0)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}