import 'package:flutter/material.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../utils/constants/colors.dart';

class AppDropdownInput<T> extends StatelessWidget {
  final String hintText;
  final List<T> options;
  final T value;
  final bool enabled;
  final String Function(T) getLabel;
  final  Function(T) onChanged;

  const AppDropdownInput({super.key,
    this.hintText = 'Please select an Option',
    this.options = const [],
    required this.getLabel,
    required this.value,
    required this.onChanged, this.enabled =true,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextUtils(text: hintText,align: TextAlign.start,fontWeight: FontWeight.w500,fontSize: 12,),
        SizedBox(height: 2,),
        Material(
          shadowColor: ColorsManager.main.withOpacity(0.18),
          elevation: 8,
          borderRadius: BorderRadius.circular(10),
          child: FormField<T>(
            key: UniqueKey(),
            enabled: enabled ?? true,
            builder: (FormFieldState<T> state) {
              return InputDecorator(
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(vertical: 15,horizontal: 10),
                  enabledBorder: OutlineInputBorder(
                    borderSide:  BorderSide(
                      color: ColorsManager.disabled.withOpacity(0.1),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  fillColor:ColorsManager.disabled.withOpacity(0.07),
                  filled: true,
                  suffixIconColor: ColorsManager.main,
                  hintText: hintText,
                  hintStyle: const TextStyle(
                      color: ColorsManager.disabled,
                      fontSize: 14
                  ),
                  labelStyle:const TextStyle(
                      color: ColorsManager.darkGrey,
                      fontSize: 10
                  ) ,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: ColorsManager.main,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: ColorsManager.warning,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: ColorsManager.warning,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                isEmpty: value == null || value == '',
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<T>(
                    isExpanded: true,
                    key: UniqueKey(),
                    value: value ?? null,
                    isDense: true,
                    onChanged:(value){
                      if(enabled == true ) {
                      onChanged(value as T);
                      }
                    },
                    items: options.map((T value) {
                      return DropdownMenuItem<T>(
                        key: UniqueKey(),
                        value: value,
                        child: TextUtils(text: getLabel(value)),
                      );
                    }).toList(),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}