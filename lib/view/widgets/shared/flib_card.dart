import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:math';

import '../../../logic/controllers/pos_controller.dart';

class FlipCard extends StatefulWidget {
  const FlipCard({super.key, required this.frontComponent, required this.backComponent});
  final Widget frontComponent;
  final Widget backComponent;
  @override
  _FlipCardState createState() => _FlipCardState();

}

class _FlipCardState extends State<FlipCard> with SingleTickerProviderStateMixin {


  late Animation<double> _frontAnimation;
  late Animation<double> _backAnimation;
  late Animation<double> _frontScale;
  late Animation<double> _backScale;
  final posController = Get.put(POSController());

  @override
  void initState() {
    posController.animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1200));
    _frontScale =  Tween(
      begin: 1.0,
      end: 0.0,
    ).animate( CurvedAnimation(
      parent: posController.animationController,
      curve:  Interval(0.0, 0.5, curve: Curves.easeIn),
    ));
    _backScale =  CurvedAnimation(
      parent: posController.animationController,
      curve:  Interval(0.5, 1.0, curve: Curves.easeOut),
    );
    super.initState();
  }

  @override
  void dispose() {
    posController.animationController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        AnimatedBuilder(
          animation: _backScale,
          builder: (BuildContext context,_) {
            final Matrix4 transform =  Matrix4.identity()
              ..scale(1.0, _backScale.value, 1.0);
            return  Transform(
              transform: transform,
              alignment: FractionalOffset.center,
              child: _buildBackSide(child: widget.backComponent),
            );
          },
        ),
        AnimatedBuilder(
          animation: _frontScale,
          builder: (BuildContext context,_) {
            final Matrix4 transform =  Matrix4.identity()
              ..scale(1.0, _frontScale.value, 1.0);
            return  Transform(
              transform: transform,
              alignment: FractionalOffset.center,
              child: _buildFrontSide(child: widget.frontComponent),
            );
          },
        ),
      ],
    );
  }

  Widget _buildFrontSide({required Widget child}) {
    return Container(
      height: 45.0,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xFF8752a3).withOpacity(0.8),
            Color(0xFF6274e7).withOpacity(0.8),
            Color(0xFF08203e).withOpacity(0.8),
          ],
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0, // soften the shadow
            spreadRadius: 0.5, //extend the shadow
          )
        ],
      ),
      child: child,
    );
  }

  Widget _buildBackSide({required Widget child}) {
    return Container(
      height: 45.0,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xFF15616d).withOpacity(0.8),
            Color(0xFF1b263b).withOpacity(0.8),
          ],
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0, // soften the shadow
            spreadRadius: 0.5, //extend the shadow
          )
        ],
      ),
      child: child,
    );
  }
}