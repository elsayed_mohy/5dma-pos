
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/constants/colors.dart';
import 'text_utils.dart';

class AccentButton extends StatelessWidget {
  final String text;
  final Function() onPressed;

  const AccentButton({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: ShapeDecoration(
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: BorderSide(color: ColorsManager.main)
        ),
        // shadows: [
        //   BoxShadow(
        //     color:ColorsManager.main ,
        //     blurRadius: 10,
        //     offset: Offset(0, 10),
        //     spreadRadius: 0,
        //   )
        // ],
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor:Colors.white,
          minimumSize: const Size(120, 55),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
              side: BorderSide(color: ColorsManager.main)
          ),
        ),
        onPressed:onPressed,
        child: TextUtils(
          text: text,
          fontSize: 12,
          fontWeight: FontWeight.w500,
          color: ColorsManager.main,
          align: TextAlign.center,
        ),
      ),
    );
  }
}
