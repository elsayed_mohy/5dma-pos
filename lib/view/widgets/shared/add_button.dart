import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';

class AddButtons extends StatelessWidget {
  const AddButtons({super.key, required this.IncreaseAction, required this.decreaseAction, required this.value});
  final Function IncreaseAction;
  final Function decreaseAction;
  final dynamic value;

  @override
  Widget build(BuildContext context) {
   return Row(
     mainAxisAlignment: MainAxisAlignment.center,
     children: [
       InkWell(
         onTap: (){
           IncreaseAction();
         },
         child: Container(
            width: 25,
            height: 25,
            decoration: ShapeDecoration(
              color: ColorsManager.chryslerBlue,
              shape: RoundedRectangleBorder(

                borderRadius: BorderRadius.circular(8),
              ),
            ),
            child: Icon(CupertinoIcons.plus,
             size: 15,
             color:Colors.white),
          ),
       ),
       SizedBox(width: 10,),
       TextUtils(text: value.toString()),
       SizedBox(width:10,),
       InkWell(
         onTap: (){
           decreaseAction();
         },
         child: Container(
           width: 25,
           height: 25,
           decoration: ShapeDecoration(
             color: ColorsManager.chryslerBlue,
             shape: RoundedRectangleBorder(

               borderRadius: BorderRadius.circular(8),
             ),
           ),
           child: Icon(CupertinoIcons.minus,
               size: 15,
               color:Colors.white),
         ),
       ),
     ],
   );
  }
}
