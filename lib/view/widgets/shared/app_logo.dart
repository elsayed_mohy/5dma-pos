import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../main.dart';
import '../../../utils/theme.dart';
import '../shared/background_paint.dart';

class AppLogo extends StatelessWidget {
  const AppLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return               Stack(
      children: [
        BackgroundPaint(height: 110,),
        SizedBox(
          width: double.infinity,
          child: Image.asset(
            "assets/images/ui/logo.png",
            height: 55,
          ),
        ),
      ],
    );
  }
}
