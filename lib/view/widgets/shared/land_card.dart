import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LandCard extends StatelessWidget {
  const LandCard({super.key, required this.action, required this.label, required this.iconPath});
  final Function action;
  final String label;
  final String iconPath;
  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      margin: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.width * 0.04,
          horizontal: MediaQuery.of(context).size.width * 0.04),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        onTap: () {
          action();
        },
        child: Ink(
            height: 30,
            width: 30,
            padding: const EdgeInsets.symmetric(vertical: 12,horizontal: 8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 5,
                    offset: const Offset(0, 0))
              ],
            ),
            child:
            Column(mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
              SvgPicture.asset(iconPath,height: 60,),
              const SizedBox(height: 15),
              Text(label,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontSize: 15, fontWeight: FontWeight.w500))
            ])),
      ),
    );
  }
}
