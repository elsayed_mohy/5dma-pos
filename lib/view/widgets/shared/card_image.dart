import 'package:flutter/material.dart';

import '../../../utils/constants/colors.dart';

class CardImage extends StatelessWidget {
  final String imagePath;
  final double? width;
  final double? height;
  const CardImage({super.key, required this.imagePath, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15.0),
        child: Image.asset(imagePath,width:width?? 60,height: height ?? 70,));

  }
}
