import 'package:flutter/material.dart';

class CardContainer extends StatelessWidget {
  const CardContainer({super.key, required this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        // padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius:  BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.07),
              blurRadius: 10,
              offset: Offset(5, 0),
              spreadRadius: 0,
            )
            // box-shadow: rgba(0, 0, 0, 0.05) 0px 1px 2px 0px;
          ],
        ),
        child: Card(child:  child));
  }
}
