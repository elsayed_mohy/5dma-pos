import 'package:flutter/material.dart';

import '../../../utils/constants/colors.dart';

class ImageViewer extends StatelessWidget {
  const ImageViewer({super.key, required this.imageUrl});
  final String imageUrl;
  @override
  Widget build(BuildContext context) {
    return Dismissible(
      direction: DismissDirection.vertical,
      key: const Key('key'),
      onDismissed: (_) => Navigator.of(context).pop(),
      child: Scaffold(
        body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [

                Center(
                  child: Hero(
                    tag: 'imageHero+$imageUrl',
                    child: Image.network(
                      imageUrl,
                      errorBuilder: (BuildContext context, Object exception,
                          StackTrace? stackTrace) {
                        return Image.asset(
                          'assets/images/ui/placeholder.png',
                          fit: BoxFit.cover,
                        );
                      },
                    ),
                  ),
                ),
                Align(
                  alignment: AlignmentDirectional.topStart,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: 50,
                      width: 55,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      decoration: ShapeDecoration(
                          color: ColorsManager.darkGrey.withAlpha(220),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        )),
                      child: BackButton(
                        color: Colors.white,onPressed: (){
                        Navigator.pop(context);
                      }, ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}