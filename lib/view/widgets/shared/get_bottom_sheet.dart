import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';

Future getBottomSheet(double height,Widget child) {
 return Get.bottomSheet(
     isScrollControlled:true,
     ignoreSafeArea: false,
     isDismissible:true,
      persistent:false,
      Container(
        height: height,
        child: child
      ),
      // barrierColor: Colors.red[50],
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20)
          )

      ),
      enableDrag: false,
      backgroundColor:ColorsManager.white
  );
}
