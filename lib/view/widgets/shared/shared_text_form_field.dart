import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';

class SharedTextFormField extends StatefulWidget {
  final TextEditingController controller;
  final bool obscureText;
  final Function validator;
  final IconData? prefixIcon;
  final Widget? suffixIcon;
  final bool? readOnly;
  final bool? enabled;
  final Function(String)? onChanged;
  final TextInputType? keyboardType;
  final String hintText;
  final List<TextInputFormatter>? inputFormatters;

  const SharedTextFormField({
    Key? key,
    required this.controller,
    required this.obscureText,
    required this.validator,
     this.prefixIcon,
     this.inputFormatters,
     this.suffixIcon,
    required this.hintText, this.keyboardType, this.readOnly,this.enabled, this.onChanged,
  }) : super(key: key);

  @override
  State<SharedTextFormField> createState() => _SharedTextFormFieldState();
}

class _SharedTextFormFieldState extends State<SharedTextFormField> {
  FocusNode _focusNode = FocusNode();
  bool _isFocused = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusNode.removeListener(_onFocusChange);
    _focusNode.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      _isFocused = _focusNode.hasFocus;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextUtils(text: widget.hintText,align: TextAlign.start,fontWeight: FontWeight.w500,fontSize: 12,),
        SizedBox(height: 2,),
        Material(
          shadowColor: ColorsManager.main.withOpacity(0.18),
          elevation: 8,
          borderRadius: BorderRadius.circular(10),
          child: TextFormField(
            onChanged: widget.onChanged,
            focusNode: _focusNode,
            style: TextStyle(fontSize: 14),
            controller: widget.controller,
            obscureText: widget.obscureText,
            enabled: widget.enabled ?? true,
            inputFormatters:widget.inputFormatters ?? [],
            cursorColor:ThemesManager().isSavedDarkMode() ? ColorsManager.main : ColorsManager.darkGrey,
            readOnly: widget.readOnly ?? false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 16,horizontal: 10),
              prefixIcon:widget.prefixIcon != null ? Icon(
                widget.prefixIcon,
                color: ColorsManager.main,
              size: 20,) : null,
              enabledBorder: OutlineInputBorder(
                borderSide:  BorderSide(
                  color: ColorsManager.disabled.withOpacity(0.1),
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              fillColor:_isFocused ? ColorsManager.main.withOpacity(0.07) : ColorsManager.disabled.withOpacity(0.08),
              filled: true,
              suffixIcon: widget.suffixIcon,
              suffixIconColor: ColorsManager.main,
              hintText: widget.hintText,
              hintStyle: const TextStyle(
                color: ColorsManager.disabled,
                fontSize: 12
              ),
              labelStyle:const TextStyle(
                  color: ColorsManager.darkGrey,
                  fontSize: 10
              ) ,
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: ColorsManager.main,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: ColorsManager.warning,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  color: ColorsManager.warning,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            keyboardType: widget.keyboardType ?? TextInputType.text,
            validator: (value) => widget.validator(value),
          ),
        ),
      ],
    );
  }
}
