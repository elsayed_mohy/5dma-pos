
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/constants/colors.dart';
import 'text_utils.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final bool disabled;
  final Function() onPressed;

  const PrimaryButton({
    Key? key,
    required this.text,
    required this.onPressed,required this.disabled,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: ShapeDecoration(
        color: disabled ?  ColorsManager.disabled : ColorsManager.main,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        shadows: [
          BoxShadow(
            color:disabled ?  Color.fromRGBO(0, 0, 0, 0.08) : Color(0x335D5ED9) ,
            blurRadius: 30,
            offset: Offset(0, 16),
            spreadRadius: 0,
          )
        ],
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor:disabled ?  ColorsManager.disabled : ColorsManager.main,
          minimumSize: const Size(120, 55),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
        onPressed:disabled? null : onPressed,
        child: TextUtils(
          text: text,
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: Colors.white,
        ),
      ),
    );
  }
}
