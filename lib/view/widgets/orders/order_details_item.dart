import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/order/order_details_model.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/utils/constants/colors.dart';

import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/app_urls.dart';
import '../product_details_bottom_sheet.dart';
import '../shared/add_button.dart';
import '../shared/card_container.dart';
import '../shared/get_bottom_sheet.dart';
import '../shared/text_utils.dart';

class OrderDetailsItem extends StatelessWidget {
  OrderDetailsItem({super.key, required this.item});

  final IssuingOrderDetail item;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
        builder: (ViewOrderController controller) {
      return CardContainer(
        child: Container(
          constraints: BoxConstraints(maxWidth: 200),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  children: [
                    CachedNetworkImage(
                      imageUrl: item.branchItem.item.imagePath != null
                          ? AppUrls.baseImageUrl +
                              item.branchItem.item.imagePath
                          : '',
                      fit: BoxFit.cover,
                      width: 50,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) => Container(
                        height: 20,
                        child: SizedBox(
                            height: 20,
                            width: 20,
                            child: Center(
                                child: CircularProgressIndicator(
                                    value: downloadProgress.progress))),
                      ),
                      errorWidget: (context, url, error) => Image.asset(
                        'assets/images/ui/placeholder.png',
                        height: 50,
                        width: 50,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextUtils(
                            text: item.branchItem.item.name,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          TextUtils(
                            text: item.branchItem.barCode,
                            fontSize: 10,
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Row(
                            children: [
                              TextUtils(
                                text: item.branchItem.salesPrice.toString(),
                                fontWeight: FontWeight.w500,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                      color: ColorsManager.disabled
                                          .withOpacity(0.3),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: TextUtils(
                                    text: '- ${item.branchItem.discount}',
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500,
                                    color: ColorsManager.warning,
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),
                    if (controller.orderDetails?.order.orderStatus.code ==
                            'NEW' ||
                        controller.orderDetails?.order.orderStatus.code ==
                            'PROCESS' ||
                        controller.orderDetails?.order.orderStatus.code ==
                            'PREPARED' ||
                        controller.orderDetails?.order.orderStatus.code ==
                            'DRIVER')
                      Row(
                        children: [
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              InkWell(
                                  onTap: () {
                                    controller.removeCreationItem(item);
                                  },
                                  child: Icon(CupertinoIcons.trash_fill)),
                              // SizedBox(
                              //   height: 10,
                              // ),
                              // InkWell(onTap: (){
                              //   getBottomSheet(
                              //     MediaQuery.of(context).size.height,
                              //     ProductDetailsBottomSheet(
                              //       branchItem: item.branchItem,
                              //     ),
                              //   );
                              // }, child: Icon(CupertinoIcons.info)),
                            ],
                          )
                        ],
                      )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        TextUtils(text: AppLocalizations.of(context)!.size),
                        SizedBox(
                          width: 10,
                        ),
                        TextUtils(text: item.sizeName),
                      ],
                    ),
                    Row(
                      children: [
                        TextUtils(text: AppLocalizations.of(context)!.color),
                        SizedBox(
                          width: 10,
                        ),
                        TextUtils(text: item.colorName),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextUtils(
                        text: AppLocalizations.of(context)!.priceWithoutTax),
                    TextUtils(text: item.branchItem.salesPrice.toString()),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextUtils(text: AppLocalizations.of(context)!.priceWithTax),
                    TextUtils(
                        text: item.branchItem.salesPriceWithTax.toString()),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextUtils(text: AppLocalizations.of(context)!.tax),
                    TextUtils(text: item.branchItem.salesTaxValue.toString()),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextUtils(text: AppLocalizations.of(context)!.total),
                    TextUtils(
                        text: (item.branchItem.salesPriceWithTax -
                                item.branchItem.discount)
                            .toString()),
                  ],
                ),
                if (controller.orderDetails?.order.orderStatus.code == 'NEW' ||
                    controller.orderDetails?.order.orderStatus.code ==
                        'PROCESS' ||
                    controller.orderDetails?.order.orderStatus.code ==
                        'PREPARED' ||
                    controller.orderDetails?.order.orderStatus.code == 'DRIVER')
                  Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      AddButtons(
                        IncreaseAction: () {
                          controller.incrementItem(item);
                        },
                        decreaseAction: () {
                          controller.decrementItem(item);
                        },
                        value: item.qty ?? 0.0,
                      ),
                    ],
                  )
              ],
            ),
          ),
        ),
      );
    });
  }
}
