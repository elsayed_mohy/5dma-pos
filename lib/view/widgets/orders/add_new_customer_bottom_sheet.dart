import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/data/model/common/config.dart';
import 'package:khdma_pos/data/model/order/item_size_model.dart';
import 'package:khdma_pos/logic/controllers/item_controller.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/services/base_service.dart';
import 'package:khdma_pos/services/pos/orders_service.dart';
import 'package:khdma_pos/view/widgets/shared/app_dropdown_input.dart';
import 'package:khdma_pos/view/widgets/shared/primary_button.dart';
import 'package:khdma_pos/view/widgets/shared/secondary_button.dart';
import 'package:khdma_pos/view/widgets/shared/shared_text_form_field.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../data/model/common/walking_consumer_model.dart';
import '../../../l10n/app_localizations.dart';

class AddNewCustomerBottomSheet extends StatefulWidget {
  AddNewCustomerBottomSheet({super.key});

  @override
  State<AddNewCustomerBottomSheet> createState() =>
      _AddNewCustomerBottomSheetState();
}

class _AddNewCustomerBottomSheetState extends State<AddNewCustomerBottomSheet> {
  final controller = Get.put(ViewOrderController());
  final storage = GetStorage();
  final orderService = OrdersService();
  final baseService = BaseService();
  Map consumerObject = {};
  bool loading = true;
  bool isFound = false;
  List<GenericType> addressTypes = [];
  List<GenericType> consumerTypes = [];
  GenericType? selectedAddressType;
  GenericType? selectedConsumerType;

  bool enableDiscount = true;
  final newCustomerFormKey = GlobalKey<FormState>();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController taxNumberController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController countryController = TextEditingController();
  final TextEditingController governorateController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController streetController = TextEditingController();
  final TextEditingController street2Controller = TextEditingController();
  final TextEditingController postalCodeController = TextEditingController();
  final TextEditingController buildingController = TextEditingController();
  final TextEditingController floorController = TextEditingController();
  final TextEditingController flatNumberController = TextEditingController();
  final TextEditingController landMarkController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    initialize();
    super.initState();
  }

  initialize() async {
    loading = true;
    var res = await storage.read("config");
    if (res != null) {
      setState(() {
        var config = Config.fromJson(res);
        addressTypes = config.addressTypes;
        consumerTypes = config.consumerType;
      });
      loading = false;
    }
  }

  getCustomerByPhoneNumber(String phoneNumber) async {
    if (phoneNumber.length > 8) {
      var res = await orderService.getConsumerByPhoneNumber(phoneNumber);
      if (res != null) {
        setState(() {
          consumerObject.addAll({
            "id": res.first.id,
            "fullName": res.first.fullName,
            "email": res.first.email,
          });
          nameController.text = consumerObject!['fullName'];
          emailController.text = consumerObject!['email'];
          isFound = true;
        });
      } else {
        baseService.showError("consumer not found");
        isFound = false;
        return;
      }
    } else {
      return;
    }
  }

  save() async {
    consumerObject.addAll({
      "fullName":nameController.text,
      "phoneNumber":phoneController.text,
      "taxNumber":taxNumberController.text,
      "email":emailController.text,
      "country":countryController.text,
      "governorate":governorateController.text,
      "city":cityController.text,
      "street":streetController.text,
      "street2":street2Controller.text,
      "postalCode":postalCodeController.text,
      "building":buildingController.text,
      "floor":floorController.text,
      "flatNumber":flatNumberController.text,
      "landMark":landMarkController.text,
    });
    print(consumerObject);
    if (consumerObject != null && isFound == true) {
      Navigator.pop(context, consumerObject!);
    } else {
      var res = await orderService
          .createWalkingConsumer(consumerObject);
      if (res != null) {
        Navigator.pop(context, res);
        baseService.showSuccess("add success");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            title: Text(
          AppLocalizations.of(context)!.customer,
        )),
        body: loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    GetBuilder<ViewOrderController>(
                        init: ViewOrderController(),
                        builder: (ViewOrderController controller) {
                          return Padding(
                            padding: const EdgeInsets.fromLTRB(25, 10, 25, 25),
                            child: Form(
                              key: newCustomerFormKey,
                              child: Column(
                                children: [
                                  SharedTextFormField(
                                    controller: phoneController,
                                    obscureText: false,
                                    keyboardType: TextInputType.number,
                                    onChanged: (value) {
                                      getCustomerByPhoneNumber(value);
                                    },
                                    validator: (String? value) {
                                      if (value == null || value.isEmpty) {
                                        return 'phone is required';
                                      }
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.phone,
                                  ),
                                  const SizedBox(height: 15),
                                  if (consumerTypes.isNotEmpty)
                                    Column(
                                      children: [
                                        AppDropdownInput(
                                          hintText:
                                              AppLocalizations.of(context)!
                                                  .size,
                                          options: consumerTypes!,
                                          value: consumerTypes[0],
                                          onChanged: (GenericType value) {
                                            setState(() {
                                              selectedConsumerType = value;
                                              consumerObject.addAll({
                                                "consumerType":{
                                                  "id":value.id
                                                }
                                              });
                                            });
                                          },
                                          getLabel: (value) => value.name,
                                        ),
                                        const SizedBox(height: 15),
                                      ],
                                    ),
                                  SharedTextFormField(
                                    controller: nameController,
                                    obscureText: false,
                                    enabled: isFound == false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      if (value == null || value.isEmpty) {
                                        return 'name is required';
                                      }
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.name,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: taxNumberController,
                                    obscureText: false,
                                    keyboardType: TextInputType.number,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.taxNumber,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: emailController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.email,
                                  ),
                                  const SizedBox(height: 15),
                                  if (addressTypes.isNotEmpty)
                                    Column(
                                      children: [
                                        AppDropdownInput(
                                          hintText:
                                              AppLocalizations.of(context)!
                                                  .size,
                                          options: addressTypes!,
                                          value: addressTypes[0],
                                          onChanged: (GenericType value) {
                                            setState(() {
                                              selectedAddressType = value;
                                              consumerObject.addAll({
                                                "addressType":{
                                                  "id":value.id
                                                }
                                              });
                                            });
                                          },
                                          getLabel: (value) => value.name,
                                        ),
                                        const SizedBox(height: 15),
                                      ],
                                    ),
                                  SharedTextFormField(
                                    controller: countryController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.country,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: governorateController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText: AppLocalizations.of(context)!
                                        .governorate,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: cityController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.city,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: streetController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.street,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: street2Controller,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.street2,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: postalCodeController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText: AppLocalizations.of(context)!
                                        .postalCode,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: buildingController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.building,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: floorController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.floor,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: flatNumberController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText: AppLocalizations.of(context)!
                                        .flatNumber,
                                  ),
                                  const SizedBox(height: 15),
                                  SharedTextFormField(
                                    controller: landMarkController,
                                    obscureText: false,
                                    keyboardType: TextInputType.text,
                                    validator: (String? value) {
                                      return null;
                                    },
                                    hintText:
                                        AppLocalizations.of(context)!.landMark,
                                  ),
                                  const SizedBox(height: 20),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      PrimaryButton(
                                        text:
                                            AppLocalizations.of(context)!.save,
                                        onPressed: () {
                                          save();
                                        },
                                        disabled: false,
                                      ),
                                      SecondaryButton(
                                          text: AppLocalizations.of(context)!
                                              .cancel,
                                          onPressed: () {
                                            Get.back();
                                          })
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ],
                ),
              ));
  }
}
