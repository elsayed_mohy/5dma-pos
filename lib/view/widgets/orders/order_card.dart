import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/order/order_model.dart';
import 'package:khdma_pos/routes/routes.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/card_container.dart';
import '../shared/text_utils.dart';

class OrderCard extends StatelessWidget {
  const OrderCard({super.key, required this.order});

  final OrderModel order;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
       var data = {
          "id" : order.id.toString(),
        };
        Get.toNamed(Routes.viewOrder,parameters: data);
      },
      child: CardContainer(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: orderCardInfoRow(
                        propertyName: AppLocalizations.of(context)!.orderNumber,
                        value: order.issuingNumber),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 2,horizontal: 5),
                    decoration: BoxDecoration(
                        borderRadius:  BorderRadius.circular(8),

                        color: Color(int.parse(order.orderStatus.color.replaceFirst("#", "0xFF")))
                    ),
                    child: TextUtils(text:order.orderStatus.name ,color: Colors.white,),
                  )
                ],
              ),
              Divider(),
              orderCardInfoRow(
                  propertyName: AppLocalizations.of(context)!.branchName,
                  value: order.branchName),
              orderCardInfoRow(
                  propertyName: AppLocalizations.of(context)!.customerName,
                  value: order.walkConsumerName ?? ''),
              orderCardInfoRow(
                  propertyName: AppLocalizations.of(context)!.paymentMethod,
                  value: order.paymentMethod.name),
              orderCardInfoRow(
                  propertyName: AppLocalizations.of(context)!.discount,
                  value: order.totalDiscount),
              Divider(),
              orderCardInfoRow(
                  propertyName: AppLocalizations.of(context)!.total,
                  value: (order.totalAmount + order.totalTax)),
            ],
          ),
        ),
      ),
    );
  }
}

Widget orderCardInfoRow({required String propertyName, required value}) {
  return Container(
    margin: EdgeInsets.only(bottom: 3),
    child: Row(
      children: [
        TextUtils(
            text: propertyName, fontSize: 12, fontWeight: FontWeight.w500),
        SizedBox(
          width: 5,
        ),
        TextUtils(text: ":", fontSize: 12, fontWeight: FontWeight.w500),
        SizedBox(
          width: 5,
        ),
        Expanded(
            child: TextUtils(
          text: value.toString(),
          fontSize: 12,
        )),
      ],
    ),
  );
}
