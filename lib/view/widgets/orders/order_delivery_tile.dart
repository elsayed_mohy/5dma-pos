import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/common/delivery_method_model.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/app_dropdown_input.dart';
import '../shared/shared_text_form_field.dart';
import '../shared/text_utils.dart';

class OrderDeliveryTile extends StatelessWidget {
  const OrderDeliveryTile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
        init: ViewOrderController(),
        builder: (ViewOrderController controller) {
          return controller.deliveryMethods.isNotEmpty
              ? CardContainer(
                  child: ExpansionTile(
                    title: TextUtils(
                      text: AppLocalizations.of(context)!.deliveryMethod,
                      fontWeight: FontWeight.w600,
                    ),
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            AppDropdownInput(
                              enabled: (
                                  controller.orderDetails?.order.orderStatus.code == 'NEW' ||
                                  controller.orderDetails?.order.orderStatus.code == 'PROCESS'),
                              hintText:
                                  AppLocalizations.of(context)!.deliveryMethod,
                              options: controller.deliveryMethods,
                              value: controller.selectedDeliveryMethod ??
                                  controller.deliveryMethods[0],
                              onChanged: (DeliveryMethod value) {
                                controller.onDeliveryMethodChanged(value.id);
                              },
                              getLabel: (value) => value.name,
                            ),
                            const SizedBox(height: 15),
                            SharedTextFormField(
                              enabled: (
                                  controller.orderDetails?.order.orderStatus.code == 'NEW' ||
                                  controller.orderDetails?.order.orderStatus.code == 'PROCESS'),
                              controller: controller.descriptionController,
                              obscureText: false,
                              keyboardType: TextInputType.number,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return 'description is required';
                                }
                                return null;
                              },
                              hintText:
                                  AppLocalizations.of(context)!.description,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              : SizedBox.shrink();
        });
  }
}
