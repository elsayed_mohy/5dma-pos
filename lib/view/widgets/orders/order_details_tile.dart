import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/text_utils.dart';

class OrderDetailsTile extends StatelessWidget {
  const OrderDetailsTile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
        init: ViewOrderController(),
        builder: (ViewOrderController controller) {
          return CardContainer(
            child: ExpansionTile(
              title: TextUtils(
                text: AppLocalizations.of(context)!.orderDetails,
                fontWeight: FontWeight.w600,
              ),
              children: <Widget>[
                Column(
                  children: [
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.orderType,
                        value: controller.orderDetails?.order.orderStatus),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.orderNumber,
                    value: controller.orderDetails?.order.issuingNumber),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.taxNumber,
                    value: controller.orderDetails?.order.totalTax),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.deliveryMethod,
                    value: controller.orderDetails?.order.deliveryMethod),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.paymentMethod,
                    value: controller.orderDetails?.order.paymentMethod.name),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.orderDate,
                    value: controller.orderDetails?.order.issuingDate),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.paidAmount,
                    value: controller.orderDetails?.order.paidAmount),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.taxAmount,
                    value: controller.orderDetails?.order.totalAmount),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.discount,
                    value: controller.orderDetails?.order.totalDiscount),
                    buildDetailsListTile(
                        title: AppLocalizations.of(context)!.deliveryEmployee,
                    value: controller.orderDetails?.order.deliveryEmployeeId),
                  ],
                )
              ],
            ),
          );
        });
  }
}

Widget buildDetailsListTile({required String title, dynamic value}) {
  return ListTile(
    dense: true,
    visualDensity: VisualDensity(vertical: -4),
    title: TextUtils(
      text: title,
      fontSize: 12,
      fontWeight: FontWeight.w500,
    ),
    trailing: TextUtils(
      text: value.toString(),
      fontSize: 12,
      fontWeight: FontWeight.w500,
    ),
  );
}
