import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/view/widgets/orders/order_details_item.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';
import 'package:khdma_pos/view/widgets/shared/prototype_height.dart';

import '../../../data/model/order/order_details_model.dart';
import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/app_urls.dart';
import '../shared/text_utils.dart';
import 'order_creation_details_item.dart';
class OrderItemsTile extends StatelessWidget {
  const OrderItemsTile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
      init: ViewOrderController(),
      builder: (ViewOrderController controller) {
        if(controller.orderCreation.isFalse) {
          return controller.orderDetails?.order !=null ?
          CardContainer(
            child:  ExpansionTile(
              title: TextUtils(
                text: AppLocalizations.of(context)!.orderItems,
                fontWeight: FontWeight.w600,
              ),
              children: <Widget>[
                SizedBox(
                  height: 265,
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context,
                          int index) =>
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: OrderDetailsItem(item:controller.orderDetails!.order.issuingOrderDetails[index]),
                          ),
                      itemCount: controller.orderDetails!.order.issuingOrderDetails.length),
                )
              ],
            ),
          ) :Container() ;
        }else {
          return CardContainer(
            child:  ExpansionTile(
              title: TextUtils(
                text: AppLocalizations.of(context)!.orderItems,
                fontWeight: FontWeight.w600,
              ),
              children: <Widget>[
                SizedBox(
                  height: 265,
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context,
                          int index) =>
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: OrderCreationDetailsItem(branchItem:controller.orderItems[index]),
                          ),
                      itemCount: controller.orderItems.length),
                )
              ],
            ),
          );
        }

      }
    );
  }
}

