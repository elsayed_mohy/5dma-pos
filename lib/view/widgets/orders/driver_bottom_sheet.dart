import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/common/branch_employee_model.dart';
import 'package:khdma_pos/view/widgets/shared/primary_button.dart';
import 'package:khdma_pos/view/widgets/shared/secondary_button.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/app_dropdown_input.dart';

class DriverBottomSheet extends StatefulWidget {
   DriverBottomSheet({super.key, required this.drivers});
  final List<BranchEmployee> drivers;

  @override
  State<DriverBottomSheet> createState() => _DriverBottomSheetState();
}

class _DriverBottomSheetState extends State<DriverBottomSheet> {
   BranchEmployee? selectedDriver;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.fromLTRB(16,16,16,10),
        child: Column(
          children: [
            TextUtils(text: AppLocalizations.of(context)!.deliveryEmployee,fontSize: 18,),
            SizedBox(height: 15),
            AppDropdownInput(
              hintText:
              AppLocalizations.of(context)!.unit,
              options: widget.drivers,
              value: widget.drivers.first,
              onChanged: (BranchEmployee value) {
                setState(() {
                  selectedDriver = value;
                });
              },
              getLabel: (value) => value.user.fullName,
            ),
            SizedBox(height: 20),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      width:200,
                      child: PrimaryButton(text: 'save', onPressed: (){
                        Navigator.pop(context, selectedDriver!);
                      }, disabled: selectedDriver == null)),
                  SecondaryButton(text: 'cancel', onPressed: (){
                    Get.back();
                  }),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
