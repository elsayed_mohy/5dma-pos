import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/utils/constants/colors.dart';
import 'package:khdma_pos/utils/constants/colors.dart';
import 'package:khdma_pos/utils/constants/colors.dart';
import 'package:khdma_pos/utils/constants/colors.dart';
import 'package:khdma_pos/view/widgets/orders/order_details_item.dart';
import 'package:khdma_pos/view/widgets/orders/order_details_returned_item.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';
import 'package:khdma_pos/view/widgets/shared/prototype_height.dart';

import '../../../data/model/order/order_details_model.dart';
import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/app_urls.dart';
import '../shared/text_utils.dart';
class OrderReturnedItemsTile extends StatelessWidget {
  const OrderReturnedItemsTile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
      init: ViewOrderController(),
      builder: (ViewOrderController controller) {
        return controller.orderDetails?.order !=null ?
         CardContainer(
          child:  ExpansionTile(
            title: TextUtils(
              text: AppLocalizations.of(context)!.returnOrder,
              fontWeight: FontWeight.w600,
            ),
            children: List<Widget>.from(
    controller.orderDetails!.order.returnOrders.map((returnOrder) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextUtils(text:  "${AppLocalizations.of(context)!.orderNumber} :",
                              fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.darkGrey,),
                TextUtils(text: returnOrder.returnNumber,
                fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.main,),
              ],
            ),
            SizedBox(height: 5,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextUtils(text: "${AppLocalizations.of(context)!.orderIssuingDate} :",
                              fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.darkGrey,),
                TextUtils(text: returnOrder.returnDate.toString(),
                fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.main,),
              ],
            ),
            SizedBox(height: 5,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextUtils(text: "${AppLocalizations.of(context)!.orderNumber} :",
                              fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.darkGrey,),
                TextUtils(text: returnOrder.returnReason.name ?? '',
                fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.main,),
              ],
            ),
            SizedBox(height: 5,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextUtils(text: "${AppLocalizations.of(context)!.description} :",
                              fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.darkGrey,),
                TextUtils(text: returnOrder.description,
                fontSize: 10,
                fontWeight: FontWeight.w500,
                color: ColorsManager.main,),
              ],
            ),
            SizedBox(height: 5,),
            SizedBox(
              height: 110,
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context,
                      int index) =>
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: OrderDetailsReturnedItem(item:returnOrder.returnOrderDetails[index]),
                      ),
                  itemCount: returnOrder.returnOrderDetails.length),
            ),
            Divider()
          ],
        ),
      );
    }))


          ),
        ) :Container() ;
      }
    );
  }
}

