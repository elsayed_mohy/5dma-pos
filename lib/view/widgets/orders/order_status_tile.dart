import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/utils/constants/colors.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/text_utils.dart';

class OrderStatusTile extends StatelessWidget {
  const OrderStatusTile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
        init: ViewOrderController(),
        builder: (ViewOrderController controller) {
          return CardContainer(
            child: ExpansionTile(
              title: TextUtils(
                text: AppLocalizations.of(context)!.status,
                fontWeight: FontWeight.w600,
              ),
              children: <Widget>[
                Theme(
                  data: ThemeData(
                      colorScheme: ColorScheme.light(
                          primary: ColorsManager.main
                      )
                  ),
                  child: Stepper(
                      type : StepperType.vertical,
                      controlsBuilder:
                          (BuildContext context, ControlsDetails details) {
                            return  Container(child: null,);
                      },
                      steps: List<Step>.from(
                          controller.orderDetails!.status.map((step) {
                           var currentIndex= controller.orderDetails!.status.indexWhere((element) =>  element.id == step.id);
                        return Step(
                          title: TextUtils(
                            text: step.name,
                            fontSize: 12,
                          ),
                          isActive :currentIndex == controller.selectedStatusIndex,
                          content: Container(child: null,),
                        );
                      }))),
                )
              ],
            ),
          );
        });
  }
}
