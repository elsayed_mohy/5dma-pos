import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/view/widgets/shared/accent_button.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/get_bottom_sheet.dart';
import '../shared/primary_button.dart';
import '../shared/shared_text_form_field.dart';
import '../shared/text_utils.dart';
import 'add_new_customer_bottom_sheet.dart';

class OrderCustomerTile extends StatelessWidget {
  OrderCustomerTile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
        init: ViewOrderController(),
        builder: (ViewOrderController controller) {
          return CardContainer(
            child: ExpansionTile(
              title: TextUtils(
                text: AppLocalizations.of(context)!.customer,
                fontWeight: FontWeight.w600,
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: controller.customerFormKey,
                    child: Column(
                      children: [
                        if (controller.orderDetails?.order.orderStatus.code ==
                                'NEW' ||
                            controller.orderDetails?.order.orderStatus.code ==
                                'PROCESS' ||
                            controller.orderCreation.isTrue)
                          Column(
                            children: [
                              SharedTextFormField(
                                controller:
                                    controller.customerPhoneSearchController,
                                obscureText: false,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'id is required';
                                  }
                                  return null;
                                },
                                hintText: AppLocalizations.of(context)!
                                    .customerSearch,
                              ),
                              const SizedBox(height: 20),
                              SizedBox(
                                height: 70,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      PrimaryButton(
                                          text: "Search",
                                          onPressed: () {
                                            controller.getCustomerByPhoneNumber(
                                                controller
                                                    .customerPhoneSearchController
                                                    .text);
                                          },
                                          disabled: false),
                                      AccentButton(
                                        text: "add new",
                                        onPressed: () async {
                                          var bottomSheetResult = await getBottomSheet(
                                              MediaQuery.of(context)
                                                  .size
                                                  .height,
                                              AddNewCustomerBottomSheet());
                                          if(bottomSheetResult !=null) {
                                            controller.addNewConsumer(bottomSheetResult);
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        SharedTextFormField(
                          controller: controller.customerIdController,
                          obscureText: false,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'id is required';
                            }
                            return null;
                          },
                          hintText: AppLocalizations.of(context)!.name,
                        ),
                        const SizedBox(height: 20),
                        SharedTextFormField(
                          controller: controller.customerFullNameController,
                          obscureText: false,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'full Name is required';
                            }
                            return null;
                          },
                          hintText: AppLocalizations.of(context)!.name,
                        ),
                        const SizedBox(height: 20),
                        SharedTextFormField(
                          controller: controller.customerPhoneNumberController,
                          obscureText: false,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'phone Number is required';
                            }
                            return null;
                          },
                          hintText: AppLocalizations.of(context)!.phone,
                        ),
                        const SizedBox(height: 20),
                        SharedTextFormField(
                          controller: controller.customerEmailController,
                          obscureText: false,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'email is required';
                            }
                            return null;
                          },
                          hintText: AppLocalizations.of(context)!.email,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
