import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:khdma_pos/data/model/order/order_details_model.dart';
import 'package:khdma_pos/data/model/order/return_order_model.dart';
import 'package:khdma_pos/utils/constants/colors.dart';

import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/app_urls.dart';
import '../shared/card_container.dart';
import '../shared/text_utils.dart';

class OrderDetailsReturnedItem extends StatelessWidget {
  OrderDetailsReturnedItem({super.key, required this.item});

  final ReturnOrderDetail item;

  @override
  Widget build(BuildContext context) {
    return CardContainer(
      child: Container(
        constraints: BoxConstraints(maxWidth: 180),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextUtils(
                      text: item.itemName,fontSize: 11,),
                  TextUtils(text: item.returnStatus.name ??'',fontSize: 11,),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextUtils(
                      text: "${AppLocalizations.of(context)!.unit} :",fontSize: 11,),
                  TextUtils(text: item.unitName,fontSize: 11,),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextUtils(text: "${AppLocalizations.of(context)!.returnCount } :",fontSize: 11,),
                  TextUtils(text: item.qty.toString(),fontSize: 11,),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
