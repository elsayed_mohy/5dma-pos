import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/view/widgets/shared/card_container.dart';

import '../../../l10n/app_localizations.dart';
import '../shared/text_utils.dart';

class OrderSummeryCard extends StatelessWidget {
  const OrderSummeryCard({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
      init: ViewOrderController(),
      builder: (ViewOrderController controller) {
        return CardContainer(child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              TextUtils(
                text: AppLocalizations.of(context)!.priceWithoutTax,
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
              TextUtils(
                  text: controller.totalPrice.toString(),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )
            ],),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              TextUtils(
                text: AppLocalizations.of(context)!.discount,
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
              TextUtils(
                  text: controller.totalDiscount.toString(),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )
            ],),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              TextUtils(
                text: AppLocalizations.of(context)!.total,
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
              TextUtils(
                  text: controller.totalPrice.toString(),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )
            ],),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              TextUtils(
                text: AppLocalizations.of(context)!.totalReturnedNet,
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
              TextUtils(
                  text: "00",
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )
            ],),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              TextUtils(
                text: AppLocalizations.of(context)!.tax,
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
              TextUtils(
                  text:controller.totalTax.toString(),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )
            ],),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              TextUtils(
                text: AppLocalizations.of(context)!.paidAmount,
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
              TextUtils(
                  text: (controller.totalPrice +controller.totalTax).toString(),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )
            ],),
            SizedBox(height: 5),
          ],),
        ));
      }
    );
  }
}
