import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/data/model/order/branch_item.dart';
import 'package:khdma_pos/data/model/order/order_details_model.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/utils/constants/colors.dart';

import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/app_urls.dart';
import '../product_details_bottom_sheet.dart';
import '../shared/add_button.dart';
import '../shared/card_container.dart';
import '../shared/get_bottom_sheet.dart';
import '../shared/text_utils.dart';

class OrderCreationDetailsItem extends StatelessWidget {
  OrderCreationDetailsItem({super.key, required this.branchItem});

  final BranchItem branchItem;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ViewOrderController>(
      builder: (ViewOrderController controller) {
        return CardContainer(
          child: Container(
            constraints: BoxConstraints(maxWidth: 200),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      CachedNetworkImage(
                        imageUrl: branchItem.item?.imagePath != null
                            ? AppUrls.baseImageUrl + branchItem.item?.imagePath
                            : '',
                        fit: BoxFit.cover,
                        width: 50,
                        progressIndicatorBuilder:
                            (context, url, downloadProgress) => Container(
                          height: 20,
                          child: SizedBox(
                              height: 20,
                              width: 20,
                              child: Center(
                                  child: CircularProgressIndicator(
                                      value: downloadProgress.progress))),
                        ),
                        errorWidget: (context, url, error) => Image.asset(
                          'assets/images/ui/placeholder.png',
                          height: 50,
                          width: 50,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextUtils(
                              text: branchItem.item?.name ?? '',
                              fontWeight: FontWeight.w500,
                              fontSize: 15,
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            TextUtils(
                              text: branchItem.barCode ?? '',
                              fontSize: 10,
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Row(
                              children: [
                                TextUtils(
                                  text: branchItem.salesPrice.toString(),
                                  fontWeight: FontWeight.w500,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                        color:
                                            ColorsManager.disabled.withOpacity(0.3),
                                        borderRadius: BorderRadius.circular(10)),
                                    child: TextUtils(
                                      text: '- ${branchItem.discount}',
                                      fontSize: 10,
                                      fontWeight: FontWeight.w500,
                                      color: ColorsManager.warning,
                                    )),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        InkWell(onTap: (){
                          controller.removeCreationItem(branchItem);
                        }, child: Icon(CupertinoIcons.trash_fill)),
                          SizedBox(
                            height: 10,
                          ),
                        InkWell(onTap: (){
                          getBottomSheet(
                            MediaQuery.of(context).size.height,
                            ProductDetailsBottomSheet(
                              branchItem: branchItem,
                            ),
                          );
                        }, child: Icon(CupertinoIcons.info)),
                      ],)
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          TextUtils(text: AppLocalizations.of(context)!.size),
                          SizedBox(
                            width: 10,
                          ),
                          TextUtils(text: branchItem.size?.name),
                        ],
                      ),
                      Row(
                        children: [
                          TextUtils(text: AppLocalizations.of(context)!.color),
                          SizedBox(
                            width: 10,
                          ),
                          TextUtils(text: branchItem.color?.name),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextUtils(
                          text: AppLocalizations.of(context)!.priceWithoutTax),
                      TextUtils(text: branchItem.salesPrice.toString()),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextUtils(text: AppLocalizations.of(context)!.priceWithTax),
                      TextUtils(text: (branchItem.salesPrice + branchItem.salesTaxValue).toString()),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextUtils(text: AppLocalizations.of(context)!.tax),
                      TextUtils(text: branchItem.salesTaxValue.toString()),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextUtils(text: AppLocalizations.of(context)!.total),
                      TextUtils(text: (branchItem.salesPrice + branchItem.salesTaxValue - branchItem.discount).toString()),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  AddButtons(
                    IncreaseAction: () {
                        controller.incrementCreationItem(branchItem);
                    },
                    decreaseAction: () {
                        controller.decrementCreationItem(branchItem);
                    },
                    value: branchItem.orderedCount ?? 0,
                  ),
                ],
              ),
            ),
          ),
        );
      }
    );
  }
}
