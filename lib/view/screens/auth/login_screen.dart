import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/view/widgets/shared/primary_button.dart';
import 'package:khdma_pos/view/widgets/shared/shared_text_form_field.dart';

import '../../../l10n/app_localizations.dart';
import '../../../logic/controllers/auth_controller.dart';
import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';
import '../../widgets/shared/app_logo.dart';
import '../../widgets/shared/text_utils.dart';

class LoginScreen extends StatelessWidget {

  LoginScreen({Key? key}) : super(key: key);

  final TextEditingController emailController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  final controller = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            backgroundColor: ThemesManager().isSavedDarkMode()
                ? ColorsManager.svgDark
                : ColorsManager.svgLight),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AppLogo(),
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.58,
                child: Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: TextUtils(
                                text: AppLocalizations.of(context)!
                                    .loginToAccount,
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                                align: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 25),
                        SharedTextFormField(
                          controller: emailController,
                          obscureText: false,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'phone is required';
                            }
                            return null;
                          },
                          prefixIcon: CupertinoIcons.person_solid,
                          hintText: AppLocalizations.of(context)!.email,
                        ),
                        const SizedBox(height: 20),
                        GetBuilder<AuthController>(builder: (_) {
                          return SharedTextFormField(
                            controller: passwordController,
                            obscureText: controller.isVisibility ? false : true,
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'password is required';
                              }
                              return null;
                            },
                            prefixIcon: CupertinoIcons.lock_fill,
                            hintText: AppLocalizations.of(context)!.password,
                            suffixIcon: IconButton(
                              onPressed: () {
                                controller.visibility();
                              },
                              icon: controller.isVisibility
                                  ? Icon(
                                Icons.visibility,
                                color: ColorsManager.main,
                              )
                                  : Icon(
                                Icons.visibility_off,
                                color: ColorsManager.main,
                              ),
                            ),
                          );
                        }),
                        const SizedBox(height: 20),
                        GetBuilder<AuthController>(builder: (_) {
                          return SizedBox(
                            width: double.infinity,
                            child: PrimaryButton(
                              text: AppLocalizations.of(context)!.signIn,
                              onPressed: () {
                                if (formKey.currentState!.validate()) {
                                  controller.login(
                                      email: emailController.text,
                                      password: passwordController.text);
                                }

                                //Get.offAllNamed(Routes.home);
                              }, disabled: false,
                            ),
                          );
                        })
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
