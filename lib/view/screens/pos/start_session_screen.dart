import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";
import "package:get/get_state_manager/src/simple/get_state.dart";
import "package:khdma_pos/data/model/branch_user_model.dart";
import "package:khdma_pos/data/model/customer_branch_model.dart";
import "package:khdma_pos/logic/controllers/session_controller.dart";
import "package:khdma_pos/services/auth_service.dart";
import "package:khdma_pos/services/customer/customer_service.dart";
import "package:khdma_pos/view/widgets/shared/accent_button.dart";
import "package:khdma_pos/view/widgets/shared/text_utils.dart";

import "../../../l10n/app_localizations.dart";
import "../../../logic/controllers/auth_controller.dart";
import "../../widgets/shared/app_dropdown_input.dart";
import "../../widgets/shared/primary_button.dart";
import "../../widgets/shared/secondary_button.dart";
import "../../widgets/shared/shared_text_form_field.dart";

class StartSessionScreen extends StatelessWidget {
  StartSessionScreen({super.key, required this.food});
  final bool food;
  final controller = Get.put(SessionController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text(controller.isStartSession == true
              ? AppLocalizations.of(context)!.startSession
              : AppLocalizations.of(context)!
              .continueSession,),
          actions: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: AccentButton(
                  text: AppLocalizations.of(context)!.logout,
                  onPressed: AuthService().logout),
            )
          ],
        ),
        body: Obx(() {
          if (controller.isLoading.value) {
            return const Center(child: CircularProgressIndicator());
          } else {
            print("food $food");
            return SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25,10,25,25),
                    child: Form(
                      key: controller.sessionFormKey,
                      child: Column(
                        children: [
                          AppDropdownInput(
                            hintText: AppLocalizations.of(context)!
                                .pleaseSelectBranch,
                            options: controller.branchesList,
                            enabled: controller.isStartSession.value,
                            value: controller.selectedBranch ??
                                controller.branchesList[0],
                            onChanged: (CustomerBranch value) {
                              controller.onBranchChange(value.id);
                              controller.selectedBranch = value;
                              controller.update();
                            },
                            getLabel: (value) => value.branchName,
                          ),
                          if (controller.branchUsersList.isNotEmpty)
                            Column(
                              children: [
                                const SizedBox(height: 15),
                                AppDropdownInput(
                                  enabled: controller.isStartSession.value,
                                  hintText: AppLocalizations.of(context)!
                                      .sessionDeliverFrom,
                                  options: controller.branchUsersList,
                                  value: controller.selectedBranchUser ??
                                      controller.branchUsersList[0],
                                  onChanged: (BranchUser value) {
                                    controller
                                        .getLastSessionFromUserId(value.id);
                                    controller.selectedBranchUser = value;
                                    controller.update();
                                  },
                                  getLabel: (value) => value.fullName,
                                ),
                              ],
                            ),
                          const SizedBox(height: 15),
                          SharedTextFormField(
                            controller: controller.pettyCashAmountController,
                            obscureText: false,
                            enabled: controller.isStartSession.value,
                            keyboardType: TextInputType.number,
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Petty Cash Amount is required';
                              }
                              return null;
                            },
                            hintText:
                                AppLocalizations.of(context)!.pettyCashAmount,
                          ),
                          const SizedBox(height: 15),
                          SharedTextFormField(
                            controller: controller.totalCashAmountController,
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            enabled: false,
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Total Cash Amount is required';
                              }
                              return null;
                            },
                            hintText:
                                AppLocalizations.of(context)!.totalCashAmount,
                          ),
                          const SizedBox(height: 15),
                          SharedTextFormField(
                            controller: controller.varianceController,
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            enabled: false,
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Variance is required';
                              }
                              return null;
                            },
                            hintText: AppLocalizations.of(context)!.variance,
                          ),
                          const SizedBox(height: 15),
                          SharedTextFormField(
                            controller: controller.justificationController,
                            obscureText: false,
                            enabled: controller.isStartSession.value,
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Justification is required';
                              }
                              return null;
                            },
                            hintText:
                                AppLocalizations.of(context)!.justification,
                          ),
                          const SizedBox(height: 30),
                          SizedBox(
                            width: double.infinity,
                            child: PrimaryButton(
                              text: controller.isStartSession.value
                                  ? AppLocalizations.of(context)!.startSession
                                  : AppLocalizations.of(context)!
                                      .continueSession,
                              onPressed: () {
                                if (controller.sessionFormKey.currentState!
                                    .validate()) {
                                  controller.isStartSession.value ?
                                  controller.startOrdersCreations( isFood: food) :
                                  controller.continueOrdersCreations( isFood: food);
                                }
                              },
                              disabled: false,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        }));
  }
}
