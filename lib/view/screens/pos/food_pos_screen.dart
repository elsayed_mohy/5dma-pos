import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/food-pos_controller.dart';
import 'package:khdma_pos/utils/theme.dart';
import 'package:khdma_pos/view/widgets/pos/food_pos_main_view.dart';
import 'package:khdma_pos/view/widgets/pos/pos_main_view.dart';
import 'package:khdma_pos/view/widgets/pos/session_information_popup.dart';
import 'package:khdma_pos/view/widgets/shared/get_bottom_sheet.dart';
import 'package:khdma_pos/view/widgets/shared/primary_button.dart';
import 'package:khdma_pos/view/widgets/shared/secondary_button.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../l10n/app_localizations.dart';
import '../../../routes/routes.dart';
import '../../../utils/constants/colors.dart';
import '../../widgets/pos/pos_table_view.dart';

class FoodPOSScreen extends StatefulWidget {
  const FoodPOSScreen({super.key});

  @override
  State<FoodPOSScreen> createState() => _POSScreenState();
}

enum MenuActions {
  sessionInformation,
  emptyShoppingCart,
  refreshProducts,
  endSession,
  returnToAnotherBranch
}

class _POSScreenState extends State<FoodPOSScreen> {
  bool posTableView = false;
  final searchTextController = TextEditingController();
  final controller = Get.put(FoodPOSController());

  void startSearch() {
    ModalRoute.of(context)!
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: stopSearching));

    setState(() {
      controller.filteredFoodItems = controller.foodItems;
      controller.isSearching = true;
    });
  }

  void stopSearching() {
    clearSearch();

    setState(() {
      controller.filteredFoodItems = controller.foodItems;
      controller.isSearching = false;
    });
  }

  void clearSearch() {
    setState(() {
      searchTextController.clear();
    });
  }

  openConfirmDeleteDialog(BuildContext context) {
    Get.defaultDialog(
      title: 's',
      onConfirm: () {},
      contentPadding: EdgeInsets.all(12),
      buttonColor: ColorsManager.main,
      middleText: 'test',
      cancel: SecondaryButton(
        onPressed: () {
          Get.back();
        },
        text: AppLocalizations.of(context)!.cancel,
      ),
      confirm: PrimaryButton(
        onPressed: () {
         // controller.deleteBox();
        },
        text: AppLocalizations.of(context)!.remove,
        disabled: false,
      ),
      confirmTextColor: ThemesManager().isSavedDarkMode()
          ? ColorsManager.white
          : ColorsManager.white,
      cancelTextColor: ThemesManager().isSavedDarkMode()
          ? ColorsManager.white
          : ColorsManager.darkGrey,
      onCancel: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: controller.isSearching ? BackButton() : null,
          title:
              controller.isSearching ? buildSearchField() : buildAppBarTitle(),
          actions: buildAppBarActions(),
        ),
        floatingActionButton: FloatingActionButton(
          mini: true,
          child: GetBuilder<FoodPOSController>(
              init: FoodPOSController(),
              builder: (FoodPOSController controller) {
                return Badge(
                  label: TextUtils(
                    text: "${controller.selectedItemsCount}",
                    color: Colors.white,
                    fontSize: 10,
                  ),
                  child: Icon(
                    CupertinoIcons.cart_fill,
                    size: 18,
                  ),
                );
              }),
          onPressed: () {
            var data = {
              "id" : "order-creation",
            };
            Get.toNamed(Routes.viewOrder,parameters: data);
          },
        ),
        body: posTableView ? POSTableView() : FoodPOSMainView());
  }

  Widget buildAppBarTitle() {
    return Text(
      AppLocalizations.of(context)!.pos,
    );
  }

  List<Widget> buildAppBarActions() {
    if (controller.isSearching) {
      return [
        IconButton(
          onPressed: () {
            clearSearch();
            Navigator.pop(context);
          },
          icon: Icon(CupertinoIcons.xmark),
        ),
      ];
    } else {
      return [
        IconButton(
          onPressed: startSearch,
          icon: Icon(
            CupertinoIcons.search,
          ),
        ),
        IconButton(
          onPressed: () {
            setState(() {
              posTableView = !posTableView;
            });
          },
          icon: Icon(
            CupertinoIcons.arrow_right_arrow_left,
          ),
        ),
        menuActions()
        // IconButton(
        //   onPressed: startSearch,
        //   icon: Icon(
        //     CupertinoIcons.bars,
        //   ),
        // ),
      ];
    }
  }

  Widget buildSearchField() {
    return TextField(
      controller: searchTextController,
      cursorColor: ColorsManager.disabled,
      style: TextStyle(color: ColorsManager.white),
      decoration: InputDecoration(
        hintText: AppLocalizations.of(context)!.searchByNameOrBarcode,
        hintStyle: TextStyle(fontSize: 12, color: ColorsManager.white),
        border: InputBorder.none,
      ),
      onChanged: (searchedCharacter) {
        setState(() {
          controller.filterData(searchedCharacter);
        });
      },
    );
  }

  Widget menuActions() {
    return PopupMenuButton<MenuActions>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      // child: Icon(CupertinoIcons.bars),

      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          value: MenuActions.sessionInformation,
          child: TextUtils(
            text: 'Session information',
          ),
        ),
        PopupMenuItem(
          value: MenuActions.emptyShoppingCart,
          child: TextUtils(
            text: 'Empty shopping cart',
          ),
        ),
        PopupMenuItem(
          value: MenuActions.refreshProducts,
          child: TextUtils(
            text: 'Refresh products',
          ),
        ),
        PopupMenuItem(
          value: MenuActions.endSession,
          child: TextUtils(
            text: 'End session',
          ),
        ),
        PopupMenuItem(
          value: MenuActions.returnToAnotherBranch,
          child: TextUtils(
            text: 'Return to another branch',
          ),
        ),
      ],
      onSelected: (value) async {
        switch (value) {
          case MenuActions.sessionInformation:
            getBottomSheet(250, SessionInformationPopup());
            print("session information");
            //controller.downloadPdf();
            break;
          case MenuActions.emptyShoppingCart:
            controller.emptyCart();
            break;
          case MenuActions.refreshProducts:
            //controller.downloadPdf();
            break;
          case MenuActions.endSession:
            //controller.downloadPdf();
            break;
          case MenuActions.returnToAnotherBranch:
            //controller.downloadPdf();
            break;
        }
        // var subBoxes = controller.getSubscriptionBoxes();
      },
    );
  }
}
