import 'package:flutter/material.dart';

class TablesLayoutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Cafe Table Layout')),
      body: Center(
        child: Table(
          border: TableBorder.all(),
          children: [
            TableRow(
              children: [
                TableCell(child: _buildTableItem('Table 1')),
                TableCell(child: _buildTableItem('Table 2')),
              ],
            ),
            TableRow(
              children: [
                TableCell(child: _buildTableItem('Table 3')),
                TableCell(child: _buildTableItem('Table 4')),
              ],
            ),
            // Add more rows and columns as needed
          ],
        ),
      ),
    );
  }

  Widget _buildTableItem(String tableName) {
    return Container(
      padding: EdgeInsets.all(8.0),
      alignment: Alignment.center,
      child: Text(tableName),
    );
  }
}