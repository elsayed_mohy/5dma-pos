import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:flutter_svg/svg.dart";
import "package:get/get.dart";
import "package:khdma_pos/view/widgets/shared/card_container.dart";
import "../../../l10n/app_localizations.dart";
import "../../../logic/controllers/orders_controller.dart";
import "../../../utils/constants/colors.dart";
import "../../widgets/orders/order_card.dart";
import "../../widgets/shared/text_utils.dart";

class FoodOrdersScreen extends StatefulWidget {
  const FoodOrdersScreen({super.key});

  @override
  State<FoodOrdersScreen> createState() => _FoodOrdersScreenState();
}

class _FoodOrdersScreenState extends State<FoodOrdersScreen> {

  final controller = Get.put(OrdersController());
  bool isSearching = false;
  bool isAdmin = true;
  final searchTextController = TextEditingController();

  // List<BoxModel> boxes = [];

  void startSearch() {
    ModalRoute.of(context)!
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: stopSearching));

    setState(() {
      isSearching = true;
    });
  }

  void stopSearching() {
    clearSearch();

    setState(() {
      isSearching = false;
    });
  }

  void clearSearch() {
    setState(() {
      searchTextController.clear();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: isSearching ? BackButton() : null,
        title: isSearching ? buildSearchField() : buildAppBarTitle(),
        actions: buildAppBarActions(),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 13.0),
        child: Column(
          children: [
            Expanded(child: Obx(
                  () {
                if (controller.isLoading.value) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Container(
                    width: double.maxFinite,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: controller.orders.isNotEmpty
                          ? SingleChildScrollView(
                        child: ListView.builder(
                            physics: ClampingScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context,
                                int index) =>
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: OrderCard(order: controller.orders[index],),
                                ),
                            itemCount: controller.orders.length),
                      )
                          : Center(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Column(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: double.infinity,
                                    child: SvgPicture.asset(
                                      'assets/images/ui/no-data.svg',
                                      height: 150,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  TextUtils(
                                    text: AppLocalizations.of(context)!
                                        .orderNotExists,
                                    fontSize: 17,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }
                // var subBoxes = controller.getSubscriptionBoxes();
              },
            )),
          ],
        ),
      ),
    );
  }

  Widget buildAppBarTitle() {
    return Text(AppLocalizations.of(context)!.ordersPage);
  }

  List<Widget> buildAppBarActions() {
    if (isSearching) {
      return [
        IconButton(
          onPressed: () {
            clearSearch();
            Navigator.pop(context);
          },
          icon: Icon(CupertinoIcons.xmark),
        ),
      ];
    } else {
      return [
        IconButton(
          onPressed: startSearch,
          icon: Icon(
            CupertinoIcons.search,
          ),
        ),
      ];
    }
  }

  Widget buildSearchField() {
    return TextField(
      controller: searchTextController,
      cursorColor: ColorsManager.disabled,
      decoration: InputDecoration(
        hintText: AppLocalizations.of(context)!.search,
        border: InputBorder.none,
      ),
      onChanged: (searchedCharacter) {
      },
    );
  }
}

