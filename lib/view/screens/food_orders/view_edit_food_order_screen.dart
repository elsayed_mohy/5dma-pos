import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';
import 'package:khdma_pos/view/widgets/orders/order_customer_tile.dart';
import 'package:khdma_pos/view/widgets/orders/order_delivery_tile.dart';
import 'package:khdma_pos/view/widgets/orders/order_details_tile.dart';
import 'package:khdma_pos/view/widgets/orders/order_items_tile.dart';
import 'package:khdma_pos/view/widgets/orders/order_returned_items_tile.dart';
import 'package:khdma_pos/view/widgets/orders/order_status_tile.dart';
import 'package:khdma_pos/view/widgets/orders/order_summery_card.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../l10n/app_localizations.dart';

enum MenuActions {
  save,
  saveAndPrint,
  saveAndClose,
  returnOrder,
  preparedOrder,
  withDriver,
  orderDelivered,
  closeOrder,
  cancelOrder,
  updateOrderItems,
  emptyCart,
  printOrder,
  cancelOrderAndCreateNew
}

class ViewEditFoodOrderScreen extends StatefulWidget {
  ViewEditFoodOrderScreen({super.key});

  @override
  State<ViewEditFoodOrderScreen> createState() => _ViewEditFoodOrderScreenState();
}

class _ViewEditFoodOrderScreenState extends State<ViewEditFoodOrderScreen> {
  final controller = Get.put(ViewOrderController());

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: buildAppBarActions(),
        ),
        body: SingleChildScrollView(child: Obx(() {
          if (controller.isLoading.isTrue) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return Column(
              children: [
                OrderDetailsTile(),
                if (controller.orderCreation.isFalse &&
                    controller.orderDetails != null)
                  OrderStatusTile(),
                OrderCustomerTile(),
                OrderItemsTile(),
                if (controller.orderDetails?.order != null &&
                    controller.orderDetails!.order.returnOrders.isNotEmpty)
                  OrderReturnedItemsTile(),
                OrderDeliveryTile(),
                SizedBox(
                  height: 5,
                ),
                OrderSummeryCard(),
              ],
            );
        })));
  }

  List<Widget> buildAppBarActions() {
    return [menuActions()];
  }

  Widget menuActions() {
    return PopupMenuButton<MenuActions>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      // child: Icon(CupertinoIcons.bars),

      itemBuilder: (BuildContext context) => [
        if (controller.orderDetails?.order.orderStatus.code == null ||
            (controller.orderDetails?.order.orderStatus.code != null &&
                (controller.orderDetails?.order.orderStatus.code != 'CLOSED' &&
                    controller.orderDetails?.order.orderStatus.code !=
                        'CANCELED' &&
                    controller.orderDetails?.order.orderStatus.code !=
                        'TOTAL_RETURN' &&
                    controller.orderDetails?.order.orderStatus.code !=
                        'PARTIAL_RETURN' &&
                    controller.orderDetails?.order.orderStatus.code !=
                        'DELIVERED')))
          PopupMenuItem(
            value: MenuActions.save,
            child: TextUtils(
              text: AppLocalizations.of(context)!.save,
            ),
          ),
        if (controller.orderDetails?.order.orderStatus.code == null ||
            controller.orderDetails?.order.orderStatus.code == "NEW")
          PopupMenuItem(
            value: MenuActions.saveAndPrint,
            child: TextUtils(
              text: AppLocalizations.of(context)!.saveAndPrint,
            ),
          ),
        if (controller.orderDetails?.order.orderStatus.code != "CANCELED" &&
            controller.orderDetails?.order.orderStatus.code != "CLOSED" &&
            controller.orderDetails?.order.orderStatus.code !=
                "PARTIAL_RETURN" &&
            controller.orderDetails?.order.orderStatus.code != "TOTAL_RETURN")
          PopupMenuItem(
            value: MenuActions.saveAndClose,
            child: TextUtils(
              text: AppLocalizations.of(context)!.saveAndClose,
            ),
          ),
        if (controller.selectedDeliveryMethod?.id != null &&
            (controller.orderDetails?.order.orderStatus.code == null ||
                controller.orderDetails?.order.orderStatus.code == "NEW" ||
                controller.orderDetails?.order.orderStatus.code == "PROCESS"))
          PopupMenuItem(
            value: MenuActions.preparedOrder,
            child: TextUtils(
              text: AppLocalizations.of(context)!.prepared,
            ),
          ),
        if (controller.selectedDeliveryMethod?.id != null &&
            (controller.orderDetails?.order.orderStatus.code == "PREPARED"))
          PopupMenuItem(
            value: MenuActions.withDriver,
            child: TextUtils(
              text: AppLocalizations.of(context)!.withDriver,
            ),
          ),
        if (controller.selectedDeliveryMethod?.id != null &&
            (controller.orderDetails?.order.orderStatus.code == "DRIVER"))
          PopupMenuItem(
            value: MenuActions.orderDelivered,
            child: TextUtils(
              text: AppLocalizations.of(context)!.delivered,
            ),
          ),
        if (controller.selectedDeliveryMethod?.id != null &&
            (controller.orderDetails?.order.orderStatus.code == "DELIVERED"))
          PopupMenuItem(
            value: MenuActions.closeOrder,
            child: TextUtils(
              text: AppLocalizations.of(context)!.closeOrder,
            ),
          ),
        if (controller.orderCreation.isTrue)
          PopupMenuItem(
            value: MenuActions.updateOrderItems,
            child: TextUtils(
              text: AppLocalizations.of(context)!.addItem,
            ),
          ),
        if (controller.orderCreation.isTrue)
          PopupMenuItem(
            value: MenuActions.emptyCart,
            child: TextUtils(
              text: AppLocalizations.of(context)!.emptyCart,
            ),
          ),
        if (controller.orderDetails?.order.orderStatus.code != null &&
            (controller.orderDetails?.order.orderStatus.code != 'CLOSED' &&
                controller.orderDetails?.order.orderStatus.code != 'CANCELED' &&
                controller.orderDetails?.order.orderStatus.code !=
                    'TOTAL_RETURN' &&
                controller.orderDetails?.order.orderStatus.code !=
                    'PARTIAL_RETURN' &&
                controller.orderDetails?.order.orderStatus.code != 'DELIVERED'))
          PopupMenuItem(
            value: MenuActions.cancelOrder,
            child: TextUtils(
              text: AppLocalizations.of(context)!.cancelOrder,
            ),
          ),
        if (controller.orderDetails?.order.orderStatus.code == "CLOSED" ||
            controller.orderDetails?.order.orderStatus.code == "PARTIAL_RETURN")
          PopupMenuItem(
            value: MenuActions.returnOrder,
            child: TextUtils(
              text: AppLocalizations.of(context)!.returnOrder,
            ),
          ),
        if (controller.orderCreation.isFalse)
          PopupMenuItem(
            value: MenuActions.printOrder,
            child: TextUtils(
              text: AppLocalizations.of(context)!.print,
            ),
          ),
      ],
      onSelected: (value) async {
        switch (value) {
          case MenuActions.printOrder:
            controller.printFile(
                orderId: controller.orderId,
                taxNumber: controller.orderDetails?.order.issuingNumber);
            break;
          case MenuActions.save:
            controller.save();
          case MenuActions.saveAndPrint:
            controller.saveAndPrint();
          case MenuActions.saveAndClose:
            controller.saveAndClose();
          case MenuActions.returnOrder:
          // TODO: Handle this case.
          case MenuActions.preparedOrder:
            controller.preparedOrder();
          case MenuActions.withDriver:
            controller.withDriver();
          case MenuActions.orderDelivered:
            controller.orderDelivered();
          case MenuActions.closeOrder:
            controller.closeOrder();
          case MenuActions.cancelOrder:
            controller.cancelOrder();
          case MenuActions.updateOrderItems:
          // TODO: Handle this case.
          case MenuActions.emptyCart:
            controller.emptyCart();
          case MenuActions.cancelOrderAndCreateNew:
            controller.cancelOrderAndCreateNew();
        }
        // var subBoxes = controller.getSubscriptionBoxes();
      },
    );
  }
}
