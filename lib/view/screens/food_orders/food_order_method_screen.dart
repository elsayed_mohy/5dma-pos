import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/utils.dart';
import 'package:khdma_pos/view/widgets/shared/primary_button.dart';
import 'package:khdma_pos/view/widgets/shared/text_utils.dart';

import '../../../l10n/app_localizations.dart';
import '../../../routes/routes.dart';

enum FoodOrderMethods { inRestaurant, takeaway, delivery }

class FoodOrderMethodScreen extends StatefulWidget {
  const FoodOrderMethodScreen({super.key});

  @override
  State<FoodOrderMethodScreen> createState() => _FoodOrderMethodScreenState();
}

class _FoodOrderMethodScreenState extends State<FoodOrderMethodScreen> {
  FoodOrderMethods? method = FoodOrderMethods.inRestaurant;

  navigate() {
    Get.toNamed(method == FoodOrderMethods.inRestaurant ? Routes.tablesScreen :Routes.foodPos);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.theme.colorScheme.background,
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.restaurants),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              TextUtils(text: AppLocalizations.of(context)!.chooseService),
              SizedBox(height: 40),
              Column(
                children: <Widget>[
                  RadioListTile<FoodOrderMethods>(
                    contentPadding: EdgeInsets.all(8),
                    tileColor: Colors.white,
                    title: const Text('In Restaurant'),
                    value: FoodOrderMethods.inRestaurant,
                    groupValue: method,
                    controlAffinity: ListTileControlAffinity.trailing,
                    onChanged: (FoodOrderMethods? value) {
                      setState(() {
                        method = value;
                      });
                    },
                  ),
                  SizedBox(height: 16,),
                  RadioListTile<FoodOrderMethods>(
                    contentPadding: EdgeInsets.all(8),
                    tileColor: Colors.white,
                    title: const Text('Take away'),
                    value: FoodOrderMethods.takeaway,
                    groupValue: method,
                    controlAffinity: ListTileControlAffinity.trailing,
                    onChanged: (FoodOrderMethods? value) {
                      setState(() {
                        method = value;
                      });
                    },
                  ),
                  SizedBox(height: 16,),
                  RadioListTile<FoodOrderMethods>(
                    contentPadding: EdgeInsets.all(8),
                    tileColor: Colors.white,
                    title: const Text('Delivery'),
                    value: FoodOrderMethods.delivery,
                    groupValue: method,
                    controlAffinity: ListTileControlAffinity.trailing,
                    onChanged: (FoodOrderMethods? value) {
                      setState(() {
                        method = value;
                      });
                    },
                  ),
                ],
              ),
              SizedBox(height: 50),
              SizedBox(
                width: double.infinity,
                child: PrimaryButton(
                    text: AppLocalizations.of(context)!.save,
                    onPressed: () {
                      navigate();
                    },
                    disabled: false),
              )
            ],
          ),
        ),
      ),
    );
  }
}
