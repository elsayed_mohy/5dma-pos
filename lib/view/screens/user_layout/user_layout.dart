import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:khdma_pos/view/screens/land_screens/home_screen.dart';
import 'package:khdma_pos/view/screens/pos/start_session_screen.dart';

import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/colors.dart';
import '../../widgets/shared/text_utils.dart';

class UserLayout extends StatefulWidget {
  const UserLayout({super.key});

  static final List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    StartSessionScreen(food: false),
  ];

  @override
  State<UserLayout> createState() => _UserLayoutState();
}

class _UserLayoutState extends State<UserLayout> {
  int currentTab = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: context.theme.colorScheme.background,
      body: Center(
        child: UserLayout._widgetOptions.elementAt(currentTab),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        shape: CircleBorder(),
        backgroundColor: ColorsManager.secondary,
        elevation: 0,
        onPressed: () {
          setState(() {
            // if(currentTab == 2) {
            // Get.toNamed(Routes.home + Routes.addEditInvoiceScreenFromHome);
            // }else {
            //   currentTab = 2;
            // }
          });
        },
        child: Icon(CupertinoIcons.cart_fill),
      ),
      bottomNavigationBar: BottomAppBar(
        color: context.theme.colorScheme.background,
        surfaceTintColor: Colors.white,
        shadowColor: ColorsManager.darkGrey,
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        elevation: 10,
        child: SizedBox(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 0;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            currentTab == 0
                                ? CupertinoIcons.house_fill
                                : CupertinoIcons.house,
                            color: currentTab != 0
                                ? ColorsManager.disabled
                                : ColorsManager.main,
                          ),
                          FittedBox(
                            fit: BoxFit.fill,
                            child: TextUtils(
                                text: AppLocalizations.of(context)!.mainPage,
                                fontSize: 11),
                          )

                          //const Padding(padding: EdgeInsets.all(10))
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 1;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            currentTab == 1
                                ? CupertinoIcons.layers_fill
                                : CupertinoIcons.layers,
                            color: currentTab != 1
                                ? ColorsManager.disabled
                                : ColorsManager.main,
                          ),
                          FittedBox(
                            fit: BoxFit.fill,
                            child: TextUtils(
                                text: AppLocalizations.of(context)!.ordersPage,
                                fontSize: 11),
                          )
                          //const Padding(padding: EdgeInsets.only(left: 10))
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(child: SizedBox()),
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 2;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            currentTab == 2
                                ? CupertinoIcons.car_fill
                                : CupertinoIcons.car,
                            color: currentTab != 2
                                ? ColorsManager.disabled
                                : ColorsManager.main,
                          ),
                          FittedBox(
                            fit: BoxFit.fill,
                            child: TextUtils(
                                text: AppLocalizations.of(context)!
                                    .transportation,
                                fontSize: 11),
                          )
                          //const Padding(padding: EdgeInsets.only(left: 10))
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 3;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            currentTab == 3
                                ? CupertinoIcons.ellipsis
                                : CupertinoIcons.ellipsis,
                            color: currentTab != 3
                                ? ColorsManager.disabled
                                : ColorsManager.main,
                          ),
                          FittedBox(
                            fit: BoxFit.fill,
                            child: TextUtils(
                                text: AppLocalizations.of(context)!.more,
                                fontSize: 11),
                          )

                          //const Padding(padding: EdgeInsets.only(left: 10))
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
