import "package:flutter/material.dart";
import "package:get/get.dart";
import "package:khdma_pos/routes/routes.dart";
import "package:khdma_pos/utils/constants/colors.dart";
import "package:khdma_pos/view/widgets/shared/land_card.dart";
import "package:khdma_pos/view/widgets/shared/text_utils.dart";

import "../../../l10n/app_localizations.dart";

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        // resizeToAvoidBottomInset: false,
        body: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(color: ColorsManager.main),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Center(
                child: Image.asset(
                  "assets/images/ui/logo.png",
                  height: 60,
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: context.theme.colorScheme.background,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16, vertical: 12),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextUtils(text: AppLocalizations.of(context)!.welcome,fontWeight: FontWeight.bold,),
                                SizedBox(height: 8),
                                TextUtils(
                                    text:
                                    AppLocalizations.of(context)!.chooseService,),
                              ])),
                      SizedBox(height: 10),
                      Expanded(
                        child: GridView.count(
                          childAspectRatio: (40 / 35),
                          crossAxisCount: 2,
                          children: [
                            LandCard(
                                action: () {
                                },
                                label: AppLocalizations.of(context)!.shopNow,
                                iconPath: "assets/images/ui/cart.svg"),
                            LandCard(
                                action: () {},
                                label: AppLocalizations.of(context)!.transportation,
                                iconPath: "assets/images/ui/destination.svg"),
                            LandCard(
                                action: () {
                                  Get.toNamed(Routes.startOrdersSessionScreen);
                                },
                                label: AppLocalizations.of(context)!.ordersPage,
                                iconPath: "assets/images/ui/icon.svg"),
                            LandCard(
                                action: () {
                                  Get.toNamed(Routes.startFoodSessionScreen);
                                },
                                label: AppLocalizations.of(context)!.restaurants,
                                iconPath: "assets/images/ui/dish.svg"),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
