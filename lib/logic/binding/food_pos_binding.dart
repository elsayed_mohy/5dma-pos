import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/food-pos_controller.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';


class FoodPOSBinding extends Bindings{

  @override
  void dependencies() {
    Get.put(FoodPOSController());

  }

}