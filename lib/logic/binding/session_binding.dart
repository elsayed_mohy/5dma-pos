import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/auth_controller.dart';
import 'package:khdma_pos/logic/controllers/session_controller.dart';


class SessionBinding extends Bindings{

  @override
  void dependencies() {
    Get.put(SessionController());

  }

}