import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';


class POSBinding extends Bindings{

  @override
  void dependencies() {
    Get.put(POSController());

  }

}