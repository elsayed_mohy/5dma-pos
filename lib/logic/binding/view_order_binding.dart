import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/view_order_controller.dart';

class ViewOrderBinding extends Bindings{

  @override
  void dependencies() {
    Get.put(ViewOrderController());
  }

}