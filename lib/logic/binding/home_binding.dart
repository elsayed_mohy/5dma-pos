
import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/food-pos_controller.dart';
import 'package:khdma_pos/logic/controllers/orders_controller.dart';
import 'package:khdma_pos/logic/controllers/pos_controller.dart';
import 'package:khdma_pos/logic/controllers/session_controller.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => SessionController());
    Get.lazyPut(() => POSController());
    Get.lazyPut(() => OrdersController());
    Get.lazyPut(() => FoodPOSController());
  }

}
