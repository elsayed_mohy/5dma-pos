import 'package:get/get.dart';
import 'package:khdma_pos/logic/controllers/orders_controller.dart';

class OrdersBinding extends Bindings{

  @override
  void dependencies() {
    Get.put(OrdersController());
  }

}