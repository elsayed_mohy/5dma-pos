import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/services/auth_service.dart';

import '../../data/model/customer_branch_model.dart';
import '../../data/model/user.dart';
import '../../main.dart';
import '../../routes/routes.dart';
import '../../services/base_service.dart';
import '../../services/customer/customer_service.dart';
import '../../utils/constants/app_urls.dart';

class AuthController extends GetxController {
  bool isVisibility = false;
  final Dio dio = Dio();
  bool isLoggedIn = false;
  final GetStorage storage = GetStorage();

  String lang = GetStorage().read<String>('lang') != null
      ? GetStorage().read<String>('lang').toString()
      : getDeviceLanguage();

  void visibility() {
    isVisibility = !isVisibility;
    update();
  }

  Future getCustomerBranches() async {

      var list = await CustomerService().getCustomerBranches();
      try {
        if (list.isNotEmpty) {
         await GetStorage().write('customer_branches', list);
         update();
        }
      } finally {
      }
    // }
  }

  void login({required String email, required String password}) async {
    print("login");
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await dio.post(AppUrls.logInEndPoint,
            data: {'login': email, 'password': password},
            options: Options(
              headers: {'accept-language': lang},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          isLoggedIn = true;
          storage.write('loggedIn', isLoggedIn);
         // log(response.data['data']['']);

          User user = User.fromJson(response.data['data']);
          print(user.branchConfig.toJson());
          storage.write('user', userToJson(user));
          update();
          await getCustomerBranches();
          AuthService().getConfigData();
          Get.offNamed(Routes.home);
        } else {
          BaseService().showError(response.data["message"].toString());
        }
      }
    } on SocketException catch (_) {
      print("internet error");
      BaseService().showInternetError();
      return;
    }
  }


}
