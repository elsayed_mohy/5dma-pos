import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/data/model/common/config.dart';
import 'package:khdma_pos/data/model/order/branch_category.dart';
import 'package:khdma_pos/data/model/order/branch_item.dart';
import 'package:khdma_pos/routes/routes.dart';
import 'package:khdma_pos/services/auth_service.dart';

import '../../data/model/session_model.dart';
import '../../services/pos/orders_service.dart';
import '../../services/pos/session_service.dart';

class POSController extends GetxController {
  SessionService sessionService = SessionService();
  late AnimationController animationController;
  List<BranchItem> branchItems = [];
  List<BranchItem> filteredBranchItems = [];
  List<BranchItem> selectedItems = [];
  List<BranchCategory> branchCategories = [];
  BranchCategory? selectedBranchCategory;
  List<PaymentMethod> paymentMethods = [];
  PaymentMethod? selectedPaymentMethod;
  List<bool> paymentIsSelected = [true, false, false, false];
  int selectedItemsCount = 0;
  double totalPrice = 0.00;
  double totalDiscount = 0.00;
  double totalTax = 0.00;
  Session? sessionData;
  RxBool isLoadingTableView = true.obs;
  bool isSearching = false;
  bool isLoading = true;
  bool isItemsLoading = true;
  bool isFlipped = false;
  GetStorage storage = GetStorage();

  selectItem(BranchItem item) {
    var index =
        selectedItems.indexWhere((selectedItem) => selectedItem.id == item.id);
    if (index >= 0) {
      item.orderedCount = selectedItems[index].orderedCount;
    }
    if (item.orderedCount != null && index >= 0) {
      item.orderedCount = item.orderedCount! + 1;
      selectedItems[index].orderedCount = item.orderedCount;
      update();
    } else {
      item.orderedCount = 1;
      selectedItemsCount++;
      selectedItems.add(item);
      update();
    }
    saveSelectedPosItems();
  }

  showSubCategories(BranchCategory category) {
    selectedBranchCategory = category;
    isFlipped = !isFlipped;
    isFlipped ? animationController.forward() : animationController.reverse();
    filteredBranchItems = branchItems
        .where((element) => element.item?.categoryId == category.id)
        .toList();
    update();
  }

  showSubCategoriesItems(SubCategory subCategory) {
    filteredBranchItems = branchItems
        .where((element) => element.item?.subCategoryId == subCategory.id)
        .toList();
    update();
  }

  hideSubCategories() {
    isFlipped = !isFlipped;
    isFlipped ? animationController.forward() : animationController.reverse();
    filteredBranchItems = branchItems;
    update();
  }

  deselectItem(BranchItem item) {
    if (item.orderedCount! > 0) {
      item.orderedCount = item.orderedCount! - 1;
      var index = selectedItems
          .indexWhere((selectedItem) => selectedItem.id == item.id);
      selectedItems[index].orderedCount = item.orderedCount;

      if (item.orderedCount == 0) {
        selectedItemsCount--;
        selectedItems.removeWhere((item) => item.id != item.id);
      }
      saveSelectedPosItems();
      update();
    }
  }

  getItemQty(BranchItem item) {
    if (selectedItems.isNotEmpty) {
      var orderCount = 0;
      for (var selectedItem in selectedItems) {
        if (selectedItem.id == item.id) {
          item.orderedCount = selectedItem.orderedCount;
          orderCount = selectedItem.orderedCount ?? 0;
        }
      }
      return orderCount;
    }
    return 0;
  }

  saveSelectedPosItems() async {
    var storeSelectedItems = selectedItems.map((e) => json.encode(e)).toList();
   await GetStorage().write("SelectedPosItems", selectedItems);
    getOrderInfoInOrderCreation(selectedItems);
  }

  getSelectedPosItems() async {
    var localSelectedPosItems = storage.read("SelectedPosItems");
    if (localSelectedPosItems != null) {
      isLoadingTableView.value = true;
     // var items = await json.decode(storage.read('SelectedPosItems'));
      var items = await GetStorage().read('SelectedPosItems') as List;
      var selectedPosItems =
          items.map((branchItem) => BranchItem.fromJson(branchItem)).toList();
      selectedItems = selectedPosItems;
      getOrderInfoInOrderCreation(selectedItems);
      selectedItemsCount = selectedPosItems.length;
      isLoadingTableView.value = false;
    } else {
      isLoadingTableView.value = true;
      selectedItems = [];
      getOrderInfoInOrderCreation(selectedItems);
      selectedItemsCount = selectedItems.length;
      isLoadingTableView.value = false;
    }
  }

  getBranchItems({required customerBranchId}) async {
    if (GetStorage().read('branch_items') != null) {
      var localBranchItems = await GetStorage().read('branch_items') as List;
      var localBranchItemsList = localBranchItems
          .map((branchItem) => BranchItem.fromJson(branchItem))
          .toList();
      branchItems = localBranchItemsList;
      filteredBranchItems = branchItems;
      isItemsLoading = false;
      update();
    } else {
      var list =
          await OrdersService().getBranchItems(branchId: customerBranchId);
      try {
        if (list.isNotEmpty) {
          branchItems = list;
          filteredBranchItems = branchItems;
          isItemsLoading = false;
          update();
        }
      } catch (error) {
        isItemsLoading = false;
        update();
      } finally {
        isLoading = false;
        update();
      }
    }
  }

  getBranchCategories({required customerBranchId}) async {
    var list =
        await OrdersService().getBranchCategories(branchId: customerBranchId);
    try {
      if (list.isNotEmpty) {
        branchCategories = list;
        selectedBranchCategory = branchCategories[0];
        isLoading = false;
        update();
      }
    } catch (error) {
      isLoading = false;
      update();
    } finally {
      isLoading = false;
      update();
    }
  }

  startSearch() {}

  filterData(searchValue) {
    for (var item in branchItems) {
      // if(typeofEquals(searchValue, type))
      if (item.barCode?.contains(searchValue.trim())) {
        filteredBranchItems = branchItems
            .where((element) => element.barCode?.contains(searchValue.trim()))
            .toList();
        update();
      } else if ((item.item?.name)
          .toLowerCase()
          .contains(searchValue.trim().toLowerCase())) {
        filteredBranchItems = branchItems
            .where((element) => element.item?.name
                .toLowerCase()
                .contains(searchValue.trim().toLowerCase()))
            .toList();
        update();
      }
    }
  }

  prepareCreationItemOfferObj(BranchItem item){
    //if(item.offerDetails)
  }

  getOrderInfoInOrderCreation(List<BranchItem> orderItems) {
    totalPrice = 0.00;
    totalDiscount = 0.00;
    totalTax = 0.00;
    if (orderItems.isNotEmpty) {
      for (var item in orderItems) {
        if (item.salesPrice != null) {
          totalPrice += item.salesPrice * item.orderedCount;
        }
        if (item.discount != null) {
          totalDiscount += item.discount * item.orderedCount;
        }
        if (item.salesTaxValue != null) {
          totalTax += item.salesTaxValue * item.orderedCount;
        }
      }
    } else {
      totalPrice = 0.00;
      totalDiscount = 0.00;
      totalTax = 0.00;
    }
  }

  setPaymentMethods() {
    if (storage.read('config') != null) {
      var config = Config.fromJson(storage.read('config'));
      paymentMethods = config.paymentMethods;
      selectedPaymentMethod = paymentMethods[0];
      update();
    }
  }

  initializeData() {
    setPaymentMethods();
    var localSession = storage.read('localSession');
    if (localSession != null) {
      if(localSession.runtimeType == Map<String, dynamic>){
        sessionData = Session.fromJson(localSession);
      }else{
        sessionData = localSession as Session;
      }
      initializeOrders(sessionData?.customerBranchId);
    } else {
      getUserSession();
    }
    getSelectedPosItems();
  }

  emptyCart() async{
    //
    print("clear the cart");
  }

  initializeOrders(branchId) async {
    await getBranchCategories(customerBranchId: branchId);
    await getBranchItems(customerBranchId: branchId);
  }

  getUserSession() async {
    var response = await sessionService.getLastSessionByUser();
    if (response.statusCode == 200) {
      var session = Session.fromJson(response.data["data"]);
      sessionData = session;
      initializeOrders(session.customerBranchId);
      update();
    } else {
      Get.toNamed(Routes.home);
    }
  }

  @override
  void onInit() {
    initializeData();
    AuthService().getConfigData();
    super.onInit();
  }
}
