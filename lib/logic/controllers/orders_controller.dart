import 'package:get/get.dart';
import 'package:khdma_pos/data/model/order/order_model.dart';
import 'package:khdma_pos/data/model/order/order_search_criteria_model.dart';
import 'package:khdma_pos/services/pos/orders_service.dart';

class OrdersController extends GetxController {
  List<OrderModel> orders = [];
  RxBool isLoading = false.obs;
  OrdersService service = OrdersService();


  filterOrders({required OrderSearchCriteria orderSearchCriteria}) async {
    isLoading.value = true;
    var list =
    await OrdersService().filterOrders(requestBody: orderSearchCriteria);
    try {
      if (list.isNotEmpty) {
        orders = list;
        isLoading.value = false;
        update();
      }
    } catch (error) {
      isLoading.value = false;
      update();
    } finally {
      isLoading.value = false;
      update();
    }
  }

  @override
  void onInit() {
    filterOrders(orderSearchCriteria: OrderSearchCriteria(
        searchCriteria: SearchCriteria(createdFromChannelId: 'PORTAL')));
    // TODO: implement onInit
    super.onInit();
  }

}