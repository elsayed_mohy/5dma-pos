import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:khdma_pos/data/model/common/branch_employee_model.dart'
    hide User;
import 'package:khdma_pos/data/model/common/delivery_method_model.dart';
import 'package:khdma_pos/data/model/order/consumer.dart' hide User;
import 'package:khdma_pos/data/model/order/return_order_details_model.dart';
import 'package:khdma_pos/services/base_service.dart';
import 'package:khdma_pos/services/view_order/order_status_service.dart';
import 'package:khdma_pos/services/view_order/view_order_service.dart';
import 'package:khdma_pos/view/widgets/orders/add_new_customer_bottom_sheet.dart';
import 'package:khdma_pos/view/widgets/orders/driver_bottom_sheet.dart';

import '../../data/model/common/walking_consumer_model.dart';
import '../../data/model/order/branch_item.dart';
import '../../data/model/order/order_details_model.dart' hide User;
import '../../data/model/order/return_order_model.dart';
import '../../data/model/session_model.dart';
import '../../data/model/user.dart';
import '../../routes/routes.dart';
import '../../services/pos/orders_service.dart';
import '../../view/widgets/shared/get_bottom_sheet.dart';

class ViewOrderController extends GetxController {
  OrderDetails? orderDetails;
  List<ReturnOrderDetails> returnOrderDetails = [];
  List<ReturnOrders> listOfReturnedOrders = [];
  List<DeliveryMethod> deliveryMethods = [];
  List<BranchItem> selectedItems = [];
  List<BranchItem> orderItems = [];
  List<Status> orderStatus = [];
  List<ConsumerAddress> consumerAddresses = [];
  WalkingConsumer? consumer;

  RxBool customerNotFound = true.obs;
  int selectedStatusIndex = 0;
  OrdersService orderService = OrdersService();
  BaseService baseService = BaseService();
  OrderStatusService ordersStatusService = OrderStatusService();
  GetStorage storage = GetStorage();
  DeliveryMethod? selectedDeliveryMethod;
  var selectedCustomerId = '';
  var walkingConsumerId = '';
  var orderId;
  var branchDeliveryCost;
  bool isWalking = false;

  Session? sessionData;
  RxBool isLoading = false.obs;
  RxBool orderCreation = false.obs;
  RxBool orderEditable = false.obs;
  double totalPrice = 0;
  double totalDiscount = 0;
  double totalTax = 0;

  final customerFormKey = GlobalKey<FormState>();
  final TextEditingController customerIdController = TextEditingController();
  final TextEditingController customerPhoneSearchController = TextEditingController();
  final TextEditingController customerFullNameController =
  TextEditingController();
  final TextEditingController customerPhoneNumberController =
  TextEditingController();
  final TextEditingController customerEmailController = TextEditingController();
  final TextEditingController customerAddressController =
  TextEditingController();

  final orderMoreInfoFormKey = GlobalKey<FormState>();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController deliveryMethodIdController =
  TextEditingController();

  initializeData() async {
      await getSessionData();
    var id = Get.parameters['id'];
    if (id == 'order-creation') {
      orderCreation.value = true;
      await getSelectedPosItems();
    } else {
      orderId = id;
      getOrderDetails(orderId: id);
    }
    await getDeliveryMethods();
  }

  getOrderInfoInOrderCreation(sessionItems) {
    print("sessionItems ${sessionItems}");
    orderItems = sessionItems;
    update();
    totalPrice = 0.00;
    totalDiscount = 0.00;
    totalTax = 0.00;
    if (orderItems.isNotEmpty) {
      for (var item in orderItems) {
        if (item.salesPrice != null) {
          totalPrice += double.parse(item.salesPrice.toString()) *
              double.parse(item.orderedCount.toString());
        }
        if (item.discount != null) {
          totalDiscount += double.parse(item.discount.toString()) *
              double.parse(item.orderedCount.toString());
        }
        if (item.salesTaxValue != null) {
          totalTax += double.parse(item.salesTaxValue.toString()) *
              double.parse(item.orderedCount.toString());
        }
      }
      update();
    } else {
      totalPrice = 0.00;
      totalDiscount = 0.00;
      totalTax = 0.00;
      Get.toNamed(AppRoutes.pos);
    }
  }

  getSessionData() async {
    var localSession =  GetStorage().read('localSession');
      print("localSession $localSession");
    if (localSession != null) {
      sessionData = Session.fromJson(localSession);
      update();
    }
  }

  getSelectedPosItems() async {
    var localSelectedPosItems = await storage.read("SelectedPosItems");
    if (localSelectedPosItems != null) {
      var items = await storage.read('SelectedPosItems') as List;
      var selectedPosItems = [];
      if (items.runtimeType == List<dynamic>) {
        selectedItems =
            items.map((branchItem) => BranchItem.fromJson(branchItem)).toList();
      } else {
        selectedItems = items as List<BranchItem>;
      }
      getOrderInfoInOrderCreation(selectedItems);
    } else {
      selectedItems = [];
      Get.back();
    }
  }

  saveSelectedPosItems() async {
    await GetStorage().write("SelectedPosItems", orderItems);
    getOrderInfoInOrderCreation(orderItems);
  }

  incrementCreationItem(BranchItem item) {
    var index =
    orderItems.indexWhere((selectedItem) => selectedItem.id == item.id);
    if (item.orderedCount != null && index >= 0) {
      item.orderedCount = item.orderedCount! + 1;
      orderItems[index].orderedCount = item.orderedCount;
      totalPrice += item.salesPrice;
      totalDiscount += item.discount;
      totalTax += item.salesTaxValue;
      update();
    }
  }

  decrementCreationItem(BranchItem item) {
    if (item.orderedCount! > 0) {
      item.orderedCount = item.orderedCount! - 1;
      var index =
      orderItems.indexWhere((selectedItem) => selectedItem.id == item.id);
      orderItems[index].orderedCount = item.orderedCount;
      totalPrice -= item.salesPrice;
      totalDiscount -= item.discount;
      totalTax -= item.salesTaxValue;
      //
      // if (item.orderedCount == 0) {
      //   selectedItems.removeWhere((item) => item.id != item.id);
      // }
      update();
    }
  }

  removeCreationItem(item) {
    orderItems.removeWhere((i) => i.id == item.id);
    update();
    saveSelectedPosItems();
    getOrderInfoInOrderCreation(orderItems);
  }

  incrementItem(item) {
    item.qty = item.qty + 1;
    totalPrice = totalPrice + item.price;
    totalTax = totalTax + item?.branchItem?.salesTaxValue;
    totalDiscount = totalDiscount + item.discount;
    update();
  }

  decrementItem(item) {
    if (item.qty == 1) {
      return;
    }
    item.qty = item.qty - 1;
    totalPrice -= item.price;
    totalTax = totalTax - item.branchItem?.salesTaxValue;
    totalDiscount -= item.discount;
    update();
  }

  removeItem(item) {
    totalPrice -= item.price * item.qty;
    totalDiscount -= item.discount * item.qty;
    totalTax -= item.branchItem.salesTaxValue * item.qty;
    orderDetails?.order.issuingOrderDetails.removeWhere((i) => i.id == item.id);
    update();
  }

  getItemQty(BranchItem item) {
    if (selectedItems.isNotEmpty) {
      var orderCount = 0;
      for (var selectedItem in selectedItems) {
        if (selectedItem.id == item.id) {
          item.orderedCount = selectedItem.orderedCount;
          orderCount = selectedItem.orderedCount ?? 0;
        }
      }
      return orderCount;
    }
    return 0;
  }

  onDeliveryMethodChanged(int deliveryMethodId) {
    var selectedDeliveryMethod = deliveryMethods
        ?.firstWhere((deliveryMethod) => deliveryMethod.id == deliveryMethodId);
    if (selectedDeliveryMethod != null) {
      this.selectedDeliveryMethod = selectedDeliveryMethod;
      orderDetails?.order.deliveryMethod = selectedDeliveryMethod;
      update();
    } else {
      this.selectedDeliveryMethod = null;
      update();
    }
  }

  formatDate(String dates) {
    DateTime date = DateFormat('yyyy-mm-dd').parse(dates);
    return DateFormat('yyyy-mm-dd hh:mm:ss').format(date);
  }

  getDeliveryMethods() async {
    var branchId =
        sessionData?.customerBranchId ?? orderDetails?.order.branchId;
    print("branchId $branchId");
    if (branchId != null) {
      var res = await orderService.getBranchDeliveryMethods(branchId);
      if (res != null) {
        deliveryMethods = res;
        update();
      }
    }
  }

  getOrderDetails({required orderId, bool? afterSave}) async {
    isLoading.value = true;
    try {
      var res = await orderService.getOrderDetails(orderId: orderId.toString());
      if (res != null) {
        orderDetails = res;
        applyOrderData(res.status, afterSave);
        isLoading.value = false;
        update();
      }
    } finally {
      isLoading.value = false;
      update();
    }
  }

  applyOrderData(List<Status> status, bool? afterSave) {
    getDeliveryMethods();
    selectedDeliveryMethod = orderDetails?.order.deliveryMethod;
    selectedCustomerId = orderDetails!.order.consumerId.toString();
    walkingConsumerId = orderDetails!.order.walkingConsumerId.toString();
    getReturnOrderDetails(orderDetails?.order.id);
    if (status != null) {
      orderStatus = status;
    }
    selectedStatusIndex = orderStatus.indexWhere(
            (item) => item.code == orderDetails?.order.orderStatus.code);
    getOrderTotalsInViewOrder();
    var searchValue =
        orderDetails?.order.phoneNumber ?? orderDetails?.order.consumerName;
    getConsumerAddresses(searchValue);
    if (orderDetails?.order.consumerId != null ||
        orderDetails?.order.walkingConsumerId != null) {
      applyCustomerData();
    }
    if (afterSave == false) {
      applyOrderMoreInfo();
    }
  }

  applyOrderMoreInfo() {
    deliveryMethodIdController.text =
        (orderDetails?.order?.deliveryMethod?.id).toString();
    descriptionController.text = orderDetails?.order.description;
    update();
  }

  applyCustomerData() {
    var id;
    if (orderDetails?.order.consumerId != null) {
      id = orderDetails?.order.consumerId;
      update();
    } else {
      id = orderDetails?.order.walkingConsumerId
          ? orderDetails?.order.walkingConsumerId
          : null;
      update();
    }
    customerIdController.text = id.toString();
    customerFullNameController.text = orderDetails?.order.consumerName ?? '';
    customerPhoneNumberController.text = orderDetails?.order.phoneNumber ?? '';
    customerEmailController.text = orderDetails?.order.consumerEmail ?? '';
    customerAddressController.text = orderDetails?.order.consumerAddress ?? '';
    update();
  }

  getConsumerAddresses(searchVal) async {
    if (searchVal != null) {
      try {
        var customer =
        await orderService.getConsumerBySearch(searchValue: searchVal);
        if (customer != null) {
          consumerAddresses = customer.first.consumerAddresses;
          update();
        }
      } finally {}
    }
  }

  getCustomerByPhoneNumber(String phoneNumber) async {
    var res = await orderService.getConsumerByPhoneNumber(phoneNumber);
    if (res != null) {
      consumer = res.first;
      storage.write("CONSUMER", res.first);
      selectCustomer(res.first);
      update();
    } else {
      baseService.showError("consumer not found");
      update();
    }
  }

  void addNewConsumer(bottomSheetResult) {
    var consumerObj = WalkingConsumer(id: bottomSheetResult["id"],
        fullName: bottomSheetResult["fullName"],
        email: bottomSheetResult["email"],
        phoneNumber: bottomSheetResult["phoneNumber"],
        isWalking: true);
    selectCustomer(consumerObj);
    storage.write("CONSUMER", consumerObj);
    update();
  }

  selectCustomer(WalkingConsumer consumer) {
    if (consumer.isWalking) {
      walkingConsumerId = consumer.id.toString();
      isWalking = consumer.isWalking;
    } else {
      selectedCustomerId = consumer.id.toString();
      if (consumer.isWalking == false) {
        isWalking = consumer.isWalking;
      } else {
        isWalking = false;
      }
    }

    this.consumer = consumer;
    var customerObj = {
      "customerNumber": consumer.id,
      "customerName": consumer.fullName,
      "phoneNumber": consumer.phoneNumber,
      "email": consumer.email
    };
    customerIdController.text = consumer.id.toString();
    customerFullNameController.text = consumer.fullName;
    customerPhoneNumberController.text = consumer.phoneNumber;
    customerEmailController.text = consumer.email;
    // this.customerForm.patchValue(customerObj);
    // consumerAddresses = consumer.consumerAddresses;
    orderDetails?.order.consumerName = consumer.fullName;
    orderDetails?.order.consumerId = consumer.id;
    orderDetails?.order.consumerEmail = consumer.email;
    update();
  }

  getOrderTotalsInViewOrder() {
    totalPrice = 0;
    totalDiscount = 0;
    totalTax = 0;
    totalPrice = orderDetails?.order.totalAmount;
    totalDiscount = orderDetails?.order.totalDiscount ?? 0;
    totalTax = orderDetails?.order.totalTax ?? 0;
  }

  getReturnOrderDetails(id) async {
    isLoading.value = true;
    try {
      var getReturnOrderDetailsRes =
      await orderService.getReturnOrderDetails(id);
      if (getReturnOrderDetailsRes != null) {
        returnOrderDetails = getReturnOrderDetailsRes;
        var orderDetailsRes =
        await orderService.getOrderDetails(orderId: id.toString());
        if (getReturnOrderDetailsRes != null) {
          listOfReturnedOrders = orderDetailsRes.order.returnOrders;
        }
      }
      isLoading.value = false;
      update();
    } finally {
      isLoading.value = false;
      update();
    }
  }

  cancelOrderAndCreateNew() {
    Get.back();
  }

  changeStatus({bool? isBack,
    String? orderStatus,
    int? branchEmployeeId,
    int? posSessionId}) async {
    var statusObject = {
      "id": orderDetails?.order.id,
      "statusCodeStr": orderStatus,
      "posSessionId": posSessionId
    };
    var changeStatusRes =
    await ordersStatusService.changeStatus(statusObject, branchEmployeeId);
    if (changeStatusRes != null) {
      orderDetails?.order.statusCode = changeStatusRes;
      selectedStatusIndex = orderDetails!.status
          .indexWhere((element) => element.code == changeStatusRes.code);
      update();
      if (isBack == true) {
        Get.back();
      }
    }
  }

  withDriver() async {
    var driversResult =
    await orderService.getDrivers(orderDetails?.order.branchId);
    if (driversResult != null) {
      BranchEmployee driverBottomSheetResult =
      await getBottomSheet(250, DriverBottomSheet(drivers: driversResult));
      orderDetails?.order.deliveryEmployeeId = driverBottomSheetResult.id;
      update();
      updateOrder(isBack: false, actionType: null, statusCode: 'DRIVER');
      update();
    }
  }

  cancelOrder() {
    changeStatus(isBack: false, orderStatus: 'CANCELED');
    update();
  }

  orderDelivered() {
    updateOrder(isBack: false, actionType: null, statusCode: 'DELIVERED');
    update();
  }

  closeOrder() {
    if (sessionData == null) {
      baseService.showError("POS_SESSION_NOT_FOUND");
      return;
    }
    changeStatus(
        isBack: false,
        branchEmployeeId: null,
        orderStatus: 'DELIVERED',
        posSessionId: sessionData?.id);
    update();
  }

  emptyCart() {
    clearSelectedItems();
    Get.back();
  }

  clearSelectedItems() {
    orderService.selectedItems = [];
    storage.remove("SelectedPosItems");
    storage.remove("consumer");
    update();
  }

  printFile({required orderId, required taxNumber}) async {
    await ViewOrderService().downFile(
        '${"order"}box_summary${DateTime.now()}.pdf', orderId, taxNumber);
  }

  preparedOrder() {
    var orderType = orderDetails?.order.createdFromChannelId ?? 'PORTAL';
    if (orderType == 'MOBILE') {
      updateOrder(isBack: false, statusCode: 'PREPARED');
    } else {
      if ((selectedCustomerId == null && walkingConsumerId == null) &&
          selectedDeliveryMethod != null) {
        baseService.showError("select customer");
        return;
      }
      if (orderCreation.isTrue) {
        applyOrder(
            isBack: false,
            statusCode: 'PREPARED',
            actionType: 'SAVE_AND_CLOSE');
      } else {
        updateOrder(
            isBack: false,
            statusCode: 'PREPARED',
            actionType: 'SAVE_AND_CLOSE');
      }
    }
  }

  applyOrder({required String statusCode,
    bool? isBack,
    String? actionType,
    dynamic? paymentId,
    dynamic? lang,
    dynamic? paidAmount}) async {
    try {
      var orderStatusByCodeResponse = await ordersStatusService
          .getOrderStatusByCode(statusCode: statusCode);
      var orderObj =
      createOrderObj(orderStatusByCodeResponse, paymentId, paidAmount);
      await orderService.saveOrder(orderObj).then((value) {
        getOrderDetails(orderId: value.order.id, afterSave: true);
        orderCreation.value = false;
        if (statusCode == 'CLOSED' && actionType == 'save_and_print') {
          orderDetails?.order.id = value.order.id;
          printFile(
              orderId: value.order.id, taxNumber: value.order.issuingNumber);
        }
        if (isBack != null && isBack == true) {
          Get.back();
        }
      });
    } finally {}
  }

  createOrderObj(Status orderStatus, dynamic? paymentId, dynamic? paidAmount) {
    User user = userFromJson(GetStorage().read("user"));

    if (sessionData == null) {
      return;
    }
    var branchId = sessionData?.customerBranchId;
    var posSessionId = sessionData?.id;
    var newDetails = [];
    var order = orderItems;

    for (var item in order!) {
      newDetails.add({
        "id": null,
        "branchItem": {
          "id": (item.item?.hasRevision && item.revisionId != null)
              ? item.revisionId
              : item.id
        },
        "branchItemId": (item.item?.hasRevision && item.revisionId != null)
            ? item.revisionId
            : item.id,
        "costMethodId": null,
        "issuingOrderId": 1,
        "price": item.salesPrice,
        "qty": item.orderedCount,
        "unitId": item.item?.baseUnit?.id,
        "salesTaxValue": item.salesTaxValue,
        "itemName": item?.item?.name,
        "discount": item.discount,
        "colorId": item?.color?.id,
        "sizeId": item?.size?.id,
      });
    }
    Map<String, dynamic> orderObj = {
      "id": "null",
      "branchId": branchId,
      "posSessionId": posSessionId,
      "consumerId": selectedCustomerId,
      // "consumerAddress":selectedConsumerAddress,
      "consumerAddress": null,
      "description": descriptionController.text,
      "deliveryMethod": selectedDeliveryMethod?.toJson(),
      "deliveryCost": branchDeliveryCost,
      "issuingReason": 'issuing reason',
      "issuingTypeId": 1,
      "orderStatus": orderStatus.toJson(),
      "createdFromChannelId": 'PORTAL',
      "paymentMethod": {"id": paymentId},
      "paidAmount": paidAmount,
      "phoneNumber":  consumer?.phoneNumber,
      "preparedBy": user.userId,
      "totalAmount": totalPrice,
      "totalDiscount": totalDiscount,
      "issuingOrderDetails": [...newDetails],
      "walkingConsumerId": walkingConsumerId,
      "offers": [],
      "taxExemptionReason": null,
      "reported": false,
      "xmlFilePath": null,
      "zatcaInvoiceStatus": null,
      "zatcaReportingDate": null,
      "zatcaReportingErrorMessage": null,
      "zatcaReportingSequence": null,
      "zatcaResponses": [
      ],
      "zatcaTransactionDate": null
    };
    return orderObj;
  }

  createOrder({bool? isBack,
    String? actionType,
    dynamic? paymentId,
    dynamic? lang,
    dynamic? paidAmount}) {
    if (selectedCustomerId == null && selectedDeliveryMethod != null) {
      baseService.showError("Select Customer");
      return;
    }

    if (actionType == 'save') {
      applyOrder(
          statusCode: 'NEW',
          isBack: isBack,
          actionType: actionType,
          paymentId: paymentId,
          lang: lang,
          paidAmount: paidAmount);
    } else if (actionType == 'save_and_close') {
      applyOrder(
          statusCode: 'CLOSED',
          isBack: isBack,
          actionType: actionType,
          paymentId: paymentId,
          lang: lang,
          paidAmount: paidAmount);
    } else if (actionType == 'save_and_print') {
      applyOrder(
          statusCode: 'CLOSED',
          isBack: isBack,
          actionType: actionType,
          paymentId: paymentId,
          lang: lang,
          paidAmount: paidAmount);
    } else if (actionType == 'save') {
      applyOrder(
          statusCode: 'NEW',
          isBack: isBack,
          actionType: actionType,
          paymentId: paymentId,
          lang: lang,
          paidAmount: paidAmount);
    }
  }

  updateOrder({bool? isBack,
    String? statusCode,
    String? actionType,
    dynamic? paymentId,
    dynamic? lang,
    dynamic? paidAmount}) async {
    var updateOrderObj = orderDetails!.order;
    if (statusCode != null) {
      updateOrderObj.orderStatus.code = statusCode;
    }
    if (paymentId != null) {
      updateOrderObj.paymentMethod.id = paymentId;
    }

    if (paidAmount != null) {
      updateOrderObj.paidAmount = paidAmount;
    }
    updateOrderObj.issuingDate = formatDate(updateOrderObj.issuingDate);
    log(jsonEncode(updateOrderObj.toJson()).toString());
    if (updateOrderObj.deliveryMethod?.id == null) {
      updateOrderObj.deliveryMethod = null;
    }
    await orderService.updateOrder(updateOrderObj).then((value) {
      print(value);
      orderDetails?.order.orderStatus.code = statusCode;
      if (actionType == 'save_and_print') {
        printFile(
            orderId: value["id"], taxNumber: value["issuingNumber"]);
      }
      if (isBack != null && isBack == true) {
        Get.back();
      }
    });
  }

  onSave({bool? isBack, String? actionType}) {
    if (orderCreation.value == true) {
      createOrder(isBack: isBack, actionType: actionType);
    } else {
      updateOrder(isBack: isBack, actionType: actionType);
    }
  }

  onSaveAndPrint({bool? isBack,
    String? actionType,
    dynamic? paymentId,
    dynamic? lang,
    dynamic? paidAmount}) {
    if (orderDetails?.order.id != null) {
      updateOrder(
          isBack: isBack,
          actionType: actionType,
          paymentId: paymentId,
          lang: lang,
          paidAmount: paidAmount);
    } else {
      createOrder(
          isBack: isBack,
          actionType: actionType,
          paymentId: paymentId,
          lang: lang,
          paidAmount: paidAmount);
    }
  }

  saveAndPrint() {
    onSaveAndPrint(isBack: true, actionType: 'save_and_print');
  }

  saveAndClose() {
    onSave(isBack: true, actionType: 'save_and_close');
  }

  save() {
    onSave(isBack: false, actionType: 'save');
  }

  @override
  void onInit() {
    initializeData();
    // TODO: implement onInit
    super.onInit();
  }


}
