import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:khdma_pos/data/model/branch_config_model.dart';
import 'package:khdma_pos/data/model/branch_user_model.dart';
import 'package:khdma_pos/data/model/session_model.dart';
import 'package:khdma_pos/routes/routes.dart';
import 'package:khdma_pos/services/customer/customer_service.dart';
import 'package:khdma_pos/services/pos/orders_service.dart';
import 'package:khdma_pos/utils/constants/status_code.dart';
import 'package:khdma_pos/utils/constants/user_type.dart';

import '../../data/model/customer_branch_model.dart';
import '../../data/model/user.dart';
import '../../services/pos/session_service.dart';

class SessionController extends GetxController {
  User user = userFromJson(GetStorage().read("user"));
  OrdersService ordersService = OrdersService();
  SessionService sessionService = SessionService();
  GetStorage storage = GetStorage();
  List<CustomerBranch> branchesList = <CustomerBranch>[].obs;
  CustomerBranch? selectedBranch;
  BranchUser? selectedBranchUser;
  List<BranchUser> branchUsersList = <BranchUser>[].obs;
  Session? lastUserSession;
  double? totalCashAmount;
  RxBool isLoading = true.obs;
  RxBool isCustomer = true.obs;
  RxBool isActiveForm = false.obs;
  RxBool isStartSession = true.obs;
  RxBool isActiveJustification = false.obs;
  final sessionFormKey = GlobalKey<FormState>();
  final pettyCashAmountController = TextEditingController(text: "0");
  final totalCashAmountController = TextEditingController(text: "0");
  final varianceController = TextEditingController(text: "0");
  final justificationController = TextEditingController();

  void setCustomerBranches() async {
    isLoading.value = true;
      var branches = await GetStorage().read('customer_branches') as List;
    if (branches != null) {
      var customerBranches = branches.map((branch) => CustomerBranch.fromJson(branch)).toList();
      branchesList.assignAll(customerBranches);
      isLoading.value = false;
    }else {
      print("customer_branches is  null");
    }
  }

  checkUserType() {
    for (var userType in user.userTypes) {
      if (userType.code == UserType.CUST.name) {
        debugPrint("userType customer");
        if(lastUserSession != null ) {
          pettyCashAmountController.text = lastUserSession!.cashAmount.toString();
          totalCashAmountController.text =
              lastUserSession!.totalOrderAmount.toString();
          varianceController.text = lastUserSession!.variance.toString();
          justificationController.text =
              lastUserSession!.justification.toString();
          sessionFormKey.currentState?.deactivate();
          update();
        }

      } else if (userType.code == UserType.EMP.name) {
        debugPrint("userType employee");
        isCustomer.value = false;
        // getCurrentUserBranch();
        // branchesList.assignAll(
        //     [branchesList.firstWhere((branch) => branch.id == user.branchId)]);
        onBranchChange(user.branchId);
        isActiveForm.value = true;
        sessionFormKey.currentState?.activate();
        update();
      }
    }
  }

  getCurrentUserBranch() {
    var branch =
        branchesList.firstWhere((branch) => branch.id == user.branchId);
    selectedBranch = branch;
    update();
  }

  getLastSession() async {
    if (GetStorage().read('localSession') != null) {
      isLoading.value = true;
      await checkUserType();
      Session localSession =
          Session.fromJson(GetStorage().read('localSession'));
      print("localSession from getLastSession ${localSession.toJson()}");
      localSession.variance =
          localSession.totalOrderAmount - localSession.cashAmount;
      pettyCashAmountController.text =localSession.cashAmount.toString();
      varianceController.text =localSession.variance.toString();
      justificationController.text =localSession.justification.toString();
      await onBranchChange(localSession.customerBranchId);

      await getLastSessionFromUserId(localSession.fromUserId);
      isStartSession.value = false;
      isActiveForm.value = false;

      isLoading.value = false;
      update();
    } else {
      var response = await sessionService.getLastSessionByUser();
      print("res => ${response}");
      try {
        isLoading.value = true;
        var session =  Session.fromJson(response.data["data"]);
        session.totalOrderAmount = session.totalOrderAmount ?? 0;
        session.variance = session.totalOrderAmount - session.cashAmount;
        pettyCashAmountController.text =session.cashAmount.toString();
        totalCashAmountController.text = session.totalOrderAmount.toString();
        varianceController.text =session.variance.toString();
        justificationController.text =session.justification.toString();
        lastUserSession = session;
        if(response.statusCode == 200) {
          await checkUserType();
        }
        isStartSession.value = false;
        isActiveForm.value = false;
        update();
        await onBranchChange(session.customerBranchId);
        await getLastSessionFromUserId(session.fromUserId);
        isLoading.value = false;
      } finally {
        isLoading.value = false;
      }
    }

  }

  getLastSessionFromUserId(int fromUserId) async {

    selectedBranchUser =branchUsersList.length > 1 ?
        branchUsersList.firstWhere((user) => user.id == fromUserId) :branchUsersList[0] ;
    var session = await sessionService.getLastSessionFromUserId(fromUserId);
    lastUserSession?.totalOrderAmount = session.totalOrderAmount;
    lastUserSession = session;
    lastUserSession?.fromUserId = fromUserId;
    pettyCashAmountController.addListener(() {
      var variance = session.totalOrderAmount - double.parse(pettyCashAmountController.text);
      var roundedVariance = variance.toStringAsFixed(2);
      varianceController.text = roundedVariance;
      lastUserSession!.variance = variance;
      if(double.parse(varianceController.text) > 0) {
        isActiveJustification.value = true;
        update();
      }
    });
    totalCashAmountController.text = session.totalOrderAmount.toString();
    update();
  }

  onBranchChange(branchId) async {
    var list = await CustomerService().getBranchUsers(branchId);
    try {
      if (list.isNotEmpty) {
        branchUsersList.assignAll(list);
        update();
      }
    } finally {
    }
  }

  continueOrdersCreations({required bool isFood}) {
    ordersService.setSelectedItems([]);
    var sessionData = lastUserSession;
      ordersService.setSessionData(sessionData!);
       storage.write("localSession", sessionData);
       isFood ? Get.toNamed(AppRoutes.foodMethodScreen) :Get.toNamed(AppRoutes.pos);
  }

  startOrdersCreations({required bool isFood})async {
    ordersService.setSelectedItems([]);

    print("last${lastUserSession}");

    var sessionData = lastUserSession;
    print(sessionData);
    sessionData?.toUserId = user.userId;
    sessionData?.startTime = DateFormat('yyyy-MM-dd hh:mm:ss a').format(DateTime.now());
    sessionData?.statusCode = StatusCode.open.value;
   var sessionResponse = await sessionService.startSession(sessionData: sessionData);
     try {
     if(sessionResponse != null ) {
        ordersService.setSessionData(sessionResponse);
       await storage.write("localSession", sessionData);
        isFood ? Get.toNamed(AppRoutes.foodMethodScreen) :Get.toNamed(AppRoutes.pos);
     }
     }catch(r){

     }


  }

  isAllowedDistance(Position position, BranchConfig branch) {
    var empLocation = {
      'latitude': position.latitude,
      'longitude': position.longitude,
    };
    var branchLocation = {
      'latitude': branch.lat,
      'longitude': branch.lon,
    };
    var empDistance = getDistanceBetweenTwoPoints(branchLocation, empLocation);
    return empDistance <= branch.allowedMetersTocheckIn;
  }

  getDistanceBetweenTwoPoints(cord1, cord2) {
    if (cord1['latitude'] == cord2['latitude'] &&
        cord1['longitude'] == cord2['longitude']) {
      return 0;
    }

    var radlat1 = (math.pi * cord1['latitude']) / 180;
    var radlat2 = (math.pi * cord2['latitude']) / 180;

    var theta = cord1['longitude'] - cord2['longitude'];
    var radtheta = (math.pi * theta) / 180;

    var dist = math.sin(radlat1) * math.sin(radlat2) +
        math.cos(radlat1) * math.cos(radlat2) * math.cos(radtheta);

    if (dist > 1) {
      dist = 1;
    }

    dist = math.acos(dist);
    dist = (dist * 180) / math.pi;
    dist = dist * 60 * 1.1515;
    dist = dist * 1609.34; //convert miles to meter
    dist = dist.floorToDouble();
    return dist;
  }

  @override
  void onInit() {
    super.onInit();
    sessionFormKey.currentState?.deactivate();
    setCustomerBranches();
    getLastSession();

  }
}
