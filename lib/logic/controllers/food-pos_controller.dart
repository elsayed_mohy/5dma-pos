import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/data/model/common/config.dart';
import 'package:khdma_pos/data/model/food/food_item.dart';
import 'package:khdma_pos/data/model/order/branch_category.dart';
import 'package:khdma_pos/data/model/order/branch_item.dart';
import 'package:khdma_pos/routes/routes.dart';
import 'package:khdma_pos/services/auth_service.dart';
import 'package:khdma_pos/services/pos/food_orders_service.dart';

import '../../data/model/session_model.dart';
import '../../services/pos/session_service.dart';

class FoodPOSController extends GetxController {
  SessionService sessionService = SessionService();
  late AnimationController animationController;
  List<FoodItem> foodItems = [];
  List<FoodItem> filteredFoodItems = [];
  List<FoodItem> selectedItems = [];
  List<BranchCategory> branchCategories = [];
  BranchCategory? selectedBranchCategory;
  List<PaymentMethod> paymentMethods = [];
  PaymentMethod? selectedPaymentMethod;
  List<bool> paymentIsSelected = [true, false, false, false];
  int selectedItemsCount = 0;
  double totalPrice = 0.00;
  double totalDiscount = 0.00;
  double totalTax = 0.00;
  Session? sessionData;
  RxBool isLoadingTableView = true.obs;
  bool isSearching = false;
  bool isLoading = true;
  bool isItemsLoading = true;
  bool isFlipped = false;
  GetStorage storage = GetStorage();

  selectItem(FoodItem item) {
    var index =
        selectedItems.indexWhere((selectedItem) => selectedItem.id == item.id);
    if (index >= 0) {
      item.orderedCount = selectedItems[index].orderedCount;
    }
    if (item.orderedCount != null && index >= 0) {
      item.orderedCount = item.orderedCount! + 1;
      selectedItems[index].orderedCount = item.orderedCount;
      update();
    } else {
      item.orderedCount = 1;
      selectedItemsCount++;
      selectedItems.add(item);
      update();
    }
    saveSelectedPosItems();
  }

  // showSubCategories(BranchCategory category) {
  //   selectedBranchCategory = category;
  //   isFlipped = !isFlipped;
  //   isFlipped ? animationController.forward() : animationController.reverse();
  //   filteredBranchItems = branchItems
  //       .where((element) => element.item?.categoryId == category.id)
  //       .toList();
  //   update();
  // }

  // showSubCategoriesItems(SubCategory subCategory) {
  //   filteredBranchItems = branchItems
  //       .where((element) => element.item?.subCategoryId == subCategory.id)
  //       .toList();
  //   update();
  // }

  // hideSubCategories() {
  //   isFlipped = !isFlipped;
  //   isFlipped ? animationController.forward() : animationController.reverse();
  //   filteredBranchItems = branchItems;
  //   update();
  // }

  deselectItem(FoodItem item) {
    if (item.orderedCount! > 0) {
      item.orderedCount = item.orderedCount! - 1;
      var index = selectedItems
          .indexWhere((selectedItem) => selectedItem.id == item.id);
      selectedItems[index].orderedCount = item.orderedCount;

      if (item.orderedCount == 0) {
        selectedItemsCount--;
        selectedItems.removeWhere((item) => item.id != item.id);
      }
      saveSelectedPosItems();
      update();
    }
  }

  getItemQty(FoodItem item) {
    if (selectedItems.isNotEmpty) {
      var orderCount = 0;
      for (var selectedItem in selectedItems) {
        if (selectedItem.id == item.id) {
          item.orderedCount = selectedItem.orderedCount;
          orderCount = selectedItem.orderedCount ?? 0;
        }
      }
      return orderCount;
    }
    return 0;
  }

  saveSelectedPosItems() async {
    var storeSelectedItems = selectedItems.map((e) => json.encode(e)).toList();
   await GetStorage().write("SelectedPosItems", selectedItems);
    getOrderInfoInOrderCreation(selectedItems);
  }

  getSelectedPosItems() async {
    var localSelectedPosItems = storage.read("SelectedPosItems");
    if (localSelectedPosItems != null) {
      isLoadingTableView.value = true;
     // var items = await json.decode(storage.read('SelectedPosItems'));
      var items = await GetStorage().read('SelectedPosItems') as List;
      var selectedPosItems =
          items.map((foodItem) => FoodItem.fromJson(foodItem)).toList();
      selectedItems = selectedPosItems;
      getOrderInfoInOrderCreation(selectedItems);
      selectedItemsCount = selectedPosItems.length;
      isLoadingTableView.value = false;
    } else {
      isLoadingTableView.value = true;
      selectedItems = [];
      getOrderInfoInOrderCreation(selectedItems);
      selectedItemsCount = selectedItems.length;
      isLoadingTableView.value = false;
    }
  }

  getFoodItems({required customerBranchId}) async {
    if (GetStorage().read('food_items') != null) {
      var localFoodItems = await GetStorage().read('food_items') as List;
      var localFoodItemsList = localFoodItems
          .map((branchItem) => FoodItem.fromJson(branchItem))
          .toList();
      foodItems = localFoodItemsList;
      filteredFoodItems = foodItems;
      isItemsLoading = false;
      update();
    } else {
      var list =
          await FoodOrdersService().getFoodItems(branchId: customerBranchId);
      try {
        if (list.isNotEmpty) {
          foodItems = list;
          filteredFoodItems = foodItems;
          isItemsLoading = false;
          update();
        }
      } catch (error) {
        isItemsLoading = false;
        update();
      } finally {
        isLoading = false;
        update();
      }
    }
  }

  getBranchCategories({required customerBranchId}) async {
    var list =
        await FoodOrdersService().getBranchMenuCategories(branchId: customerBranchId);
    try {
      if (list.isNotEmpty) {
       // branchCategories = list;
        selectedBranchCategory = branchCategories[0];
        isLoading = false;
        update();
      }
    } catch (error) {
      isLoading = false;
      update();
    } finally {
      isLoading = false;
      update();
    }
  }

  startSearch() {}

  filterData(searchValue) {
    // for (var item in branchItems) {
    //   // if(typeofEquals(searchValue, type))
    //   if (item.barCode?.contains(searchValue.trim())) {
    //     filteredBranchItems = branchItems
    //         .where((element) => element.barCode?.contains(searchValue.trim()))
    //         .toList();
    //     update();
    //   } else if ((item.item?.name)
    //       .toLowerCase()
    //       .contains(searchValue.trim().toLowerCase())) {
    //     filteredBranchItems = branchItems
    //         .where((element) => element.item?.name
    //             .toLowerCase()
    //             .contains(searchValue.trim().toLowerCase()))
    //         .toList();
    //     update();
    //   }
    // }
  }

  prepareCreationItemOfferObj(BranchItem item){
    //if(item.offerDetails)
  }

  getOrderInfoInOrderCreation(List<FoodItem> orderItems) {
    totalPrice = 0.00;
    totalDiscount = 0.00;
    totalTax = 0.00;
    if (orderItems.isNotEmpty) {
      // for (var item in orderItems) {
      //   if (item.salesPrice != null) {
      //     totalPrice += item.salesPrice * item.orderedCount;
      //   }
      //   if (item.discount != null) {
      //     totalDiscount += item.discount * item.orderedCount;
      //   }
      //   if (item.salesTaxValue != null) {
      //     totalTax += item.salesTaxValue * item.orderedCount;
      //   }
      // }
    } else {
      totalPrice = 0.00;
      totalDiscount = 0.00;
      totalTax = 0.00;
    }
  }

  setPaymentMethods() {
    if (storage.read('config') != null) {
      var config = Config.fromJson(storage.read('config'));
      paymentMethods = config.paymentMethods;
      selectedPaymentMethod = paymentMethods[0];
      update();
    }
  }

  initializeData() {
    setPaymentMethods();
    var localSession = storage.read('localSession');
    if (localSession != null) {
      if(localSession.runtimeType == Map<String, dynamic>){
        sessionData = Session.fromJson(localSession);
      }else{
        sessionData = localSession as Session;
      }
      initializeOrders(sessionData?.customerBranchId);
    } else {
      getUserSession();
    }
    getSelectedPosItems();
  }

  emptyCart() async{
    //
    print("clear the cart");
  }

  initializeOrders(branchId) async {
   // await getBranchCategories(customerBranchId: branchId);
    await getFoodItems(customerBranchId: branchId);
  }

  getUserSession() async {
    var response = await sessionService.getLastSessionByUser();
    if (response.statusCode == 200) {
      var session = Session.fromJson(response.data["data"]);
      sessionData = session;
      initializeOrders(session.customerBranchId);
      update();
    } else {
      Get.toNamed(Routes.home);
    }
  }

  @override
  void onInit() {
    initializeData();
    AuthService().getConfigData();
    super.onInit();
  }
}
