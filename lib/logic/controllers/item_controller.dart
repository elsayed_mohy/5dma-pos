import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:khdma_pos/services/pos/item_service.dart';
import '../../data/model/order/branch_item.dart';
import '../../data/model/order/item_size_model.dart';
import '../../data/model/user.dart';
import '../../utils/constants/constants.dart';
import 'package:collection/collection.dart';

class ItemController extends GetxController {
  User user = userFromJson(GetStorage().read("user"));
  List<BaseUnit> units = [];
  BaseUnit? selectedUnit;
  List<Color> colors = [];
  Color? selectedColor ;
  ItemSize? selectedSize ;
  List<ItemSize> sizes = [];
  List<BranchItem> revisionsItems = [];
  ItemService itemService = ItemService();
  GetStorage storage = GetStorage();
  Constants constants = Constants();
  double maxDiscount = 0;
  final formKey = GlobalKey<FormState>();

  final barcodeController = TextEditingController();
  final branchItemIdController = TextEditingController();

  final nameController = TextEditingController();

  final priceWithTaxController = TextEditingController();

  final priceWithoutTaxController = TextEditingController();

  final discountController = TextEditingController();

  final priceController = TextEditingController();

  final availableQtyController = TextEditingController();

  final orderQtyController = TextEditingController();
  final priceAfterDiscountController = TextEditingController();
  final revisionIdController = TextEditingController();
  final taxValueController = TextEditingController();

  getUnits() async {
    var response = await itemService.getUnits();
    try {
      units = response;
      update();
    } finally {}
  }

  getColors(mainBranchItemId) async {
    var response = await itemService.getItemColors(mainBranchItemId);
    try {
      colors = response;
      update();
    } finally {}
  }

  getRevisionsItems() async {
    var localSelectedPosItems = GetStorage().read("SelectedPosItems");
    if (localSelectedPosItems != null) {
      var items = await GetStorage().read('SelectedPosItems') as List;
      var selectedPosItems = [];
      if(items.runtimeType == List<dynamic>){
        revisionsItems = items.map((branchItem) => BranchItem.fromJson(branchItem)).toList();
      }else{
        revisionsItems = items as List<BranchItem>;
      }


      print(revisionsItems);
     // revisionsItems = selectedPosItems;
      update();
    }
  }

  getRevItem(id) {
    if(revisionsItems.isNotEmpty) {
      print(revisionsItems.firstWhereOrNull((item) => item.id == id));
    return revisionsItems.firstWhereOrNull((item) => item.id == id) ;
    }
  }

  sizeChanged(sizeId) {
    var item = sizes.firstWhere((itemSize) => itemSize.size.id == sizeId);
    setMaxDiscountValue(item.salesPrice + item.salesTaxValue);
    var priceAfterDiscount =
        (item.salesPrice + item.salesTaxValue) - item.discount;
    availableQtyController.text = item.quantity.toString();
    priceController.text = item.salesPrice.toString();
    priceWithTaxController.text =
        (item.salesPrice + item.salesTaxValue).toString();
    discountController.text = item.discount.toString();
    priceAfterDiscountController.text = priceAfterDiscount.toString();
    revisionIdController.text = item.id.toString();
    taxValueController.text = item.salesTaxValue.toString();
  }

  onDiscountChange() {
    var newTax = (double.parse(priceController.text) - double.parse(discountController.text)) *
        constants.getTaxPercentage();
    var newPriceAfterDiscount =
        (double.parse(priceController.text) + newTax) - double.parse(discountController.text);

    priceController.text =
    (((newTax) * constants.ROUND).round() / constants.ROUND).toString();
    priceAfterDiscountController.text =
    (((newPriceAfterDiscount) * constants.ROUND).round() / constants.ROUND).toString();
  }

  onChangePriceAfterDiscount(){
    var newDiscount = double.parse((priceController.text))
        - (double.parse(priceAfterDiscountController.text) - ((double.parse(priceAfterDiscountController.text) /1.15) * constants.getTaxPercentage()));
    var newTax = (double.parse(priceAfterDiscountController.text) / 1.15) * constants.getTaxPercentage();

      discountController.text = (((newDiscount) * constants.ROUND).round() / constants.ROUND).toString();
      priceController.text =  (((newTax) * constants.ROUND).round() / constants.ROUND).toString();
  }

  bool hasDiscountPermission() {
    if (constants.userIsAdmin() ||
        constants.userIsBackOffice() ||
        constants.userIsCustomer()) {
      return true;
    } else if (constants.userIsEmployee() || constants.userIsSupervisor()) {
      if (user.discountPercentage != null) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

  getSizes(mainBranchItemId, colorId) async {
    var response = await itemService.getItemSizes(mainBranchItemId, colorId);
    try {
      sizes = response;
      print(sizes.toString());
      update();
    } finally {}
  }

  setMaxDiscountValue(salesPriceWithTax) {
    maxDiscount = salesPriceWithTax;
    if (constants.userIsEmployee() || constants.userIsSupervisor()) {
      if (user.discountPercentage != null) {
        var discountPercentage = user.discountPercentage;
        var maxDiscountAmount = user.maxDiscountAmount;
        var discountValue = salesPriceWithTax * (discountPercentage / 100);

        update();
        if (maxDiscountAmount && discountValue > maxDiscountAmount) {
          maxDiscount = maxDiscountAmount;
          update();
        } else {
          maxDiscount = discountValue;
          update();
        }
      }
    }
  }

  @override
  void onInit() {
    getRevisionsItems();
    getUnits();
    // TODO: implement onInit
    super.onInit();
  }
}
